package com.mfuhrmann.algotrading.strategymanager.model.xtb;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.stream.Stream;

@Repository
public interface XtbTickDao extends MongoRepository<XtbTick, String> {

    Stream<XtbTick> findAllBySymbolAndTimeBetweenOrderByTime(String symbol, Instant startDate, Instant endDate);


//
//    //    Optional<Trades> findByCurrencyPair_Currency1AndTxId(String currency1, Long txId);
////
//    Stream<XtbCandle> findAllBySymbolAndDurationOrderByOpenTimeAsc(String symbol, Duration duration);
//
//    @Query(value = "{$and: [{openTime:{  $gte:?1, $lt: ?2}}, {symbol:?0}, {duration:?3}]}")
//    List<XtbCandle> findAllBySymbolAndOpenTimeGreaterThanEqualAndOpenTimeLessThanAndDuration(String symbol, Instant start, Instant end, Duration duration);
//
//    Stream<XtbCandle> findAllBySymbolAndDurationAndOpenTimeAfterOrderByOpenTimeAsc(String symbol, Duration duration, Instant openTime);
//
//
//    List<XtbCandle> deleteAllByOpenTimeBeforeAndDuration(Instant startDate, Duration duration);
//
//
//    //
////    Optional<Trades> getFirstByCurrencyPair_Currency1OrderByTxIdDesc(String currency1);
////
////    Stream<Trades> findAllByCurrencyPair_Currency1AndDateIsBetweenOrderByTxIdAsc(String currency1, LocalDateTime startDate, LocalDateTime endDate);
////
//    Optional<XtbCandle> findFirstBySymbolAndDurationOrderByOpenTimeDesc(String symbol, Duration duration);
//
//    Optional<XtbCandle> findBySymbolAndDurationAndOpenTime(String symbol, Duration duration, Instant openTime);
//
//
//    Long countAllBySymbolAndDuration(String symbol, Duration duration);

}
