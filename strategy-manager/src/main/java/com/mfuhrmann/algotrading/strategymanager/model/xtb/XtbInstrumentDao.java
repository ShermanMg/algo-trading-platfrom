package com.mfuhrmann.algotrading.strategymanager.model.xtb;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface XtbInstrumentDao extends MongoRepository<XtbInstrument, String> {


    Optional<XtbInstrument> findBySymbol(String symbol);


//    Optional<XtbInstrument> findAllBy(String symbol, Duration duration, Instant openTime);


}
