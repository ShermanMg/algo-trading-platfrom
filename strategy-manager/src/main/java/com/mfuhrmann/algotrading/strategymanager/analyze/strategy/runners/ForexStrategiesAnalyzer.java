package com.mfuhrmann.algotrading.strategymanager.analyze.strategy.runners;

import com.mfuhrmann.algotrading.commons.CurrencyPair;
import com.mfuhrmann.algotrading.strategymanager.analyze.strategy.*;
import com.mfuhrmann.algotrading.strategymanager.model.deutschebank.FxRateRangeRepository;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandleDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.ta4j.core.*;

import java.text.DecimalFormat;
import java.time.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
public class ForexStrategiesAnalyzer {

    private static final Logger LOGGER = LoggerFactory.getLogger(TechnicalAnalysisService.class);

    private final XtbCandleDao xtbCandleDao;
    private final StrategyFactory strategyFactory;
    private final StrategyAnalyzer strategyAnalyzer;
    private final StrategyManager strategyManager;
    private final ChartDisplayer chartDisplayer;
    private final FxRateRangeRepository fxRateRangeRepository;

    private static final DecimalFormat df = new DecimalFormat("#.####");


    public ForexStrategiesAnalyzer(XtbCandleDao xtbCandleDao, StrategyFactory strategyFactory, StrategyAnalyzer strategyAnalyzer, StrategyManager strategyManager,
                                   ChartDisplayer chartDisplayer, FxRateRangeRepository fxRateRangeRepository) {
        this.xtbCandleDao = xtbCandleDao;
        this.strategyFactory = strategyFactory;
        this.strategyAnalyzer = strategyAnalyzer;
        this.strategyManager = strategyManager;
        this.chartDisplayer = chartDisplayer;
        this.fxRateRangeRepository = fxRateRangeRepository;
    }


//    @Scheduled(initialDelay = 1000, fixedDelay = 3600 * 1000 * 24)
    public void analyze() {
        Duration duration = Duration.ofHours(1);

        findBestStrategyForCurrencyForPeriod("DE30", LocalDateTime.now().minusDays(90), LocalDateTime.now(), duration);


    }

    private void findBestStrategyForCurrencyForPeriod(String currency,LocalDateTime start,LocalDateTime end , Duration duration) {


        List<Bar> bars = createBars(currency, duration, 2000);

        if (bars.isEmpty()) {
            throw new IllegalArgumentException("Wrong data");
        }

        LOGGER.info("Building strategies for duration " + duration);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        TimeSeries series = new BaseTimeSeries(bars);
        StrategyHolder bestStrategy = strategyManager.chooseBestStrategy(series);
//
        LOGGER.info(" ************************************************************ ");
        LOGGER.info("");

        LOGGER.info("Best strategy " + bestStrategy.getStrategy().getName());

        strategyAnalyzer.checkStrategy(bestStrategy.getStrategy(), series, StrategyAnalyzer.ORDER_TYPE);
        stopWatch.stop();

        LOGGER.info("Total time " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTotalTimeMillis()) + " seconds");

        LOGGER.info(" ************************************************************ ");
//        int split = (int) (bars.size() * 0.5);
//        List<Bar> trainBars = new ArrayList<>(bars.subList(0, (split)));


        BaseTimeSeries baseTimeSeries = new BaseTimeSeries(bars);

//        chartDisplayer.displayChartWithStrategy(new CurrencyPair(currency.substring(0, 3), currency.substring(3, 6)), baseTimeSeries, bestStrategy.getStrategy(), duration, 14);
        chartDisplayer.displayChartWithStrategy(new CurrencyPair(currency.substring(0, 2), currency.substring(2, 4)), baseTimeSeries, bestStrategy.getStrategy(), duration, 14);
    }
//
//    public StrategyHolder displayChart(Duration duration, TimeSeries series) {
//
//
////        Strategy bestWalkForwardStrategy = checkBestWalkForwardStrategy(allSeries, strategies);
//
//        chartDisplayer.displayChartWithStrategy(new CurrencyPair("DE30", "PLN"), series, bestStrategy.getStrategy(), duration, 14);
//
//        LOGGER.info("  ");
//        LOGGER.info("  ");
//        LOGGER.info(" ================================================================= ");
//
//
//        return bestStrategy;
//
//    }

    private List<Bar> createBars(String symbol, Duration duration, int limit) {

        return xtbCandleDao.findAllBySymbolAndDurationAndOpenTimeAfterOrderByOpenTimeAsc(symbol, duration, LocalDateTime.of(LocalDate.ofYearDay(2019, 70), LocalTime.MIDNIGHT).toInstant(ZoneOffset.UTC))
                .map(candle -> new BaseBar(
                        duration,
                        candle.getOpenTime().atZone(ZoneId.systemDefault()),
                        Decimal.valueOf(candle.getOpen()),
                        Decimal.valueOf(candle.getHigh()),
                        Decimal.valueOf(candle.getLow()),
                        Decimal.valueOf(candle.getClose()),
                        Decimal.valueOf(candle.getVolume())))
//                .skip(600)
//                .limit(limit)
                .collect(Collectors.toList());

    }
}
