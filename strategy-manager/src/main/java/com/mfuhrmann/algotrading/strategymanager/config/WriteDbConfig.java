package com.mfuhrmann.algotrading.strategymanager.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;


@Configuration
@EnableMongoRepositories(basePackages = "com.mfuhrmann.algotrading.strategymanager.model.test", mongoTemplateRef = "writeDbMongoTemplate")
public class WriteDbConfig {

}
