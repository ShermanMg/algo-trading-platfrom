package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandle;

import java.time.*;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Strategy {

    private final Map<Duration, CandleDataSource> dataSources;
    private final Map<LocalDateTime,List<XtbCandle>> xtbCandlesFlattened;

    public Strategy(Map<Duration, CandleDataSource> dataSources) {
        this.dataSources = dataSources;
        xtbCandlesFlattened = dataSources.entrySet().stream()
                .flatMap(e -> e.getValue().getXtbCandles().values().stream())
                .collect(Collectors.groupingBy(candle -> LocalDateTime.ofInstant(candle.getOpenTime(), ZoneId.systemDefault())));
    }


    boolean shouldEnter(LocalDateTime dateTime) {

        LocalDateTime candle1Time = LocalDateTime.of(LocalDate.now(), LocalTime.of(3, 0));
        return dateTime.equals(candle1Time);
    }

    boolean shouldExit(LocalDateTime dateTime) {

        LocalDateTime candle3Time = LocalDateTime.of(LocalDate.now(), LocalTime.of(5, 0));
        return dateTime.equals(candle3Time);
    }

    public Map<Duration, CandleDataSource> getDataSources() {
        return dataSources;
    }

    public XtbCandle getCandleForLocalDateTime(LocalDateTime time) {
        return xtbCandlesFlattened.get(time).stream().findFirst()
                .orElseThrow(() -> new RuntimeException("No candle for time " + time));
    }

}
