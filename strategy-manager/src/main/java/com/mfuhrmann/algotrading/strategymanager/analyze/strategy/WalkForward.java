package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.ta4j.core.*;
import org.ta4j.core.analysis.criteria.TotalProfitCriterion;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class WalkForward {

    /**
     * Builds a list of split indexes from splitDuration.
     *
     * @param series        the time series to get split begin indexes of
     * @param splitDuration the duration between 2 splits
     * @return a list of begin indexes after split
     */
    public static List<Integer> getSplitBeginIndexes(TimeSeries series, Duration splitDuration) {
        ArrayList<Integer> beginIndexes = new ArrayList<>();

        int beginIndex = series.getBeginIndex();
        int endIndex = series.getEndIndex();

        // Adding the first begin index
        beginIndexes.add(beginIndex);

        // Building the first interval before next split
        ZonedDateTime beginInterval = series.getFirstBar().getEndTime();
        ZonedDateTime endInterval = beginInterval.plus(splitDuration);

        for (int i = beginIndex; i <= endIndex; i++) {
            // For each bar...
            ZonedDateTime barTime = series.getBar(i).getEndTime();
            if (barTime.isBefore(beginInterval) || !barTime.isBefore(endInterval)) {
                // Bar out of the interval
                if (!endInterval.isAfter(barTime)) {
                    // Bar after the interval
                    // --> Adding a new begin index
                    beginIndexes.add(i);
                }

                // Building the new interval before next split
                beginInterval = endInterval.isBefore(barTime) ? barTime : endInterval;
                endInterval = beginInterval.plus(splitDuration);
            }
        }
        return beginIndexes;
    }

    /**
     * Returns a new time series which is a view of a subset of the current series.
     * <p>
     * The new series has begin and end indexes which correspond to the bounds of the sub-set into the full series.<br>
     * The bar of the series are shared between the original time series and the returned one (i.e. no copy).
     *
     * @param series     the time series to get a sub-series of
     * @param beginIndex the begin index (inclusive) of the time series
     * @param duration   the duration of the time series
     * @return a constrained {@link TimeSeries time series} which is a sub-set of the current series
     */
    public static TimeSeries subseries(TimeSeries series, int beginIndex, Duration duration) {

        // Calculating the sub-series interval
        ZonedDateTime beginInterval = series.getBar(beginIndex).getEndTime();
        ZonedDateTime endInterval = beginInterval.plus(duration);

        // Checking bars belonging to the sub-series (starting at the provided index)
        int subseriesNbBars = 0;
        int endIndex = series.getEndIndex();
        for (int i = beginIndex; i <= endIndex; i++) {
            // For each bar...
            ZonedDateTime barTime = series.getBar(i).getEndTime();
            if (barTime.isBefore(beginInterval) || !barTime.isBefore(endInterval)) {
                // Bar out of the interval
                break;
            }
            // Bar in the interval
            // --> Incrementing the number of bars in the subseries
            subseriesNbBars++;
        }

        return series.getSubSeries(beginIndex, beginIndex + subseriesNbBars);
    }

    /**
     * Splits the time series into sub-series lasting sliceDuration.<br>
     * The current time series is splitted every splitDuration.<br>
     * The last sub-series may last less than sliceDuration.
     *
     * @param series        the time series to split
     * @param splitDuration the duration between 2 splits
     * @param sliceDuration the duration of each sub-series
     * @return a list of sub-series
     */
    public static List<TimeSeries> splitSeries(TimeSeries series, Duration splitDuration, Duration sliceDuration) {
        ArrayList<TimeSeries> subseries = new ArrayList<>();
        if (splitDuration != null && !splitDuration.isZero()
                && sliceDuration != null && !sliceDuration.isZero()) {

            List<Integer> beginIndexes = getSplitBeginIndexes(series, splitDuration);
            for (Integer subseriesBegin : beginIndexes) {
                subseries.add(subseries(series, subseriesBegin, sliceDuration));
            }
        }
        return subseries;
    }


    public Strategy run(TimeSeries series, Set<Strategy> strategies) {
        // Splitting the series into slices
        List<TimeSeries> subSeries = splitSeries(series, Duration.ofHours(6), Duration.ofDays(7));

        // Building the map of strategies

        List<Strategy> bestStrategies = new LinkedList<>();
        // The analysis criterion
        AnalysisCriterion profitCriterion = new TotalProfitCriterion();

        for (TimeSeries slice : subSeries) {
            // For each sub-series...
//            System.out.println("Sub-series: " + slice.getSeriesPeriodDescription());
            TimeSeriesManager sliceManager = new TimeSeriesManager(slice);
            for (Strategy strategy : strategies) {
                // For each strategy...
                TradingRecord tradingRecord = sliceManager.run(strategy);
                double profit = profitCriterion.calculate(slice, tradingRecord);

                if (profit > 1.5) {
//                    System.out.println("\tProfit for " + name + ": " + profit);
                }
            }
            Strategy bestStrategy = profitCriterion.chooseBest(sliceManager, new ArrayList<>(strategies));
//            System.out.println("\t\t--> Best strategy: " + strategies.get(bestStrategy) + "\n");

            bestStrategies.add(bestStrategy);

        }


        Map<Strategy, Long> map = bestStrategies.stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));


        System.out.println("NEW STRAT ");

        Long mapSize = map.values().stream().reduce((aLong, aLong2) -> aLong + aLong2).orElse(0L);

        map.entrySet().stream().sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue())).limit(3)
                .forEachOrdered(entry -> System.out.println(entry.getKey() + " " + entry.getValue() + " " + mapSize));

        System.out.println(Arrays.toString(map.entrySet().toArray()));


        return map.entrySet().stream().sorted((o1, o2) -> o2.getValue().compareTo(o1.getValue())).findFirst().orElseThrow(IllegalStateException::new).getKey();

    }


}