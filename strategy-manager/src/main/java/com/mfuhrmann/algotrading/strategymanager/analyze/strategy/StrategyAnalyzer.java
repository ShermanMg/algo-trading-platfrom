package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ta4j.core.*;
import org.ta4j.core.analysis.criteria.*;

import java.util.Set;

import static com.mfuhrmann.algotrading.strategymanager.analyze.strategy.StrategyManager.TRADE_MARGIN_COST;


@Component
public class StrategyAnalyzer {


    private final StrategyFactory strategyFactory;

    private static final Logger LOGGER = LoggerFactory.getLogger(StrategyAnalyzer.class);

    public static final Order.OrderType ORDER_TYPE = Order.OrderType.BUY;


    @Autowired
    public StrategyAnalyzer(StrategyFactory strategyFactory) {
        this.strategyFactory = strategyFactory;
    }

    public double checkStrategy(Strategy strategy, TimeSeries series, Order.OrderType orderType) {
        // Running the strategy
        return checkStrategy(strategy, series, series.getBeginIndex(), series.getEndIndex(), ORDER_TYPE);
    }

    public Strategy walkForward(TimeSeries timeSeries, Set<Strategy> strategies) {

        return new WalkForward().run(timeSeries, strategies);
    }

    public double checkStrategy(Strategy strategy, TimeSeries series, int startIndex, int endIndex, Order.OrderType buy) {
        // Running the strategy
        TimeSeriesManager seriesManager = new TimeSeriesManager(series);
        TradingRecord tradingRecord = seriesManager.run(strategy, ORDER_TYPE, Decimal.valueOf(1000), startIndex, endIndex);


        // Total profit
        TotalProfitCriterionWithTransactionCost totalProfitCriterion = new TotalProfitCriterionWithTransactionCost(TRADE_MARGIN_COST);
        double totalProfit = totalProfitCriterion.calculate(series, tradingRecord);

        //TODO change to Report instead of printout

        LOGGER.info("Number of trad es for the strategy: " + tradingRecord.getTradeCount());
        LOGGER.info("Total profit: " + (totalProfit - 1) * 100 + "%");
        // Number of bars
        LOGGER.info("Number of bars: " + new NumberOfBarsCriterion().calculate(series, tradingRecord));
//        // Average profit (per bar)
        LOGGER.info("Average profit (per bar): " + new AverageProfitCriterion().calculate(series, tradingRecord));
        // Number of trades
        LOGGER.info("Number of trades: " + new NumberOfTradesCriterion().calculate(series, tradingRecord));
        // Profitable trades ratio
        LOGGER.info("Profitable trades ratio: " + new AverageProfitableTradesCriterion().calculate(series, tradingRecord));
        // Maximum drawdown
        LOGGER.info("Maximum drawdown: " + new MaximumDrawdownCriterion().calculate(series, tradingRecord));
//        // Reward-risk ratio
//        LOGGER.info("Reward-risk ratio: " + new RewardRiskRatioCriterion().calculate(series, tradingRecord));
//        // Total transaction cost
        LOGGER.info("Total transaction cost (from $1000): " + new LinearTransactionCostCriterion(1000, TRADE_MARGIN_COST).calculate(series, tradingRecord));
        // Buy-and-hold
        LOGGER.info("Buy-and-hold: " + new BuyAndHoldCriterion().calculate(series, tradingRecord));
        LOGGER.info("Custom strategy profit vs buy-and-hold strategy profit: " + new VersusBuyAndHoldCriterion(totalProfitCriterion).calculate(series, tradingRecord));
        // Total profit vs buy-and-hold

        return totalProfit;
    }
}
