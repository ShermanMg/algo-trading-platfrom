package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandle;
import lombok.Getter;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;

@Getter
public class CandleDataSource {

    private final Duration duration;
    private final Map<LocalDateTime, XtbCandle> xtbCandles;

    public CandleDataSource(Duration duration, Map<LocalDateTime, XtbCandle> xtbCandles) {
        this.duration = duration;
        this.xtbCandles = xtbCandles;
    }







}
