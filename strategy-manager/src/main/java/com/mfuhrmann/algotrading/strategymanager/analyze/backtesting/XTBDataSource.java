package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandle;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbTick;
import com.mfuhrmann.trading.backtesting.trade.datasources.DataSource;
import com.mfuhrmann.trading.backtesting.trade.DataSourceCandle;
import com.mfuhrmann.trading.backtesting.trade.DataSourceTick;
import com.mfuhrmann.trading.backtesting.trade.strategy.StrategyParams;
import scala.collection.JavaConverters;

import java.util.List;

public class XTBDataSource implements DataSource {

    private final List<XtbCandle> xtbCandles;
    private final List<XtbTick> xtbTicks;

    public XTBDataSource(List<XtbCandle> xtbCandles,List<XtbTick> xtbTicks) {
        this.xtbCandles = xtbCandles;
        this.xtbTicks = xtbTicks;
    }


    @Override
    public StrategyParams getParams() {
        return null;
    }

    @Override
    public scala.collection.immutable.List<? extends DataSourceCandle> getCandles() {

        return JavaConverters.asScalaBufferConverter(xtbCandles).asScala().toList();

    }

    @Override
    public scala.collection.immutable.List<? extends DataSourceTick> getTicks() {
        return JavaConverters.asScalaBufferConverter(xtbTicks).asScala().toList();
    }
}

