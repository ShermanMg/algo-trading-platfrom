package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandle;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BackTest {

    private final Strategy strategy;
    private int pipsProfit;
    private int numberOfTrades;

    public BackTest(Strategy strategy) {
        this.strategy = strategy;
    }


    public int test(LocalDateTime start, LocalDateTime end) {


        List<LocalDateTime> entryDates = between(start, end)
                .filter(strategy::shouldEnter)
                .collect(Collectors.toList());


        List<LocalDateTime> exitDates = between(start, end)
                .filter(strategy::shouldExit)
                .collect(Collectors.toList());


        LocalDateTime entry = entryDates.get(0);
        LocalDateTime exit = exitDates.get(0);


        XtbCandle entryCandle = strategy.getCandleForLocalDateTime(entry);
        XtbCandle exitCandle = strategy.getCandleForLocalDateTime(exit);


        return exitCandle.getOpen().bigDecimal().subtract(entryCandle.getOpen().bigDecimal())
                .round(new MathContext(4, RoundingMode.HALF_UP))
                .multiply(BigDecimal.valueOf(10000))
                .toBigInteger().intValue();


    }


    public Stream<LocalDateTime> between(LocalDateTime startDate, LocalDateTime endDate) {
        return Stream.iterate(startDate, d -> d.plusMinutes(1))
                .limit(ChronoUnit.MINUTES.between(startDate, endDate) + 1);
    }
}
