package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.ta4j.core.*;
import org.ta4j.core.indicators.*;
import org.ta4j.core.indicators.helpers.*;
import org.ta4j.core.trading.rules.*;

import java.util.*;
import java.util.concurrent.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class StrategyFactory {

    private static final int NB_BARS_PER_WEEK = 12 * 24 * 7;
    private static final int QUEUE_CAPACITY =300_000;
    private static final Logger LOGGER = LoggerFactory.getLogger(StrategyFactory.class);

    private final RuleFactory ruleFactory;

    private final BlockingQueue<Strategy> blockingQueue = new LinkedBlockingDeque<>(QUEUE_CAPACITY);

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();


    public StrategyFactory(RuleFactory ruleFactory) {
        this.ruleFactory = ruleFactory;
    }

    public Set<Strategy> getNextStrategiesBatch() {

        return IntStream.range(0, QUEUE_CAPACITY)
                .mapToObj(i -> getNextElement())
                .takeWhile(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private Optional<Strategy> getNextElement() {
        try {
            return Optional.ofNullable(blockingQueue.poll(10, TimeUnit.SECONDS));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    private void fillQueue(Strategy strategy) {
        try {
            blockingQueue.put(strategy);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public Long buildStrategiesAsync(TimeSeries timeSeries) {

//        List<Rule> entryRules = ruleFactory.createEntryRules(timeSeries);
//        List<Rule> exitRules = ruleFactory.createExitRules(timeSeries);
        List<Rule> entryRules = List.of(new UnderIndicatorRule(new ClosePriceIndicator(timeSeries), Decimal.valueOf(11500)));
        List<Rule> exitRules= List.of(new OverIndicatorRule(new ClosePriceIndicator(timeSeries), Decimal.valueOf(11750)));

        LOGGER.info("Created " + (exitRules.size() + entryRules.size()) + " rules");

        executorService.submit(() ->
                entryRules.stream()
                        .flatMap(entryRule -> exitRules.stream()
                                .map(exitRule -> createStrategyFromRule(entryRule, exitRule)))
                        .forEach(this::fillQueue));

        LOGGER.info("Created " + exitRules.size() * exitRules.size() + " strategies");


        return (long) exitRules.size() * entryRules.size();
    }


    public Map<Strategy, String> buildStrategies(TimeSeries timeSeries) {

        List<Strategy> strategies = new LinkedList<>();

//        List<Strategy> rsiMap = getRsiCustomStrategies(timeSeries);
//
//        List<Strategy> shortLongEmaStrategies = getShortLongEmaStrategies(timeSeries);
//
//        strategies.addAll(rsiMap);
//        strategies.addAll(shortLongEmaStrategies);
//
//        strategies.add(buildMovingmomentStrategy(timeSeries));
//        strategies.add(buildCCiStrategy(timeSeries));
//        strategies.add(buildRsiStrategy(timeSeries));
//        strategies.add(buildGlobalExtremaStrategy(timeSeries));

        List<Rule> rules = ruleFactory.createEntryRules(timeSeries);

        LOGGER.info("Created " + rules.size() + " rules");


        List<Strategy> customStrategies = rules.stream().limit(1)
                .flatMap(entryRule -> rules.stream().limit(1)
                        .map(exitRule -> createStrategyFromRule(entryRule, exitRule)))
                .collect(Collectors.toList());

        strategies.addAll(customStrategies);

        LOGGER.info("Created " + strategies.size() + " strategies");

        return strategies.stream().collect(Collectors.toMap(Function.identity(), Strategy::getName));
    }

    private List<Strategy> getRsiCustomStrategies(TimeSeries timeSeries) {
        return IntStream.range(3, 100)
                .mapToObj(value -> rsiCustom(timeSeries, value))
                .collect(Collectors.toList());
    }

    private List<Strategy> getShortLongEmaStrategies(TimeSeries timeSeries) {
        return IntStream.range(1, 201)
                .filter(value -> isFibonacci(value) || value == 200)
                .mapToObj(shortEma -> IntStream.range(1, 201)
                        .filter(longEma -> longEma > shortEma)
                        .filter(value -> isFibonacci(value) || value == 200)
                        .mapToObj(longEma -> shortLongwEmaCrossing(timeSeries, shortEma, longEma)))
                .flatMap(strategyStream -> strategyStream)
                .collect(Collectors.toList());
    }

    private boolean isPerfectSquare(int x) {
        int s = (int) Math.sqrt(x);
        return (s * s == x);
    }

    // Returns true if n is a Fibonacci Number, else false
    private boolean isFibonacci(int n) {
        // n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
        // is a perfect square
        return isPerfectSquare(5 * n * n + 4) ||
                isPerfectSquare(5 * n * n - 4);
    }


    private Strategy createStrategyFromRule(Rule entryRule, Rule exitRule) {


        return new BaseStrategy(
                "Entry Rule: " + entryRule.getClass().getSimpleName()
                        + " Exit Rule: " + exitRule.getClass().getSimpleName(),
                entryRule, exitRule, 14);
    }

    public Strategy rsiCustom(TimeSeries series, int i) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        // The bias is bearish when the shorter-moving average moves below the longer moving average.

        // Entry rule
//        Rule entryRule = new OverIndicatorRule(shortEma, longEma) // Trend
//                .and(new CrossedDownIndicatorRule(stochasticOscillK, Decimal.valueOf(20))) // Signal 1
//                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        RSIIndicator rsiIndicator = new RSIIndicator(closePrice, i);

        Rule entryRule = new UnderIndicatorRule(rsiIndicator, Decimal.valueOf(25)); // Signal 1

//                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        // Exit rule
        Rule exitRule = new OverIndicatorRule(rsiIndicator, Decimal.valueOf(70))
                .or(new StopLossRule(closePrice, Decimal.valueOf(3)));

        // Signal 1

        return new BaseStrategy("RSI " + i, entryRule, exitRule);
    }


    public static Strategy shortLongwEmaCrossing(TimeSeries series, int shortEmaIndex, int longEmaIndex) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);


        EMAIndicator shortEma = new EMAIndicator(closePrice, shortEmaIndex);

        EMAIndicator longEma = new EMAIndicator(closePrice, longEmaIndex);
        // Entry rule
        Rule entryRule = new OverIndicatorRule(shortEma, longEma); // Trend

        Rule exitRule = new UnderIndicatorRule(shortEma, longEma); // Signal 1
//                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        return new BaseStrategy("Short Long Ema " + shortEma + " " + longEma, entryRule, exitRule);
    }


    public static Strategy customStrategyOld(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        // The bias is bearish when the shorter-moving average moves below the longer moving average.


        EMAIndicator shortEma = new EMAIndicator(closePrice, 10);
        EMAIndicator longEma = new EMAIndicator(closePrice, 20);

        RSIIndicator rsiIndicator = new RSIIndicator(closePrice, 24);

        MACDIndicator macd = new MACDIndicator(closePrice, 9, 26);
        EMAIndicator emaMacd = new EMAIndicator(macd, 18);

        // Entry rule
//        Rule entryRule = new OverIndicatorRule(shortEma, longEma) // Trend
//                .and(new CrossedDownIndicatorRule(stochasticOscillK, Decimal.valueOf(20))) // Signal 1
//                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        Rule entryRule = new CrossedUpIndicatorRule(shortEma, longEma); // Signal 1
//                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        // Exit rule
        Rule exitRule = new CrossedUpIndicatorRule(rsiIndicator, Decimal.valueOf(80));

        return new BaseStrategy(entryRule, exitRule);
    }


    public static Strategy buildMovingmomentStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);

        // The bias is bullish when the shorter-moving average moves above the longer moving average.
        // The bias is bearish when the shorter-moving average moves below the longer moving average.
        EMAIndicator shortEma = new EMAIndicator(closePrice, 9);
        EMAIndicator longEma = new EMAIndicator(closePrice, 26);

        StochasticOscillatorKIndicator stochasticOscillK = new StochasticOscillatorKIndicator(series, 14);

        MACDIndicator macd = new MACDIndicator(closePrice, 9, 26);
        EMAIndicator emaMacd = new EMAIndicator(macd, 18);

        // Entry rule
        Rule entryRule = new OverIndicatorRule(shortEma, longEma) // Trend
                .and(new CrossedDownIndicatorRule(stochasticOscillK, Decimal.valueOf(20))) // Signal 1
                .and(new OverIndicatorRule(macd, emaMacd)); // Signal 2

        // Exit rule
        Rule exitRule = new UnderIndicatorRule(shortEma, longEma) // Trend
                .and(new CrossedUpIndicatorRule(stochasticOscillK, Decimal.valueOf(80))) // Signal 1
                .and(new UnderIndicatorRule(macd, emaMacd)); // Signal 2

        return new BaseStrategy("buildMovingmomentStrategy", entryRule, exitRule);
    }

    public static Strategy buildCCiStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        CCIIndicator longCci = new CCIIndicator(series, 200);
        CCIIndicator shortCci = new CCIIndicator(series, 5);
        Decimal plus100 = Decimal.HUNDRED;
        Decimal minus100 = Decimal.valueOf(-100);

        Rule entryRule = new OverIndicatorRule(longCci, plus100) // Bull trend
                .and(new UnderIndicatorRule(shortCci, minus100)); // Signal

        Rule exitRule = new UnderIndicatorRule(longCci, minus100) // Bear trend
                .and(new OverIndicatorRule(shortCci, plus100)); // Signal

        Strategy strategy = new BaseStrategy("buildCCiStrategy", entryRule, exitRule);
        strategy.setUnstablePeriod(5);
        return strategy;
    }

    public static Strategy buildRsiStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
        SMAIndicator shortSma = new SMAIndicator(closePrice, 5);
        SMAIndicator longSma = new SMAIndicator(closePrice, 200);

        // We use a 2-period RSI indicator to identify buying
        // or selling opportunities within the bigger trend.
        RSIIndicator rsi = new RSIIndicator(closePrice, 2);

        // Entry rule
        // The long-term trend is up when a security is above its 200-period SMA.
        Rule entryRule = new OverIndicatorRule(shortSma, longSma) // Trend
                .and(new CrossedDownIndicatorRule(rsi, Decimal.valueOf(5))) // Signal 1
                .and(new OverIndicatorRule(shortSma, closePrice)); // Signal 2

        // Exit rule
        // The long-term trend is down when a security is below its 200-period SMA.
        Rule exitRule = new UnderIndicatorRule(shortSma, longSma) // Trend
                .and(new CrossedUpIndicatorRule(rsi, Decimal.valueOf(95))) // Signal 1
                .and(new UnderIndicatorRule(shortSma, closePrice)); // Signal 2

        // TODO: Finalize the strategy

        return new BaseStrategy("buildRsiStrategy", entryRule, exitRule);
    }

    public static Strategy buildGlobalExtremaStrategy(TimeSeries series) {
        if (series == null) {
            throw new IllegalArgumentException("Series cannot be null");
        }

        ClosePriceIndicator closePrices = new ClosePriceIndicator(series);

        // Getting the max price over the past week
        MaxPriceIndicator maxPrices = new MaxPriceIndicator(series);
        HighestValueIndicator weekMaxPrice = new HighestValueIndicator(maxPrices, NB_BARS_PER_WEEK);
        // Getting the min price over the past week
        MinPriceIndicator minPrices = new MinPriceIndicator(series);
        LowestValueIndicator weekMinPrice = new LowestValueIndicator(minPrices, NB_BARS_PER_WEEK);

        // Going long if the close price goes below the min price
        MultiplierIndicator downWeek = new MultiplierIndicator(weekMinPrice, Decimal.valueOf("1.004"));
        Rule buyingRule = new UnderIndicatorRule(closePrices, downWeek);

        // Going short if the close price goes above the max price
        MultiplierIndicator upWeek = new MultiplierIndicator(weekMaxPrice, Decimal.valueOf("0.996"));
        Rule sellingRule = new OverIndicatorRule(closePrices, upWeek);

        return new BaseStrategy("buildGlobalExtremaStrategy", buyingRule, sellingRule);
    }


}
