package com.mfuhrmann.algotrading.strategymanager.model.deutschebank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Getter
@AllArgsConstructor
@ToString
@Document(collection = "fx_rate_range")
public class FxRateRange {
    private final double low, mid, high;
    private String currency;
    private LocalDate localDate;
    private Fixing fixing;
}