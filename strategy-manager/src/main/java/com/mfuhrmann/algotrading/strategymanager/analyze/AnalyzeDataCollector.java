package com.mfuhrmann.algotrading.strategymanager.analyze;

import com.google.common.collect.Iterables;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandleDao;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbInstrumentDao;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.mfuhrmann.algotrading.strategymanager.analyze.AnalyzeDataCollector.PriceResult.*;

@Component
public class AnalyzeDataCollector {

    //    private static final BigDecimal THRESHOLD1 = BigDecimal.valueOf(0.0018);
    private static final BigDecimal THRESHOLD3 = BigDecimal.valueOf(0.002);

    private final XtbCandleDao xtbCandleDao;
    private final XtbInstrumentDao xtbInstrumentDao;

    public AnalyzeDataCollector(XtbCandleDao xtbCandleDao, XtbInstrumentDao xtbInstrumentDao) {
        this.xtbCandleDao = xtbCandleDao;
        this.xtbInstrumentDao = xtbInstrumentDao;
    }


    public List<Data> prepareDataInBatches(int trainingNumber, int resultNumber) {

        List<BigDecimal> eurusd = xtbCandleDao.findAllBySymbolAndDurationOrderByOpenTimeAsc("EURUSD", Duration.ofHours(1))
                .map(xtbCandle -> xtbCandle.getClose().bigDecimal())
                .collect(Collectors.toList());

        List<Data> data = IntStream.range(0, eurusd.size() - (trainingNumber + resultNumber))
                .mapToObj(i -> new Data(eurusd.subList(i, i + trainingNumber), eurusd.subList(i + trainingNumber - 1, i + trainingNumber + resultNumber)))
                .collect(Collectors.toList());


        Map<PriceResult, List<Data>> collect = data.stream().collect(Collectors.groupingBy(o -> o.getPriceResult()));

        System.out.println(collect.size());

        return data;
    }


    public static class Data {
        private final List<BigDecimal> trainingPrices;
        private final List<BigDecimal> resultPrices;
        private final PriceResult priceResult;


        public Data(List<BigDecimal> trainingPrices, List<BigDecimal> resultPrices) {
            this.trainingPrices = trainingPrices;
            this.resultPrices = resultPrices;
            this.priceResult = calculatePriceResult(trainingPrices, resultPrices);
        }

        private PriceResult calculatePriceResult(List<BigDecimal> trainingPrices, List<BigDecimal> resultPrices) {
            BigDecimal last = Iterables.getLast(trainingPrices);

            for (BigDecimal resultPrice : resultPrices) {
                if (resultPrice.compareTo(last.add(THRESHOLD3)) > 0) {
                    return HIGH;
                } else if (resultPrice.compareTo(last.subtract(THRESHOLD3)) < 0) {
                    return LOW;
                }
            }
            return NONE;

        }

        public PriceResult getPriceResult() {
            return priceResult;
        }

        public List<BigDecimal> getResultPrices() {
            return resultPrices;
        }

        public List<BigDecimal> getTrainingPrices() {
            return trainingPrices;
        }
    }

    public enum PriceResult {
        HIGH, NONE, LOW,
    }
}
