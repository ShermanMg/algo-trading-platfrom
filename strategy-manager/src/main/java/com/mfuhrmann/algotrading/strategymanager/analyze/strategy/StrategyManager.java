package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ta4j.core.Strategy;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.TimeSeriesManager;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.LongAdder;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class StrategyManager {

    //    public static final double TRADE_MARGIN_COST = 0.0041;
    public static final double TRADE_MARGIN_COST = 0.0000;


    private static final Logger LOGGER = LoggerFactory.getLogger(StrategyManager.class);

    private final StrategyFactory strategyFactory;

    private final StrategyAnalyzer strategyAnalyzer;
    private static final DecimalFormat df = new DecimalFormat("#.####");

    @Autowired
    public StrategyManager(StrategyFactory strategyFactory, StrategyAnalyzer strategyAnalyzer) {
        this.strategyFactory = strategyFactory;
        this.strategyAnalyzer = strategyAnalyzer;
    }


    public StrategyHolder chooseBestStrategy(TimeSeries series) {


        Long maxResults = strategyFactory.buildStrategiesAsync(series);

        LongAdder count = new LongAdder();

        List<StrategyHolder> strategyHolders = Stream.generate(strategyFactory::getNextStrategiesBatch).parallel()
                .takeWhile(strategies -> !strategies.isEmpty())
                .peek(strategies -> System.out.println("taken " + strategies.size() + " strategies"))
                .filter(strategies -> !strategies.isEmpty())
                .map(strategies -> findBestStrategy(series, maxResults, count, strategies))
                .collect(Collectors.toList());

        LOGGER.info("best strategies " + strategyHolders.size());

        Set<Strategy> strategies = strategyHolders.stream()
                .map(StrategyHolder::getStrategy)
                .collect(Collectors.toSet());

        return findBestStrategy(series, maxResults, count, strategies);

//         chartDisplayer.displayChartWithStrategy(currencyPair, series, bestStrategyInBatch, duration, 14);

//        checkBestWalkForwardStrategy(series, bestStrategyInBatch);
    }

    private StrategyHolder findBestStrategy(TimeSeries series, Long maxResults, LongAdder count, Set<Strategy> strategies) {

        count.add(strategies.size());
        displayStats(maxResults, count);

        return findBestStrategy(series, strategies);
    }

    private void displayStats(Long maxResults, LongAdder count) {

        double processedPart = (double) count.longValue() / maxResults;

        LOGGER.info("Executed " + count + " strategies out of " + maxResults + " which is " + df.format(processedPart * 100) + " %");

    }

    private StrategyHolder findBestStrategy(TimeSeries series, Set<Strategy> strategies) {
        TotalProfitCriterionWithTransactionCost totalProfitCriterion = new TotalProfitCriterionWithTransactionCost(TRADE_MARGIN_COST);

//        TotalProfitCriterion criterion = new TotalProfitCriterion ();

        Strategy bestStrategyInBatch = totalProfitCriterion.chooseBest(new TimeSeriesManager(series), new ArrayList<>(strategies));

        double profit = strategyAnalyzer.checkStrategy(bestStrategyInBatch, series, StrategyAnalyzer.ORDER_TYPE);

        return new StrategyHolder(bestStrategyInBatch, profit);
    }
}
