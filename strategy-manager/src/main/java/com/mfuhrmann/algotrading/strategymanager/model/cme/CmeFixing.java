package com.mfuhrmann.algotrading.strategymanager.model.cme;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;
import java.util.List;

@Getter
@AllArgsConstructor
@ToString
@Document(collection = "cme_fixing")
public class CmeFixing {

    @Indexed(unique = true)
    private String fileName;
    private String currency;
    private LocalDate localDate;
    private List<FixingRow> fixingRows;


    @Getter
    @AllArgsConstructor
    public static class FixingRow {
        private TENOR tenor;
        private double rate;
    }

    public enum TENOR {

        T_ON, T_TN, T_SPOT, T_1W, T_2W, T_3W, T_1M, T_2M, T_3M, T_4M, T_6M, T_9M, T_1Y, T_18M, T_2Y, T_5Y;

        static TENOR parse(String text) {
            return TENOR.valueOf("T_" + text.toUpperCase());
        }
    }
}