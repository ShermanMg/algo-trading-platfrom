package com.mfuhrmann.algotrading.strategymanager.model.cme;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.stream.Stream;

public interface CmeFixingRepository extends MongoRepository<CmeFixing, String> {


    Stream<CmeFixing> findAllByCurrency(String currency);
}
