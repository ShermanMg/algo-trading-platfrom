package com.mfuhrmann.algotrading.strategymanager.config;

import lombok.Data;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "mongodb")
public class MultipleMongoProperties {

    private MongoProperties readDB = new MongoProperties();
    private MongoProperties writeDB = new MongoProperties();
}