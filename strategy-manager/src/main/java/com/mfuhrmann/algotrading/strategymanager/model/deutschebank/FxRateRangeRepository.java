package com.mfuhrmann.algotrading.strategymanager.model.deutschebank;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FxRateRangeRepository extends MongoRepository<FxRateRange, String> {

}
