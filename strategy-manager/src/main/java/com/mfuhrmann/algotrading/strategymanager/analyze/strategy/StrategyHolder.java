package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.ta4j.core.Strategy;

public class StrategyHolder {
    private final Strategy strategy;
    private final double profit;

    public StrategyHolder(Strategy strategy, double profit) {
        this.strategy = strategy;
        this.profit = profit;
    }

    public Strategy getStrategy() {
        return strategy;
    }

    public double getProfit() {
        return profit;
    }

    @Override
    public String toString() {
        return "StrategyHolder{" +
                "strategy=" + strategy +
                ", profit=" + profit +
                '}';
    }
}