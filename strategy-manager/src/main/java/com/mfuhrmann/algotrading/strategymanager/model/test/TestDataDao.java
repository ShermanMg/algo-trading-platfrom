package com.mfuhrmann.algotrading.strategymanager.model.test;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestDataDao extends MongoRepository<TestData, String> {


}
