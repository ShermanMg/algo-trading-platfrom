package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.Rule;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.CachedIndicator;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.TripleEMAIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

import java.util.*;
import java.util.function.BiFunction;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Component
class RuleFactory {

    private final IndicatorFactory indicatorFactory;

    private static final List<Decimal> DECIMALS = IntStream.range(1, 11)
            .mapToObj(value -> Decimal.valueOf((double) value / 10))
            .collect(Collectors.toList());


    private static final List<Decimal> RSI_VALUES = IntStream.range(1, 35)
            .mapToObj(Decimal::valueOf)
            .collect(Collectors.toList());

    private static final List<Decimal> RSI_VALUES_TOP = IntStream.range(65, 99)
            .boxed().sorted(Comparator.reverseOrder())
            .map(Decimal::valueOf)
            .collect(Collectors.toList());

    @Autowired
    RuleFactory(IndicatorFactory indicatorFactory) {
        this.indicatorFactory = indicatorFactory;
    }

    List<Rule> createEntryRules(TimeSeries series) {

        List<EMAIndicator> emaIndicators = indicatorFactory.createEmaIndicators(series);
        List<SMAIndicator> smaIndicators = indicatorFactory.createSmaIndicators(series);
        List<TripleEMAIndicator> tripleEmaIndicators = indicatorFactory.createTripleEmaIndicators(series);

        List<BiFunction<Indicator<Decimal>, Indicator<Decimal>, Rule>> possibleFunctionList = getPossibleFunctions();


        List<? extends CachedIndicator<Decimal>> collect = Stream.of(emaIndicators, smaIndicators, tripleEmaIndicators)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

//        List<UnderIndicatorRule> rsiIndicators = RSI_VALUES.between()
//                .flatMap(rsiValue ->
//                        indicatorFactory.createRsiIndicators(series).between()
//                                .map(rsiIndicator -> new UnderIndicatorRule(rsiIndicator, rsiValue)))
//                .collect(Collectors.toList());

        List<UnderIndicatorRule> rsiIndicators = Collections.emptyList();

        List<Rule> rules = possibleFunctionList.stream()
                .flatMap(f2 -> createRuleFromFunction(collect, f2).stream())
                .collect(Collectors.toList());

        return Stream.of(rsiIndicators, rules)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());


//
//        List<UnderIndicatorRule> under = RSI_VALUES.between()
//                .flatMap(v -> indicators.between()
//                        .map(i -> new UnderIndicatorRule(i, v)))
//                .collect(Collectors.toList());
//
//        List<OverIndicatorRule> over = RSI_VALUES_TOP.between()
//                .flatMap(v -> indicators.between()
//                        .map(i -> new OverIndicatorRule(i, v)))
//                .collect(Collectors.toList());


//
//        return Stream.concat(under.between(), over.between())
//                .collect(Collectors.toList());


//        List<Rule> customIndicators = possibleFunctionList.between()
//                .flatMap(f1 -> possibleFunctionList.between()
//                        .flatMap(f2 -> createRuleFromFunction(indicators, f2).between()))
//                .collect(Collectors.toList());
//

    }

    List<Rule> createExitRules(TimeSeries series) {

//        StopLossRule stopLossRule = new StopLossRule(new ClosePriceIndicator(series), Decimal.valueOf(3));

        List<EMAIndicator> emaIndicators = indicatorFactory.createEmaIndicators(series);
        List<SMAIndicator> smaIndicators = indicatorFactory.createSmaIndicators(series);
        List<TripleEMAIndicator> tripleEmaIndicators = indicatorFactory.createTripleEmaIndicators(series);

        List<BiFunction<Indicator<Decimal>, Indicator<Decimal>, Rule>> possibleFunctionList = getPossibleFunctions();

        List<? extends CachedIndicator<Decimal>> collect = Stream.of(emaIndicators, smaIndicators, tripleEmaIndicators)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

//        List<OverIndicatorRule> rsiRules = RSI_VALUES_TOP.between().flatMap(rsiValue ->
//                indicatorFactory.createRsiIndicators(series).between()
//                        .map(rsiIndicator -> new OverIndicatorRule(rsiIndicator, rsiValue)))
//                .collect(Collectors.toList());

        List<OverIndicatorRule> rsiRules = Collections.emptyList();

        return possibleFunctionList.stream()
                .flatMap(f2 -> createRuleFromFunction(collect, f2).stream())
                .collect(Collectors.toList());

//        return Stream.of(rsiRules, rules)
//                .flatMap(Collection::between)
//                .map(r -> new AndRule(stopLossRule, r))
//                .collect(Collectors.toList());

//
//        List<UnderIndicatorRule> under = RSI_VALUES.between()
//                .flatMap(v -> indicators.between()
//                        .map(i -> new UnderIndicatorRule(i, v)))
//                .collect(Collectors.toList());
//
//        List<OverIndicatorRule> over = RSI_VALUES_TOP.between()
//                .flatMap(v -> indicators.between()
//                        .map(i -> new OverIndicatorRule(i, v)))
//                .collect(Collectors.toList());


//
//        return Stream.concat(under.between(), over.between())
//                .collect(Collectors.toList());


//        List<Rule> customIndicators = possibleFunctionList.between()
//                .flatMap(f1 -> possibleFunctionList.between()
//                        .flatMap(f2 -> createRuleFromFunction(indicators, f2).between()))
//                .collect(Collectors.toList());
//

    }

    @NotNull
    private List<BiFunction<Indicator<Decimal>, Indicator<Decimal>, Rule>> getPossibleFunctions() {
        List<BiFunction<Indicator<Decimal>, Indicator<Decimal>, Rule>> possibleFunctionList = new ArrayList<>();
        possibleFunctionList.add(OverIndicatorRule::new);
        possibleFunctionList.add(UnderIndicatorRule::new);
        possibleFunctionList.add(CrossedUpIndicatorRule::new);
        possibleFunctionList.add(CrossedDownIndicatorRule::new);
        return possibleFunctionList;
    }

    private List<Rule> createOverIndicatorRules(List<Indicator<Decimal>> indicators) {

        List<OverIndicatorRule> rulesWithDecimal = DECIMALS.stream().flatMap(decimal ->
                indicators.stream().map(indicator -> new OverIndicatorRule(indicator, decimal)))
                .collect(Collectors.toList());


        List<OverIndicatorRule> rulesWithRules = indicators.stream().flatMap(indicator1 ->
                indicators.stream().filter(i -> !i.equals(indicator1)).map(indicator2 -> new OverIndicatorRule(indicator1, indicator2)))
                .collect(Collectors.toList());

        return Stream.concat(rulesWithDecimal.stream(), rulesWithRules.stream()).collect(Collectors.toList());
    }

    private List<Rule> createRuleFromFunction(List<? extends Indicator<Decimal>> indicators, BiFunction<Indicator<Decimal>, Indicator<Decimal>, Rule> function) {

//        List<Rule> rulesWithDecimal = DECIMALS.between()
//                .flatMap(decimal -> indicators.between()
//                        .map(indicator -> new OverIndicatorRule(indicator, decimal)))
//                .collect(Collectors.toList());
        List<Rule> rulesWithDecimal = Collections.emptyList();


        List<Rule> rulesWithRules = indicators.stream()
                .flatMap(indicator1 -> indicators.stream()
                        .filter(i -> !i.equals(indicator1))
                        .map(indicator2 -> function.apply(indicator1, indicator2)))
                .collect(Collectors.toList());

        return Stream.concat(rulesWithDecimal.stream(), rulesWithRules.stream()).collect(Collectors.toList());
    }


}
