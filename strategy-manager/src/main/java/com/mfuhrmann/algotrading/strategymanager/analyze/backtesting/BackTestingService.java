package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.google.common.base.Stopwatch;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.*;
import com.mfuhrmann.trading.backtesting.trade.datasources.DataSource;
import com.mfuhrmann.trading.backtesting.trade.portfolio.BackTestingTradesExecutor;
import com.mfuhrmann.trading.backtesting.trade.portfolio.SimpleForexPortfolio;
import com.mfuhrmann.trading.backtesting.trade.results.StrategyComparator;
import com.mfuhrmann.trading.backtesting.trade.risk.CurrencyExposureRiskCalculator;
import com.mfuhrmann.trading.backtesting.trade.strategy.Strategy;
import com.mfuhrmann.trading.backtesting.trade.strategy.sma.SmaStrategy;
import com.mfuhrmann.trading.backtesting.trade.strategy.sma.SmaStrategyParams;
import com.mfuhrmann.trading.backtesting.trade.utils.Money;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import scala.collection.JavaConverters;
import scala.math.BigDecimal;

import javax.annotation.PostConstruct;
import java.time.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

@Service
public class BackTestingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackTestingService.class);

    private final XtbCandleDao xtbCandleDao;
    private final XtbTickDao xtbTickDao;
    private final XtbTickClearedDao xtbTickClearedDao;
    private final XtbInstrumentService xtbInstrumentService;
    private final CsvWriter csvWriter;


    public BackTestingService(XtbCandleDao xtbCandleDao, XtbTickDao xtbTickDao, XtbTickClearedDao xtbTickClearedDao, XtbInstrumentService xtbInstrumentService, CsvWriter csvWriter) {
        this.xtbCandleDao = xtbCandleDao;
        this.xtbTickDao = xtbTickDao;
        this.xtbTickClearedDao = xtbTickClearedDao;
        this.xtbInstrumentService = xtbInstrumentService;
        this.csvWriter = csvWriter;
    }


    @PostConstruct
    public void init() {


        Set<String> currencies = Set.of("EUR", "USD", "JPY", "GBP", "CHF", "CAD", "AUD", "NZD");
//        Set<String> currencies = Set.of("GBP", "AUD");

//        AtomicLong counter = new AtomicLong();

//        Long count = xtbTickDao.count();
//        fixingQuoteLevel(counter, count);


        Stopwatch stopwatch = Stopwatch.createStarted();

//        Map<Instant, List<XtbCandle>> collect = getCandlesGroupedByTime(currencies);
        List<XtbTick> ticks = getTicks(currencies);
        LOGGER.info("Queried ticks: " + ticks.size());

//        List<XtbCandle> candles = ticks.entrySet().stream()
//                .flatMap(e -> e.getValue().stream())
//                .collect(Collectors.toList());


        testDifferentStrategies(ticks);

//        csvWriter.writeToCsv(candles, result);
//        LOGGER.info("Strategy : " + result);

        LOGGER.info("Calculation took " + stopwatch.elapsed(TimeUnit.MILLISECONDS) + " ms");

    }

    private void fixingQuoteLevel(AtomicLong counter, Long count) {
        xtbTickDao.findAll().stream()
                .collect(Collectors.groupingBy(XtbTick::getMarketSymbol, Collectors.groupingBy(XtbTick::getTime)))
                .forEach((key, value) -> value
                        .forEach((key1, value1) -> value1.stream()
                                .sorted(Comparator.comparing(t -> t.getAsk().subtract(t.getBid())))
                                .peek(tick -> {
                                    if (counter.incrementAndGet() % 10_000 == 0) {
                                        System.out.println(" " + (counter.doubleValue() / count));
                                    }
                                })
                                .findFirst().ifPresent(tickZero -> xtbTickClearedDao.save(new XtbTickCleared(tickZero)))));
    }

    private Map<Instant, List<XtbCandle>> getCandlesGroupedByTime(Set<String> currencies) {
        return xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> currencies.contains(xtbInstrument.getSymbol().substring(0, 3)) && currencies.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .flatMap(fxInstrument -> xtbCandleDao.findAllBySymbolAndDurationAndOpenTimeAfterOrderByOpenTimeAsc(fxInstrument.getSymbol(), Duration.ofMinutes(1), LocalDateTime.now().minusDays(5).toInstant(ZoneOffset.UTC)))
                .collect(Collectors.groupingBy(XtbCandle::getOpenTime));
    }

    private List<XtbTick> getTicks(Set<String> instruments) {
        return xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> instruments.contains(xtbInstrument.getSymbol().substring(0, 3)) && instruments.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .flatMap(fxInstrument -> xtbTickDao.findAllBySymbolAndTimeBetweenOrderByTime(
                        fxInstrument.getSymbol(),
                        LocalDateTime.of(LocalDate.of(2019, 6, 10), LocalTime.of(1, 0)).toInstant(ZoneOffset.UTC),
                        LocalDateTime.of(LocalDate.of(2019, 6, 10), LocalTime.of(22, 0)).toInstant(ZoneOffset.UTC)))
                .sorted(Comparator.comparing(XtbTick::getTime))
                .collect(Collectors.toList());
    }


//    private StrategyResult testSmatrategy(Map<Instant, List<XtbCandle>> candles) {
//
//        List<XtbCandle> collect = candles.entrySet().stream()
//                .sorted(Map.Entry.comparingByKey())
//                .flatMap(e -> e.getValue().stream())
//                .collect(Collectors.toList());
//
//        DataSource xtbDataSource = new XTBDataSource(collect, Collections.emptyList());
//
//        SimpleForexPortfolio portfolio = new SimpleForexPortfolio(new Money(BigDecimal.valueOf(10_000), Currency.getInstance("PLN")), 30, new BackTestingTradesExecutor());
//
//        SmaStrategy strategy = new SmaStrategy(portfolio, new CurrencyExposureRiskCalculator(portfolio), new SmaIndicator(100), new StrategyParams(0.35, 200));
//
//        StrategyBackTester strategyBackTester = new StrategyBackTester(strategy, portfolio);
//
//        StrategyResult run = strategyBackTester.run(xtbDataSource.getTicks());
//
//        StrategyStatsCollector strategyStatsCollector = strategy.statsCollector();
//
//        LOGGER.info(strategyStatsCollector.toString());
//
//        return run;
//
//    }

    private void testDifferentStrategies(List<XtbTick> ticks) {


        DataSource xtbDataSource = new XTBDataSource(Collections.emptyList(), ticks);
//
        List<Strategy> strategies = generateStrategies();
//        List<Strategy> strategies = testCustomStrategies();
        LOGGER.info("Generated " + strategies.size() + " strategies");


        StrategyComparator strategyComparator = new StrategyComparator(JavaConverters.asScalaBuffer(strategies).toList(), xtbDataSource);


        strategyComparator.run();


    }


//    private List<Strategy> testCustomStrategies() {
////StrategyParams(0.3, 150, 6.0E-4, 150)
//        SimpleForexPortfolio defaultPortfolio = createDefaultPortfolio();
//        SmaStrategy smaStrategy = new SmaStrategy(defaultPortfolio, new CurrencyExposureRiskCalculator(defaultPortfolio), new StrategyParams(0.3, 150, 0.0006, 150, 4, 10));
//
//        return List.of(smaStrategy);


//    }

    private List<Strategy> generateStrategies() {


        List<Strategy> collect = getRisksStream()
                .flatMap(risk ->
                        getTicksCount().flatMap(tick ->
                                getDelta().flatMap(delta ->
                                        getSLValues().flatMap(sl ->
                                                getTPValues().
                                                        filter(tp -> tp > sl * 2)
                                                        .map(tp -> {
                                                                    SimpleForexPortfolio defaultPortfolio = createDefaultPortfolio();
                                                                    return new SmaStrategy(defaultPortfolio, new CurrencyExposureRiskCalculator(defaultPortfolio), new SmaStrategyParams(risk, tick, delta, tick, sl, tp));
                                                                }

                                                        )))))
//                .limit(20)
                .collect(Collectors.toList());

        Collections.shuffle(collect);

        return collect;


    }


    private Stream<Integer> getTPValues() {
        return Stream.of(9, 14, 18, 25);
    }


    private Stream<Integer> getSLValues() {
        return Stream.of(3, 6, 8, 12);
    }

    @NotNull
    private SimpleForexPortfolio createDefaultPortfolio() {
        return new SimpleForexPortfolio(new Money(BigDecimal.valueOf(10_000), Currency.getInstance("PLN")), 30, new BackTestingTradesExecutor());
    }

    @NotNull
    private Stream<Double> getDelta() {
        return Stream.of(5, 7, 10, 15)
                .mapToDouble(i -> i / (double) 10000)
                .boxed();

    }

    @NotNull
    private Stream<Integer> getTicksCount() {
        return Stream.of(100, 400, 800, 1600);
    }

    @NotNull
    private Stream<Double> getRisksStream() {
        return IntStream.rangeClosed(0, 40)
                .filter(i -> i % 15 == 0)
                .mapToDouble(i -> i / (double) 100)
                .boxed();
    }

//    private StrategyResult testStrategy(List<XtbCandle> eurusd, Strategy strategy) {
//        DataSource xtbDataSource = new XTBDataSource(eurusd);
//
//
//        SimpleForexPortfolio portfolio = new SimpleForexPortfolio(new Money(BigDecimal.valueOf(10_000), Currency.getInstance("PLN")), 30, new BackTestingTradesExecutor());
//
//        RandomStrategy randomStrategy = new RandomStrategy(portfolio, new CurrencyExposureRiskCalculator(portfolio), new StrategyParams(0.35));
//
//        StrategyBackTester strategyBackTester = new StrategyBackTester(xtbDataSource, randomStrategy, portfolio);
//
//        StrategyResult run = strategyBackTester.run();
//
//        StrategyStatsCollector strategyStatsCollector = randomStrategy.statsCollector();
//
//        LOGGER.info(strategyStatsCollector.toString());
//
//        return run;
//    }

}
