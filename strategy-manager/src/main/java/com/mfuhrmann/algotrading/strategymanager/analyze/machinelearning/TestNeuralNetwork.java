package com.mfuhrmann.algotrading.strategymanager.analyze.machinelearning;

import com.mfuhrmann.algotrading.strategymanager.analyze.AnalyzeDataCollector;
import org.datavec.api.records.reader.impl.collection.CollectionSequenceRecordReader;
import org.datavec.api.writable.DoubleWritable;
import org.datavec.api.writable.IntWritable;
import org.datavec.api.writable.Writable;
import org.deeplearning4j.api.storage.StatsStorage;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.PerformanceListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.ui.stats.StatsListener;
import org.deeplearning4j.ui.storage.InMemoryStatsStorage;
import org.deeplearning4j.util.ModelSerializer;
import org.math.plot.Plot2DPanel;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerStandardize;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

@Component
public class TestNeuralNetwork {


    public static final int seed = 34560;
    //Number of epochs (full passes of the data)
    public static final int nEpochs = 50000;
    //Number of data points
    public static final int batchSize = 500;
    //Network learning rate
    public static final double learningRate = 0.01;

    private static final int numInputs = 100;
    private static final int numOutputs = 3;
    private static final int numHiddenNodes = 200;
    public static final int TRADE_PERIOD = 10;

    public static final double L_2 = 0.00001;

    private final AnalyzeDataCollector analyzeDataCollector;

    @Autowired
    public TestNeuralNetwork(AnalyzeDataCollector analyzeDataCollector) {
        this.analyzeDataCollector = analyzeDataCollector;
    }


//    @PostConstruct
    public void init() {
        List<AnalyzeDataCollector.Data> rawData = analyzeDataCollector.prepareDataInBatches(numInputs, TRADE_PERIOD);
//        List<AnalyzeDataCollector.Data> data = autoGenreatedData();

//
        Map<AnalyzeDataCollector.PriceResult, List<AnalyzeDataCollector.Data>> rawCollect = rawData.stream().collect(groupingBy(d -> d.getPriceResult()));

        Integer score = rawData.stream().collect(groupingBy(d -> d.getPriceResult())).entrySet().stream()
                .min(Comparator.comparing(o -> o.getValue().size()))
                .map(e -> e.getValue().size())
                .orElse(0);

        List<AnalyzeDataCollector.Data> data = Arrays.stream(AnalyzeDataCollector.PriceResult.values())
                .flatMap(priceResult -> rawCollect.get(priceResult).stream()
                        .limit(score))
                .collect(toList());

        Map<AnalyzeDataCollector.PriceResult, List<AnalyzeDataCollector.Data>> collect = data.stream().collect(groupingBy(d -> d.getPriceResult()));
        collect.forEach((key, value) -> System.out.println(key + " " + value.size()));

        AtomicInteger atomicInteger = new AtomicInteger();
        data.stream()
                .filter(d -> atomicInteger.incrementAndGet() % 100 == 0)
                .forEach(this::drawPlotFromData);

        Collections.shuffle(data);

        int splitIndex = (int) (data.size() * 0.8);

        List<AnalyzeDataCollector.Data> trainData = data.subList(0, splitIndex);

        System.out.println(trainData.stream().collect(groupingBy(o -> o.getPriceResult(), Collectors.counting())));

        List<AnalyzeDataCollector.Data> testData = data.subList(splitIndex, data.size());

        System.out.println(testData.stream().collect(groupingBy(o -> o.getPriceResult(), Collectors.counting())));

        List<List<List<Writable>>> trainListFeatues = transformDataToTripleListFeatures(trainData);
        List<List<List<Writable>>> trainListLabels = transformDataToTripleListLabels(trainData);

        List<List<List<Writable>>> testListFeatures = transformDataToTripleListFeatures(testData);
        List<List<List<Writable>>> testListLabels = transformDataToTripleListLabels(testData);

        SequenceRecordReaderDataSetIterator inputIterator = new SequenceRecordReaderDataSetIterator(
                new CollectionSequenceRecordReader(trainListFeatues),
                new CollectionSequenceRecordReader(trainListLabels),
                batchSize, numOutputs);

        SequenceRecordReaderDataSetIterator testIterator = new SequenceRecordReaderDataSetIterator(
                new CollectionSequenceRecordReader(testListFeatures),
                new CollectionSequenceRecordReader(testListLabels),
                batchSize, numOutputs);

        DataNormalization normalizer = new NormalizerStandardize();
        normalizer.fit(inputIterator);              //Collect training data statistics
        inputIterator.reset();
        inputIterator.setPreProcessor(normalizer);

        testIterator.setPreProcessor(normalizer);

        try {
            executeNetwork(inputIterator, testIterator);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    private void executeNetwork(DataSetIterator trainDataIter, DataSetIterator testDataIter) throws IOException {
//        UIServer uiServer = UIServer.getInstance();
        StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for saving and loading later
//        uiServer.attach(statsStorage);


        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
//                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//                .learningRate(learningRate)
                .updater(new Nesterovs(learningRate))
                .seed(seed)
                .iterations(1)
                //To configure: .updater(new Nesterovs(0.9))
                .list()
                .layer(0, new GravesLSTM.Builder()
                        .nIn(numInputs)
                        .nOut(numHiddenNodes)
                        .activation(Activation.TANH)
                        .weightInit(WeightInit.XAVIER)
                        .l2(L_2)
                        .build())
                .layer(1, new DropoutLayer.Builder(0.5)
                        .weightInit(WeightInit.XAVIER)
                        .build())
                .layer(2, new DenseLayer.Builder()
                        .nIn(numHiddenNodes)
                        .nOut(numHiddenNodes)
                        .weightInit(WeightInit.XAVIER)
                        .l2(L_2)
                        .activation(Activation.TANH).build())
                .layer(3, new DenseLayer.Builder()
                        .nIn(numHiddenNodes)
                        .nOut(numHiddenNodes)
                        .weightInit(WeightInit.XAVIER)
                        .l2(L_2)
                        .activation(Activation.TANH).build())
                .layer(4, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .nIn(numHiddenNodes)
                        .nOut(numOutputs)
                        .activation(Activation.SOFTMAX)
                        .weightInit(WeightInit.XAVIER)
                        .l2(L_2)
                        .build())
                .pretrain(false).backprop(true).build();


        MultiLayerConfiguration confOld = new NeuralNetConfiguration.Builder()
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
//                .updater(new Sgd(learningRate))
                .learningRate(learningRate)
                .seed(seed)
                .iterations(1)
                //To configure: .updater(new Nesterovs(0.9))
                .list()
                .layer(0, new GravesLSTM.Builder()
                        .nIn(numInputs)
                        .nOut(numHiddenNodes)
                        .activation(Activation.SOFTSIGN)
                        .weightInit(WeightInit.DISTRIBUTION)
                        .build())
                .layer(1, new GravesLSTM.Builder()
                        .nIn(numHiddenNodes)
                        .nOut(numHiddenNodes)
                        .activation(Activation.SOFTSIGN)
                        .weightInit(WeightInit.DISTRIBUTION)
                        .build())
                .layer(2, new DropoutLayer.Builder(0.2).build())
                .layer(3, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MCXENT)
                        .weightInit(WeightInit.DISTRIBUTION)
                        .activation(Activation.SOFTMAX)
                        .nIn(numHiddenNodes)
                        .nOut(numOutputs)
                        .build())
                .pretrain(false).backprop(true).build();

        MultiLayerNetwork model = new MultiLayerNetwork(conf);
        model.init();
        model.setListeners(new ScoreIterationListener(50), new PerformanceListener(50), new StatsListener(statsStorage));

        String str = "Test set evaluation at epoch %d: Accuracy = %.2f, F1 = %.2f";


        long now = Instant.now().getEpochSecond();

        for (int epoch = 0; epoch < nEpochs; epoch++) {
            model.fit(trainDataIter);

//            Evaluate on the test set:
            Evaluation evaluation = model.evaluate(testDataIter);

            if (epoch % 50 == 0) {

                System.out.println(String.format(str, epoch, evaluation.accuracy(), evaluation.f1()));
                System.out.println(evaluation.stats());
            }

            trainDataIter.reset();
            testDataIter.reset();
            if (epoch % 1000 == 0) {
                System.out.println("Saving model : model-" + epoch + "-" + now + ".ml");

                ModelSerializer.writeModel(model, new File("models/model-" + epoch + "-" + now + ".ml"), true);

            }
        }
        System.out.println("FINISHED");

        Evaluation evaluation = model.evaluate(testDataIter);


        //Print the evaluation statistics
        System.out.println(evaluation.stats());
    }

    private List<AnalyzeDataCollector.Data> autoGenreatedData() {

        Stream<AnalyzeDataCollector.Data> dataStream = IntStream.range(0, numInputs)
                .mapToObj(i -> new AnalyzeDataCollector.Data(
                        IntStream.range(i, i + numInputs)
                                .mapToObj(BigDecimal::valueOf)
                                .map(d -> BigDecimal.ONE.add(d.divide(BigDecimal.valueOf(10), MathContext.DECIMAL64)))
                                .collect(Collectors.toList()),
                        highResult(i)
                ));


        Stream<AnalyzeDataCollector.Data> dataStreamReversed = IntStream.range(numInputs + 5, 5 + numInputs + numInputs)
                .boxed().sorted(Comparator.reverseOrder())
                .map(i -> new AnalyzeDataCollector.Data(
                        IntStream.range(i - numInputs, i)
                                .boxed().sorted(Comparator.reverseOrder())
                                .map(BigDecimal::valueOf)
                                .map(d -> BigDecimal.ONE.add(d.divide(BigDecimal.valueOf(10), MathContext.DECIMAL64)))
                                .collect(Collectors.toList()),

                        lowResult(i)
                ));


        return Stream.concat(dataStream, dataStreamReversed).collect(Collectors.toList());
    }

    private List<BigDecimal> lowResult(Integer i) {
        return IntStream.range(i - numInputs - 4, i - numInputs).boxed()
                .sorted(Comparator.reverseOrder())
                .map(BigDecimal::valueOf)
                .map(d -> BigDecimal.ONE.add(d.divide(BigDecimal.valueOf(10), MathContext.DECIMAL64)))
                .collect(Collectors.toList());
    }

    private List<BigDecimal> highResult(int i) {
        return IntStream.range(i + numInputs, i + numInputs + 4)
                .mapToObj(BigDecimal::valueOf)
                .map(d -> BigDecimal.ONE.add(d.divide(BigDecimal.valueOf(10), MathContext.DECIMAL64)))
                .collect(Collectors.toList());
    }

    private List<List<Writable>> transformDataFeatures(List<AnalyzeDataCollector.Data> data) {

        return data.stream().map(this::getWritablesFeatures)
                .collect(Collectors.toList());

    }

    private List<List<Writable>> transformDataLabels(List<AnalyzeDataCollector.Data> data) {

        return data.stream().map(this::getWritablesLabels)
                .collect(Collectors.toList());

    }

    private List<List<List<Writable>>> transformDataToTripleListFeatures(List<AnalyzeDataCollector.Data> data) {

        return data.stream().map(d -> Collections.singletonList(getWritablesFeatures(d)))
                .collect(Collectors.toList());

    }

    private List<List<List<Writable>>> transformDataToTripleListLabels(List<AnalyzeDataCollector.Data> data) {

        return data.stream().map(d -> Collections.singletonList(getWritablesLabels(d)))
                .collect(Collectors.toList());

    }

    private void drawPlotFromData(AnalyzeDataCollector.Data data) {
        double[] y = data.getTrainingPrices().stream().mapToDouble(BigDecimal::doubleValue).toArray();
        double[] y2 = data.getResultPrices().stream().mapToDouble(BigDecimal::doubleValue).toArray();

        drawPlot(y, y2, data);
    }

    private void drawPlot(double[] y, double[] y2, AnalyzeDataCollector.Data data) {

        Plot2DPanel plot = new Plot2DPanel();

        plot.addLinePlot("my plot", Color.GREEN, y);
        plot.addLinePlot("my plot", Color.BLUE, y2);

        // put the PlotPanel in a JFrame, as a JPanel
        JFrame frame = new JFrame(data.getPriceResult().name());
        frame.setContentPane(plot);
        frame.setVisible(true);
        frame.setSize(new Dimension(1024, 800));
    }

    private List<Writable> getWritablesFeatures(AnalyzeDataCollector.Data data) {


        Stream<Writable> a = data.getTrainingPrices().stream()
                .map(d -> new DoubleWritable(d.doubleValue()));

        return a
                .collect(Collectors.toList());
    }


    private List<Writable> getWritablesLabels(AnalyzeDataCollector.Data data) {

        return Stream.of(new IntWritable(data.getPriceResult().ordinal()))
                .collect(Collectors.toList());
    }

    private List<Writable> getWritables(AnalyzeDataCollector.Data data) {


        Stream<Writable> a = data.getTrainingPrices().stream()
                .map(d -> new DoubleWritable(d.doubleValue() * 100000));

        Stream<Writable> b = Stream.of(new IntWritable(data.getPriceResult().ordinal()));
        return Stream.concat(a, b)
                .collect(Collectors.toList());
    }
}
