package com.mfuhrmann.algotrading.strategymanager.analyze.strategy.runners;

import com.mfuhrmann.algotrading.commons.CurrencyPair;
import com.mfuhrmann.algotrading.strategymanager.analyze.strategy.*;
import com.mfuhrmann.algotrading.strategymanager.model.deutschebank.FxRateRange;
import com.mfuhrmann.algotrading.strategymanager.model.deutschebank.FxRateRangeRepository;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandleDao;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.ta4j.core.*;
import org.ta4j.core.indicators.EMAIndicator;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.SMAIndicator;
import org.ta4j.core.indicators.bollinger.BollingerBandsLowerIndicator;
import org.ta4j.core.indicators.bollinger.BollingerBandsMiddleIndicator;
import org.ta4j.core.indicators.bollinger.BollingerBandsUpperIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.indicators.statistics.StandardDeviationIndicator;
import org.ta4j.core.trading.rules.CrossedDownIndicatorRule;
import org.ta4j.core.trading.rules.CrossedUpIndicatorRule;
import org.ta4j.core.trading.rules.OverIndicatorRule;
import org.ta4j.core.trading.rules.UnderIndicatorRule;

import java.text.DecimalFormat;
import java.time.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


@Component
public class TechnicalAnalysisService {

    private final XtbCandleDao xtbCandleDao;
    private final StrategyFactory strategyFactory;
    private final StrategyAnalyzer strategyAnalyzer;
    private final StrategyManager strategyManager;
    private final ChartDisplayer chartDisplayer;
    private final FxRateRangeRepository fxRateRangeRepository;

    private static final DecimalFormat df = new DecimalFormat("#.####");


    private static final Logger LOGGER = LoggerFactory.getLogger(TechnicalAnalysisService.class);


    public TechnicalAnalysisService(XtbCandleDao xtbCandleDao, StrategyFactory strategyFactory, StrategyAnalyzer strategyAnalyzer, StrategyManager strategyManager, ChartDisplayer chartDisplayer, FxRateRangeRepository fxRateRangeRepository) {
        this.xtbCandleDao = xtbCandleDao;
        this.strategyFactory = strategyFactory;
        this.strategyAnalyzer = strategyAnalyzer;
        this.strategyManager = strategyManager;
        this.chartDisplayer = chartDisplayer;
        this.fxRateRangeRepository = fxRateRangeRepository;
    }


//    @Scheduled(fixedRate = 360000000)
    public void manualAnalysisWithDeutscheBankData() {

        List<String> currencies = fxRateRangeRepository.findAll().stream()
                .map(FxRateRange::getCurrency)
                .filter(currency->!currency.equals("EURPLN") && !currency.equals("EURGBP"))
                .distinct()
                .collect(Collectors.toList());


        currencies.forEach(c -> displayChartForCurrency(c));

//
//        StrategyHolder bestStrategy = displayChartWithStrategy(duration, baseTimeSeries, null);
//
//        LOGGER.info("Checking on test data");
//        new ArrayList<>(bars.subList(split, bars.size())).forEach(baseTimeSeries::addBar);
//
//        strategyAnalyzer.checkStrategy(bestStrategy.getStrategy(), new BaseTimeSeries(bars), split, bars.size() - 2,StrategyAnalyzer.ORDER_TYPE);

//        chartDisplayer.displayChartWithStrategy(new CurrencyPair("GOLD", "USD"), baseTimeSeries, bestStrategy.getStrategy(), duration, 14);


//        LOGGER.info("Checking on test data");
//        new ArrayList<>(bars.subList(split, bars.size())).forEach(baseTimeSeries::addBar);
        // Close price
//        IntStream.of(10, 12, 14, 17, 21, 24, 30).forEach(period -> {
//
//            BaseStrategy baseStrategy = create(baseTimeSeries, period);
//
////            Strategy strategy = RSI2Strategy.buildStrategy(baseTimeSeries);
//
//            StrategyHolder bestStrategy = displayChartWithStrategy(duration, baseTimeSeries, new StrategyHolder(baseStrategy, 1.0));
//
//            LOGGER.info("Checking on test data");
//
//            strategyAnalyzer.checkStrategy(bestStrategy.getStrategy(), new BaseTimeSeries(bars), split, bars.size() - 2, StrategyAnalyzer.ORDER_TYPE);
//
////            chartDisplayer.displayChartWithStrategy(new CurrencyPair("GOLD", "USD"), baseTimeSeries, bestStrategy.getStrategy(), duration, 14);
//
//            System.out.println(" NEW STRATEGY !!!!!!!!!!!!!!!!!!!!!!!!!");
//
//        });


    }

    @NotNull
    private BaseStrategy create(BaseTimeSeries baseTimeSeries, int period) {

        ClosePriceIndicator closePrice = new ClosePriceIndicator(baseTimeSeries);
        EMAIndicator avg14 = new EMAIndicator(closePrice, period);
        StandardDeviationIndicator sd14 = new StandardDeviationIndicator(closePrice, period);

        // Bollinger bands
        BollingerBandsMiddleIndicator middleBBand = new BollingerBandsMiddleIndicator(avg14);
        BollingerBandsLowerIndicator lowBBand = new BollingerBandsLowerIndicator(middleBBand, sd14);
        BollingerBandsUpperIndicator upBBand = new BollingerBandsUpperIndicator(middleBBand, sd14);


        Rule entryRule = new CrossedDownIndicatorRule(new ClosePriceIndicator(baseTimeSeries), lowBBand);
        Rule exitRule = new CrossedUpIndicatorRule(new ClosePriceIndicator(baseTimeSeries), upBBand);

        return new BaseStrategy(entryRule, exitRule);
    }

    private void displayChartForCurrency(String currency) {
        Duration duration = Duration.ofHours(1);

        List<Bar> bars = createBars(currency, duration, 2000);

        if (bars.isEmpty()) {
            throw new IllegalArgumentException("Wrong data");
        }

//        int split = (int) (bars.size() * 0.5);
//        List<Bar> trainBars = new ArrayList<>(bars.subList(0, (split)));


        BaseTimeSeries baseTimeSeries = new BaseTimeSeries(bars);

        chartDisplayer.displayChart(new CurrencyPair(currency.substring(0, 3), currency.substring(3, 6)), baseTimeSeries, duration);
    }


    public StrategyHolder displayChart(Duration duration, TimeSeries series, StrategyHolder bestStrategy2) {

        LOGGER.info("Building strategies for duration " + duration);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start();


        StrategyHolder bestStrategy = strategyManager.chooseBestStrategy(series);
//
        LOGGER.info(" ************************************************************ ");
        LOGGER.info("");
        LOGGER.info("");
        LOGGER.info("");

        LOGGER.info("Best strategy " + bestStrategy.getStrategy().getName());

        strategyAnalyzer.checkStrategy(bestStrategy.getStrategy(), series, StrategyAnalyzer.ORDER_TYPE);
        stopWatch.stop();

        LOGGER.info("Total time " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTotalTimeMillis()) + " seconds");

        LOGGER.info(" ************************************************************ ");


//        Strategy bestWalkForwardStrategy = checkBestWalkForwardStrategy(allSeries, strategies);

//        chartDisplayer.displayChartWithStrategy(new CurrencyPair("DE30", "PLN"), series, bestStrategy.getStrategy(), duration, 14);

        LOGGER.info("  ");
        LOGGER.info("  ");
        LOGGER.info(" ================================================================= ");


        return bestStrategy;

    }

    public static class RSI2Strategy {

        /**
         * @param series a time series
         * @return a 2-period RSI strategy
         */
        public static Strategy buildStrategy(TimeSeries series) {
            if (series == null) {
                throw new IllegalArgumentException("Series cannot be null");
            }

            ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
            SMAIndicator shortSma = new SMAIndicator(closePrice, 5);
            SMAIndicator longSma = new SMAIndicator(closePrice, 200);

            // We use a 2-period RSI indicator to identify buying
            // or selling opportunities within the bigger trend.
            RSIIndicator rsi = new RSIIndicator(closePrice, 2);

            // Entry rule
            // The long-term trend is up when a security is above its 200-period SMA.
            Rule entryRule = new OverIndicatorRule(shortSma, longSma) // Trend
                    .and(new CrossedDownIndicatorRule(rsi, Decimal.valueOf(5))) // Signal 1
                    .and(new OverIndicatorRule(shortSma, closePrice)); // Signal 2

            // Exit rule
            // The long-term trend is down when a security is below its 200-period SMA.
            Rule exitRule = new UnderIndicatorRule(shortSma, longSma) // Trend
                    .and(new CrossedUpIndicatorRule(rsi, Decimal.valueOf(95))) // Signal 1
                    .and(new UnderIndicatorRule(shortSma, closePrice)); // Signal 2

            // TODO: Finalize the strategy

            return new BaseStrategy(entryRule, exitRule);
        }


    }

    private List<Bar> createBars(String symbol, Duration duration, int limit) {

        return xtbCandleDao.findAllBySymbolAndDurationAndOpenTimeAfterOrderByOpenTimeAsc(symbol, duration, LocalDateTime.of(LocalDate.ofYearDay(2019, 1), LocalTime.MIDNIGHT).toInstant(ZoneOffset.UTC))
                .map(candle -> new BaseBar(
                        duration,
                        candle.getOpenTime().atZone(ZoneId.systemDefault()),
                        Decimal.valueOf(candle.getOpen()),
                        Decimal.valueOf(candle.getHigh()),
                        Decimal.valueOf(candle.getLow()),
                        Decimal.valueOf(candle.getClose()),
                        Decimal.valueOf(candle.getVolume())))
//                .skip(600)
//                .limit(limit)
                .collect(Collectors.toList());

    }


}
