package com.mfuhrmann.algotrading.strategymanager.model.xtb;

import java.util.Arrays;

public enum InstrumentType {

    INDEX("IND"),
    ETF("ETF"),
    STOCKS("STK"),
    STOCKS_CASH("STC"),
    COMMODITY("CMD"),
    FOREX("FX"),
    CRYPTO("CRT");
    private final String code;

    InstrumentType(String code) {
        this.code = code;
    }



    public static InstrumentType parse(String symbol) {
        return Arrays.stream(InstrumentType.values())
                .filter(instrumentType -> instrumentType.code.equals(symbol)).findAny()
                .orElseThrow(() -> new IllegalArgumentException("No such instrument type " + symbol));
    }
}
