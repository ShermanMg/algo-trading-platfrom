package com.mfuhrmann.algotrading.strategymanager.model.xtb;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class XtbInstrumentService {

    private final XtbInstrumentDao xtbInstrumentDao;
    private final List<XtbInstrument> xtbInstruments;
    private final Map<InstrumentType, List<XtbInstrument>> instrumentsGrouped;
    private final Cache<String, XtbInstrument> cahce;
    private final Set<String> sp500StocksSymbolList;

    private static final List<String> VALID_FX = List.of("EUR", "USD", "GBP", "JPY", "AUD", "NZD", "CHF", "CAD");

    public XtbInstrumentService(XtbInstrumentDao xtbInstrumentDao) {
        this.xtbInstrumentDao = xtbInstrumentDao;

        try {
            sp500StocksSymbolList = Files.lines(Paths.get("data-collector/src/main/resources/constituents_csv.csv"))
                    .skip(1)
                    .map(line -> line.split(",")[0])
                    .filter(s -> !s.trim().isEmpty())
                    .map(s -> s.replace(".", ""))
                    .map(s -> s + ".US_4")
                    .collect(Collectors.toSet());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        this.xtbInstruments = xtbInstrumentDao.findAll().stream()
                .sorted(Comparator.comparingInt(o -> o.getInstrumentType().ordinal()))
                .filter(xtbInstrument -> xtbInstrument.getInstrumentType() != InstrumentType.STOCKS ||
                        (sp500StocksSymbolList.contains(xtbInstrument.getSymbol())))
                .collect(Collectors.toList());
        this.instrumentsGrouped = xtbInstruments.stream()
                .collect(Collectors.groupingBy(XtbInstrument::getInstrumentType));
        cahce = CacheBuilder.newBuilder()
                .maximumSize(2000)
                .expireAfterWrite(1, TimeUnit.DAYS)
                .build(
                        new CacheLoader<>() {
                            public XtbInstrument load(String key) {
                                return xtbInstrumentDao.findBySymbol(key).orElse(null);
                            }
                        });


    }

    public Optional<XtbInstrument> getInstrumentForSymbol(String symbol) {
//            return Optional.ofNullable(cahce.get(symbol, () -> xtbInstrumentDao.findBySymbol(symbol).orElse(null)));
        try {
            return Optional.ofNullable(cahce.get(symbol, () -> xtbInstrumentDao.findBySymbol(symbol).orElse(null)));
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    public List<XtbInstrument> getXtbInstruments() {
        return xtbInstruments;
    }

    public List<XtbInstrument> getSelectedInstruments() {
        return Stream.concat(
                getInstrumentsByType(InstrumentType.FOREX).stream()
                , Stream.concat(getInstrumentsByType(InstrumentType.COMMODITY).stream(),
                        getInstrumentsByType(InstrumentType.INDEX).stream()
//                getFxInstruments().stream().filter(this::getFxPredicate),
//                getInstrumentsByType(InstrumentType.STOCKS).stream()
                )).collect(Collectors.toList());

//        return getXtbInstruments();
    }

    private boolean getFxPredicate(XtbInstrument xtbInstrument) {
        return VALID_FX.contains(xtbInstrument.getSymbolRecordData().getCurrency()) &&
                VALID_FX.contains(xtbInstrument.getSymbolRecordData().getCurrencyProfit());
    }

    public List<XtbInstrument> getFxInstruments() {
        return instrumentsGrouped.get(InstrumentType.FOREX);
    }

    public List<XtbInstrument> getIndexInstruments() {
        return instrumentsGrouped.get(InstrumentType.INDEX);
    }

    public List<XtbInstrument> getInstrumentsByType(InstrumentType type) {
        return instrumentsGrouped.get(type);
    }

}
