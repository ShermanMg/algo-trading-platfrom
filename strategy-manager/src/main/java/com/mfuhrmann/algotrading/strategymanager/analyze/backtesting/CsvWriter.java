package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandle;
import com.mfuhrmann.trading.backtesting.trade.ClosedTrade;
import com.mfuhrmann.trading.backtesting.trade.StrategyResult;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;
import scala.collection.JavaConverters;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class CsvWriter {

    public static final String REPLACEMENt = "{%fx-symbol}";
    public static final String path = "candles/candles-" + REPLACEMENt + ".csv";

    public void writeToCsv(List<XtbCandle> candles, StrategyResult result) {


        Map<String, Map<Instant, List<XtbCandle>>> fxGroupedByDate = candles.stream()
                .collect(Collectors.groupingBy(XtbCandle::getSymbol, Collectors.groupingBy(XtbCandle::getOpenTime)));

        Map<String, List<String>> forexSymbolsToHeader = candles.stream()
                .map(XtbCandle::getSymbol)
                .distinct()
                .collect(Collectors.toMap(Function.identity(), fx -> List.of(fx + "-Date", fx + "-open", fx + "-high", fx + "-low", fx + "-close", "open trade volume", "close trade volume")));


        forexSymbolsToHeader.keySet()
                .forEach(fx -> printRecords(fx, fxGroupedByDate.get(fx), result));


    }


    private void printRecords(String fx, Map<Instant, List<XtbCandle>> fxGroupedByDate, StrategyResult result) {

        Collection<ClosedTrade> closedTrades = JavaConverters.asJavaCollection(result.getTradeBySymbol(fx));

        Map<Instant, BigDecimal> openVolume = closedTrades.stream().
                collect(Collectors.groupingBy(ClosedTrade::openTime, Collectors.mapping(t -> t.positionSize().bigDecimal(), Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));

        Map<Instant, BigDecimal> closeVolume = closedTrades.stream().
                collect(Collectors.groupingBy(ClosedTrade::closeTime, Collectors.mapping(t -> t.positionSize().bigDecimal().negate(), Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));

        CSVPrinter csvPrinter = createCsvPrinter(fx);


        fxGroupedByDate.keySet().stream()
                .sorted()
                .map(instant -> {
                    XtbCandle xtbCandle = fxGroupedByDate.get(instant).get(0);
                    return Arrays.asList(formatTime(instant),
                            xtbCandle.getOpen().toString(),
                            xtbCandle.getHigh().toString(),
                            xtbCandle.getLow().toString(),
                            xtbCandle.getClose().toString(),
                            openVolume.getOrDefault(instant, BigDecimal.ZERO).toString(),
                            closeVolume.getOrDefault(instant, BigDecimal.ZERO).toString());

                }).forEach(values -> printRecord(csvPrinter, values));

        try {
            csvPrinter.close(true);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void printRecord(CSVPrinter csvPrinter, List<String> values) {
        try {
            csvPrinter.printRecord(values.toArray());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    private CSVPrinter createCsvPrinter(String fx) {
        CSVPrinter csvPrinter;
        try {
            csvPrinter = CSVFormat.RFC4180
                    .withHeader(List.of(fx + "-Date", fx + "-open", fx + "-high", fx + "-low", fx + "-close", "open volume", "close volume").toArray(new String[0]))
                    .print(new File(path.replace(REPLACEMENt, fx)), Charset.defaultCharset());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return csvPrinter;
    }

    private static final DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");

    private String formatTime(Instant instant) {

        return df.format(instant.atZone(ZoneId.systemDefault()));

    }
}
