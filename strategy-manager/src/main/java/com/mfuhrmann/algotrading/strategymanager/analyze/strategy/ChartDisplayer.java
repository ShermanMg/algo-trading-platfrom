package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import com.mfuhrmann.algotrading.commons.CurrencyPair;
import com.mfuhrmann.algotrading.strategymanager.model.cme.CmeFixing;
import com.mfuhrmann.algotrading.strategymanager.model.cme.CmeFixingRepository;
import com.mfuhrmann.algotrading.strategymanager.model.deutschebank.Fixing;
import com.mfuhrmann.algotrading.strategymanager.model.deutschebank.FxRateRangeRepository;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.xy.CandlestickRenderer;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.*;
import org.jfree.data.xy.AbstractIntervalXYDataset;
import org.jfree.data.xy.DefaultHighLowDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.OHLCDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.*;
import org.ta4j.core.indicators.RSIIndicator;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;

import java.awt.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;

@Component
public class ChartDisplayer {

    private final FxRateRangeRepository fxRateRangeRepository;
    private final CmeFixingRepository cmeFixingRepository;

    @Autowired
    public ChartDisplayer(FxRateRangeRepository fxRateRangeRepository, CmeFixingRepository cmeFixingRepository) {
        this.fxRateRangeRepository = fxRateRangeRepository;
        this.cmeFixingRepository = cmeFixingRepository;
    }


    public void displayChartWithStrategy(CurrencyPair currencyPair, TimeSeries series, Strategy strategy, Duration duration, int rsiTimeFrame) {

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);
        RSIIndicator rsiIndicator = new RSIIndicator(closePrice, rsiTimeFrame);


        // Bollinger bands
//        BollingerBandsMiddleIndicator middleBBand = new BollingerBandsMiddleIndicator(avg14);
//        BollingerBandsLowerIndicator lowBBand = new BollingerBandsLowerIndicator(middleBBand, sd14);
//        BollingerBandsUpperIndicator upBBand = new BollingerBandsUpperIndicator(middleBBand, sd14);

        /*
          Building chart dataset
//         */
//        TimeSeriesCollection dataset = new TimeSeriesCollection();
////        dataset.addSeries(buildChartTimeSeries(series, closePrice, "Apple Inc. (AAPL) - NASDAQ GS"));
////        dataset.addSeries(buildChartTimeSeries(series, ema10, "Ema 10"));
////        dataset.addSeries(buildChartTimeSeries(series, ema26, "Ema 26"));
////        dataset.addSeries(buildChartTimeSeries(series, ema200, "Emma 200"));
//        dataset.addSeries(buildChartTimeSeries(series, rsiIndicator, "RSI"));

        /**
         * Creating the chart
         */
//        JFreeChart chart = ChartFactory.createCandlestickChart(
//                "Bitbay LSK price",
//                "Time",
//                "PLN",
//                ohlcDataset,
//                true);


        DateAxis domainAxis = new DateAxis("Date");


        //Build Candlestick Chart based on stock price OHLC
        NumberAxis priceAxis = new NumberAxis("Price");
        OHLCDataset priceDataset = createOHLCDataset(series);


        CandlestickRenderer priceRenderer = new CandlestickRenderer();
        XYPlot pricePlot = new XYPlot(priceDataset, domainAxis, priceAxis, priceRenderer);


        priceRenderer.setSeriesPaint(0, Color.BLACK);
        priceRenderer.setDrawVolume(false);
        priceAxis.setAutoRangeIncludesZero(false);

        //Build Bar Chart for volume by wrapping price dataset with an IntervalXYDataset
        IntervalXYDataset volumeDataset = getVolumeDataset(priceDataset, duration.toMillis()); // Each bar is 24 hours wide.
        NumberAxis volumeAxis = new NumberAxis("Volume");
        XYBarRenderer volumeRenderer = new XYBarRenderer();

        XYPlot volumePlot = new XYPlot(volumeDataset, domainAxis, volumeAxis, volumeRenderer);
        volumeRenderer.setSeriesPaint(0, Color.BLUE);

        NumberAxis rsiAxis = new NumberAxis("RSI");
        TickUnits units = new TickUnits();
        units.add(new NumberTickUnit(10));
        rsiAxis.setStandardTickUnits(units);


        XYPlot indicatorPlot = new XYPlot(new TimeSeriesCollection(buildChartTimeSeries(series, rsiIndicator, "RSI")), domainAxis, rsiAxis, new XYLineAndShapeRenderer(true, false));

        //Build Combined Plot
        CombinedDomainXYPlot mainPlot = new CombinedDomainXYPlot(domainAxis);
        mainPlot.add(pricePlot, 4);
        mainPlot.add(volumePlot, 1);//4) how do i add additional technical charts like MACD, STO, etc etc below the original candlestick chart?
        mainPlot.add(indicatorPlot, 1);

        JFreeChart chart = new JFreeChart("Bitbay  " + currencyPair.getCurrency1() + currencyPair.getCurrency2() + duration, null, mainPlot, false);


        ((DateAxis) chart.getXYPlot().getDomainAxis()).setTimeline(SegmentedTimeline.newMondayThroughFridayTimeline());


        // Candlestick rendering
//        CandlestickRenderer renderer = new CandlestickRenderer();
//        renderer.setAutoWidthMethod(CandlestickRenderer.WIDTHMETHOD_SMALLEST);
//        XYPlot plot = chart.getXYPlot();
//        plot.setRenderer(renderer);
//        // Additional dataset
//        int index = 1;
//        plot.setDataset(index, dataset);
//        plot.mapDatasetToRangeAxis(index, 0);
//        XYLineAndShapeRenderer renderer2 = new XYLineAndShapeRenderer(true, false);
//        renderer2.setSeriesPaint(index, Color.blue);
//        plot.setRenderer(index, renderer2);
//        // Misc
//        plot.setRangeGridlinePaint(Color.lightGray);
//        plot.setBackgroundPaint(Color.white);
//        NumberAxis numberAxis = (NumberAxis) plot.getRangeAxis();
//        numberAxis.setAutoRangeIncludesZero(false);
//        plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);


        addBuySellSignals(series, strategy, pricePlot);
        /**
         * Displaying the chart
         */
        displayChartWithStrategy(chart);

    }

    public void displayChart(CurrencyPair currencyPair, TimeSeries series, Duration duration) {

        ClosePriceIndicator closePrice = new ClosePriceIndicator(series);


        DateAxis domainAxis = new DateAxis("Date");


        //Build Candlestick Chart based on stock price OHLC
        NumberAxis priceAxis = new NumberAxis("Price");
        OHLCDataset priceDataset = createOHLCDataset(series);


        CandlestickRenderer priceRenderer = new CandlestickRenderer();
        XYPlot pricePlot = new XYPlot(priceDataset, domainAxis, priceAxis, priceRenderer);
        priceRenderer.setSeriesPaint(0, Color.BLACK);
        priceRenderer.setDrawVolume(false);
        priceAxis.setAutoRangeIncludesZero(false);


        createFxRangePlot(currencyPair, pricePlot);


//        XYPlot fxRangePlot = getFxRateRenderer(domainAxis, priceAxis);


        //Build Bar Chart for volume by wrapping price dataset with an IntervalXYDataset
        IntervalXYDataset volumeDataset = getVolumeDataset(priceDataset, duration.toMillis()); // Each bar is 24 hours wide.
        NumberAxis volumeAxis = new NumberAxis("Volume");
        XYBarRenderer volumeRenderer = new XYBarRenderer();

        XYPlot volumePlot = new XYPlot(volumeDataset, domainAxis, volumeAxis, volumeRenderer);
        volumeRenderer.setSeriesPaint(0, Color.BLUE);

        NumberAxis rsiAxis = new NumberAxis("RSI");
        TickUnits units = new TickUnits();
        units.add(new NumberTickUnit(10));
        rsiAxis.setStandardTickUnits(units);


//        XYPlot indicatorPlot = new XYPlot(new TimeSeriesCollection(buildChartTimeSeries(series, rsiIndicator, "RSI")), domainAxis, rsiAxis, new XYLineAndShapeRenderer(true, false));

        //Build Combined Plot
        CombinedDomainXYPlot mainPlot = new CombinedDomainXYPlot(domainAxis);
        mainPlot.add(pricePlot, 4);
        mainPlot.add(volumePlot, 1);//4) how do i add additional technical charts like MACD, STO, etc etc below the original candlestick chart?
//        mainPlot.add(fxRangePlot);

//        mainPlot.add(indicatorPlot, 1);

        JFreeChart chart = new JFreeChart("  " + currencyPair.getCurrency1() + currencyPair.getCurrency2() + duration, null, mainPlot, false);


        ((DateAxis) chart.getXYPlot().getDomainAxis()).setTimeline(SegmentedTimeline.newMondayThroughFridayTimeline());


        XYLineAndShapeRenderer lineRenderer = new XYLineAndShapeRenderer(true, false);
        pricePlot.setRenderer(10, lineRenderer);


        /**
         * Displaying the chart
         */
        displayChartWithStrategy(chart);

    }

    private void createFxRangePlot(CurrencyPair currencyPair, XYPlot pricePlot) {
        org.jfree.data.time.TimeSeries lowSeries = new org.jfree.data.time.TimeSeries("Low");
        org.jfree.data.time.TimeSeries highSeries = new org.jfree.data.time.TimeSeries("High");
        org.jfree.data.time.TimeSeries cmeSeries = new org.jfree.data.time.TimeSeries("High");


        addLowHighBoundariesData(currencyPair, lowSeries, highSeries);
        addCmeSeriesData(currencyPair, cmeSeries);


        TimeSeriesCollection fxDataSet = new TimeSeriesCollection();
        fxDataSet.addSeries(lowSeries);
        fxDataSet.addSeries(highSeries);
        fxDataSet.addSeries(cmeSeries);

        XYLineAndShapeRenderer fxDataRenderer = new XYLineAndShapeRenderer(true, false);
        fxDataRenderer.setSeriesPaint(0, Color.RED);
        fxDataRenderer.setSeriesPaint(1, Color.BLUE);
        fxDataRenderer.setSeriesPaint(2, Color.ORANGE);
        pricePlot.setDataset(1, fxDataSet);
        pricePlot.setRenderer(1, fxDataRenderer);
    }

    private void addCmeSeriesData(CurrencyPair currencyPair, org.jfree.data.time.TimeSeries series) {

        cmeFixingRepository.findAllByCurrency(currencyPair.getCurrency1() + currencyPair.getCurrency2())
                .forEach(cmeFixing -> {

                    LocalDateTime localDate = cmeFixing.getLocalDate().atTime(LocalTime.of(17, 30)).plusDays(1);


                    Double aDouble = cmeFixing.getFixingRows().stream()
                            .filter(fixingRow -> fixingRow.getTenor() == CmeFixing.TENOR.T_ON)
                            .map(CmeFixing.FixingRow::getRate)
                            .findFirst().orElse(0.0);

                    series.addOrUpdate(new Hour(localDate.getHour(), new Day(localDate.getDayOfMonth(), localDate.getMonthValue(), localDate.getYear())), aDouble);

                });

    }

    private void addLowHighBoundariesData(CurrencyPair currencyPair, org.jfree.data.time.TimeSeries lowSeries, org.jfree.data.time.TimeSeries highSeries) {
        fxRateRangeRepository.findAll().stream()
                .filter(fxRateRange -> fxRateRange.getCurrency().equals(currencyPair.getCurrency1() + currencyPair.getCurrency2()))
                .filter(fxRateRange -> fxRateRange.getLocalDate().isAfter(LocalDate.of(2019, 1, 1)))
                .forEach(fxRateRange -> {

                    LocalTime localTime;
                    if (fxRateRange.getFixing() == Fixing.MORNING) {
                        localTime = LocalTime.of(9, 0);
                    } else {
                        localTime = LocalTime.of(13, 0);
                    }
                    LocalDate localDate = fxRateRange.getLocalDate().plusDays(5);


                    lowSeries.addOrUpdate(new Hour(localTime.getHour(), new Day(localDate.getDayOfMonth(), localDate.getMonthValue(), localDate.getYear())), fxRateRange.getLow());
                    highSeries.addOrUpdate((new Hour(localTime.getHour(), new Day(localDate.getDayOfMonth(), localDate.getMonthValue(), localDate.getYear()))), fxRateRange.getHigh());
                });
    }

//    @NotNull
//    private XYPlot getFxRateRenderer(DateAxis domainAxis, NumberAxis priceAxis) {
//        XYLineAndShapeRenderer fxRateRenderer = new XYLineAndShapeRenderer(true, false);
//
//
//
//        XYPlot fxRangePlot = new XYPlot(dataset, domainAxis, priceAxis, fxRateRenderer);
//
//
//        fxRateRenderer.setSeriesPaint(0, Color.BLACK);
//        return fxRangePlot;
//    }

    @SuppressWarnings("serial")
    protected static IntervalXYDataset getVolumeDataset(final OHLCDataset priceDataset, final long barWidthInMilliseconds) {
        return new AbstractIntervalXYDataset() {
            public int getSeriesCount() {
                return priceDataset.getSeriesCount();
            }

            @SuppressWarnings("unchecked")
            public Comparable getSeriesKey(int series) {
                return priceDataset.getSeriesKey(series) + "-Volume";
            }

            public int getItemCount(int series) {
                return priceDataset.getItemCount(series);
            }

            public Number getX(int series, int item) {
                return priceDataset.getX(series, item);
            }

            public Number getY(int series, int item) {
                return priceDataset.getVolume(series, item);
            }

            public Number getStartX(int series, int item) {
                return priceDataset.getX(series, item).doubleValue() - barWidthInMilliseconds / 2;
            }

            public Number getEndX(int series, int item) {
                return priceDataset.getX(series, item).doubleValue() + barWidthInMilliseconds / 2;
            }

            public Number getStartY(int series, int item) {
                return new Double(0.0);
            }

            public Number getEndY(int series, int item) {
                return priceDataset.getVolume(series, item);
            }
        };
    }

    private void addBuySellSignals(TimeSeries series, Strategy strategy, XYPlot plot) {
        // Running the strategy
        TimeSeriesManager seriesManager = new TimeSeriesManager(series);
        List<Trade> trades = seriesManager.run(strategy).getTrades();
        // Adding markers to plot
        for (Trade trade : trades) {
            // Buy signal
            double buySignalBarTime = new Minute(Date.from(series.getBar(trade.getEntry().getIndex()).getEndTime().toInstant())).getFirstMillisecond();
            Marker buyMarker = new ValueMarker(buySignalBarTime);
            buyMarker.setPaint(Color.GREEN);
            buyMarker.setLabel("BUY");
            plot.addDomainMarker(buyMarker);
            // Sell signal
            double sellSignalBarTime = new Minute(Date.from(series.getBar(trade.getExit().getIndex()).getEndTime().toInstant())).getFirstMillisecond();
            Marker sellMarker = new ValueMarker(sellSignalBarTime);
            sellMarker.setPaint(Color.RED);
            sellMarker.setLabel("SELL");
            plot.addDomainMarker(sellMarker);

        }

//        IntervalMarker marker = new IntervalMarker(1.15, 1.24);
//        marker.setPaint(Color.YELLOW);
//        plot.addDomainMarker(marker);
    }

    /**
     * Builds a JFreeChart OHLC dataset from a ta4j time series.
     *
     * @param series a time series
     * @return an Open-High-Low-Close dataset
     */
    public OHLCDataset createOHLCDataset(TimeSeries series) {
        final int nbTicks = series.getBarCount();

        Date[] dates = new Date[nbTicks];
        double[] opens = new double[nbTicks];
        double[] highs = new double[nbTicks];
        double[] lows = new double[nbTicks];
        double[] closes = new double[nbTicks];
        double[] volumes = new double[nbTicks];

        for (int i = 0; i < nbTicks; i++) {
            Bar tick = series.getBar(i);
            dates[i] = new Date(tick.getEndTime().toEpochSecond() * 1000);
            opens[i] = tick.getOpenPrice().toDouble();
            highs[i] = tick.getMaxPrice().toDouble();
            lows[i] = tick.getMinPrice().toDouble();
            closes[i] = tick.getClosePrice().toDouble();
            volumes[i] = tick.getVolume().toDouble();
        }

        OHLCDataset dataset = new DefaultHighLowDataset("btc", dates, highs, lows, opens, closes, volumes);

        return dataset;
    }

    /**
     * Builds an additional JFreeChart dataset from a ta4j time series.
     *
     * @param series a time series
     * @return an additional dataset
     */
    private TimeSeriesCollection createAdditionalDataset(TimeSeries series) {
        ClosePriceIndicator indicator = new ClosePriceIndicator(series);
        TimeSeriesCollection dataset = new TimeSeriesCollection();
        org.jfree.data.time.TimeSeries chartTimeSeries = new org.jfree.data.time.TimeSeries("Btc price");
        for (int i = 0; i < series.getBarCount(); i++) {
            Bar tick = series.getBar(i);
            chartTimeSeries.add(new Second(new Date(tick.getEndTime().toEpochSecond() * 1000)), indicator.getValue(i).toDouble());
        }
        dataset.addSeries(chartTimeSeries);
        return dataset;
    }


    /**
     * Displays a chart in a frame.
     *
     * @param chart the chart to be displayed
     */
    private void displayChartWithStrategy(JFreeChart chart) {
        // Chart panel
        ChartPanel panel = new ChartPanel(chart);
        panel.setFillZoomRectangle(true);
        panel.setMouseWheelEnabled(true);
        panel.setPreferredSize(new Dimension(2560, 960));
        // Application frame
        ApplicationFrame frame = new ApplicationFrame("Ta4j example - Cash flow to chart");
        frame.setContentPane(panel);
        frame.pack();
        RefineryUtilities.centerFrameOnScreen(frame);
        frame.setVisible(true);
    }

    private static org.jfree.data.time.TimeSeries buildChartTimeSeries(TimeSeries barseries, Indicator<Decimal> indicator, String name) {
        org.jfree.data.time.TimeSeries chartTimeSeries = new org.jfree.data.time.TimeSeries(name);
        for (int i = 0; i < barseries.getBarCount(); i++) {
            Bar bar = barseries.getBar(i);
            chartTimeSeries.addOrUpdate(new Second(Date.from(bar.getEndTime().toInstant())), indicator.getValue(i).doubleValue());
        }
        return chartTimeSeries;
    }

}
