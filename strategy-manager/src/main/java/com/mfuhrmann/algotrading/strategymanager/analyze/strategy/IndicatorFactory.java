package com.mfuhrmann.algotrading.strategymanager.analyze.strategy;

import org.springframework.stereotype.Component;
import org.ta4j.core.Decimal;
import org.ta4j.core.Indicator;
import org.ta4j.core.TimeSeries;
import org.ta4j.core.indicators.*;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class IndicatorFactory {

    private final List<Integer> FIBONACCI_NUMBERS = IntStream.range(0, 201)
            .filter(i -> isFibonacci(i) || i == 200)
            .boxed().collect(Collectors.toList());

    private static final List<Integer> RSI_VALUES = IntStream.of(10, 14, 21, 28)
            .boxed()
            .collect(Collectors.toList());


    List<RSIIndicator> createRsiIndicators(TimeSeries series) {

        List<Indicator<Decimal>> priceIndicators = getPriceIndicators(series);
        return priceIndicators.stream().flatMap(priceIndicator ->
                RSI_VALUES.stream().map(v -> new RSIIndicator(priceIndicator, v)))
                .collect(Collectors.toList());
    }

    public List<EMAIndicator> createEmaIndicators(TimeSeries series) {

        List<Indicator<Decimal>> priceIndicators = getPriceIndicators(series);


        return FIBONACCI_NUMBERS.stream()
                .filter(i -> i > 20)
                .flatMap(integer -> priceIndicators.stream().map(price -> new EMAIndicator(price, integer)))
                .collect(Collectors.toList());
    }

    public List<SMAIndicator> createSmaIndicators(TimeSeries series) {
        List<Indicator<Decimal>> priceIndicators = getPriceIndicators(series);

        return FIBONACCI_NUMBERS.stream()
                .filter(i -> i > 20)
                .flatMap(integer -> priceIndicators.stream()
                        .map(price -> new SMAIndicator(price, integer)))
                .collect(Collectors.toList());
    }

    public List<TripleEMAIndicator> createTripleEmaIndicators(TimeSeries series) {
        List<Indicator<Decimal>> priceIndicators = getPriceIndicators(series);

        return FIBONACCI_NUMBERS.stream()
                .filter(i -> i > 20)
                .flatMap(integer -> priceIndicators.stream()
                        .map(price -> new TripleEMAIndicator(price, integer)))
                .collect(Collectors.toList());
    }


    private List<Indicator<Decimal>> getPriceIndicators(TimeSeries series) {
        ClosePriceIndicator closePriceIndicator = new ClosePriceIndicator(series);
//        OpenPriceIndicator openpriceIndicator = new OpenPriceIndicator(series);
//        MaxPriceIndicator maxPriceIndicator = new MaxPriceIndicator(series);
//        MinPriceIndicator minPriceIndicator = new MinPriceIndicator(series);

//        return Arrays.asList(closePriceIndicator, openpriceIndicator, maxPriceIndicator, minPriceIndicator);

        return Arrays.asList(closePriceIndicator);
    }

    private static boolean isPerfectSquare(int x) {
        int s = (int) Math.sqrt(x);
        return (s * s == x);
    }

    // Returns true if n is a Fibonacci Number, else false
    private static boolean isFibonacci(int n) {
        // n is Fibonacci if one of 5*n*n + 4 or 5*n*n - 4 or both
        // is a perfect square
        return isPerfectSquare(5 * n * n + 4) ||
                isPerfectSquare(5 * n * n - 4);
    }

}
