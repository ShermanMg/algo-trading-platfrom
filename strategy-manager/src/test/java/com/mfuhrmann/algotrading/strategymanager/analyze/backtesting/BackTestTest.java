package com.mfuhrmann.algotrading.strategymanager.analyze.backtesting;

import com.mfuhrmann.algotrading.strategymanager.model.xtb.InstrumentType;
import com.mfuhrmann.algotrading.strategymanager.model.xtb.XtbCandle;
import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.*;
import java.util.Map;

public class BackTestTest {

    private BackTest backTest;

    @Test
    public void test1() {

        //Given
        Duration duration = Duration.ofHours(1);

        LocalDateTime candle1Time = LocalDateTime.of(LocalDate.now(), LocalTime.of(3, 0));
        LocalDateTime candle2Time = LocalDateTime.of(LocalDate.now(), LocalTime.of(4, 0));
        LocalDateTime candle3Time = LocalDateTime.of(LocalDate.now(), LocalTime.of(5, 0));

        XtbCandle candle1 = new XtbCandle("EURUSD",
                InstrumentType.FOREX,
                duration,
                candle1Time.atZone(ZoneId.systemDefault()).toInstant(),
                BigDecimal.valueOf(1.1244),
                BigDecimal.valueOf(1.1246),
                BigDecimal.valueOf(1.1240),
                BigDecimal.valueOf(1.1241),
                BigDecimal.valueOf(1));

        XtbCandle candle2 = new XtbCandle("EURUSD",
                InstrumentType.FOREX,
                duration,
                candle2Time.atZone(ZoneId.systemDefault()).toInstant(),
                BigDecimal.valueOf(1.1244),
                BigDecimal.valueOf(1.1248),
                BigDecimal.valueOf(1.1244),
                BigDecimal.valueOf(1.1248),
                BigDecimal.valueOf(1));

        XtbCandle candle3 = new XtbCandle("EURUSD",
                InstrumentType.FOREX,
                duration,
                candle3Time.atZone(ZoneId.systemDefault()).toInstant(),
                BigDecimal.valueOf(1.1247),
                BigDecimal.valueOf(1.1253),
                BigDecimal.valueOf(1.1248),
                BigDecimal.valueOf(1.1252),
                BigDecimal.valueOf(1));

        CandleDataSource candleDataSource = new CandleDataSource(duration, Map.of(candle1Time, candle1, candle2Time, candle2, candle3Time, candle3));


        backTest = new BackTest(
                new Strategy(
                        Map.of(duration, candleDataSource)));


        //When
        int pipsProfit = backTest.test(candle1Time, candle3Time);


        //Then

        Assertions.assertThat(pipsProfit).isEqualTo(3);

    }
}