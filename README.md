This is a algo trading application platform that's supposed to collect the data from different exchanges, analyze them and create a profitable forex strategy.
This is a very much still in progress project.

Right now main focus is on developing FX strategy.

Supported connectors:
- XTB - data collection + trade execution ( not finished 100% )
- Dukascopy -
- CME (5pm London fixing for the currencies)
- Deutsche Bank ( 11 and 2 pm fixings)
- IC Markets ( in progress (fix protocol to be implemented))

#Technologies
Most of the project is written in java with latest version of the sprinb-boot.

Backtesting module is written in Scala.
