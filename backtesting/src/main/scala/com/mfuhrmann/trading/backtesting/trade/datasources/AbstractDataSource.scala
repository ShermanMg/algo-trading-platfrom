package com.mfuhrmann.trading.backtesting.trade.datasources

import com.mfuhrmann.trading.backtesting.trade.{DataSourceCandle, DataSourceTick}

abstract class AbstractDataSource(val candles: List[_ <: DataSourceCandle], val ticks: List[_ <: DataSourceTick]) extends DataSource {

  private val candlesGrouped = candles.groupBy(c => c.getOpenTime.plus(c.getDuration))
  private val ticksGrouped = ticks.groupBy(c => c.getTime)
  private val timeKeys = candlesGrouped.keySet.union(ticksGrouped.keySet)
    .toList
    .sorted


  final override def iterateOverDataSet(dataSourceWalker: DataSourceWalker): Unit = {

    timeKeys.foreach(key => {
      ticksGrouped.getOrElse(key, List.empty).foreach(tick => dataSourceWalker.onNextTick(tick))
      candlesGrouped.getOrElse(key, List.empty).foreach(candle => dataSourceWalker.onNextCandle(candle))
    })


  }


}
