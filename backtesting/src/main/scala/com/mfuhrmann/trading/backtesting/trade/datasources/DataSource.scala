package com.mfuhrmann.trading.backtesting.trade.datasources

import com.mfuhrmann.trading.backtesting.trade.strategy.StrategyParams
import com.mfuhrmann.trading.backtesting.trade.{DataSourceCandle, DataSourceTick}

trait DataSource {
  def getParams: StrategyParams

  def getCandles: List[_ <: DataSourceCandle]

  def getTicks: List[_ <: DataSourceTick]

  def iterateOverDataSet(dataSourceWalker: DataSourceWalker)

}

trait DataSourceWalker {

  def onNextCandle(dataSourceCandle: DataSourceCandle)

  def onNextTick(dataSourceTick: DataSourceTick)

}
