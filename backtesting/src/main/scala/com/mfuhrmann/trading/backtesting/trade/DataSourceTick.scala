package com.mfuhrmann.trading.backtesting.trade

import java.math.BigDecimal
import java.time.Instant


trait DataSourceTick {
  def getMarketSymbol: MarketSymbol

  def getBid: BigDecimal

  def getAsk: BigDecimal

  def getTime: Instant

  def getAskVolume: Long

  def getBidVolume: Long
}
