package com.mfuhrmann.trading.backtesting.trade.strategy.scalping

import com.mfuhrmann.trading.backtesting.trade.strategy.StrategyParams

class ScalpingStrategyParams(riskThreshold: Double, val ticksCount: Int, val fasterEmaValue: Int, val sloweEmaValue: Int, stopLoss: Int, tp: Int, val stochasticOscillatorValue: Int, val maxSpread: BigDecimal) extends StrategyParams(riskThreshold, stopLoss, tp, ticksCount) {

}
