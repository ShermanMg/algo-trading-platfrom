package com.mfuhrmann.trading.backtesting.trade.portfolio

import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.strategy.BidAskPrice
import com.mfuhrmann.trading.backtesting.trade.{ClosedTrade, ForexMarketSymbol, MarketSymbol, Trade}
import com.mfuhrmann.trading.backtesting.trade.utils.Money

import scala.collection.mutable
import scala.util.Try

class MoneyManager(val initialMoney: Money, leverage: Int) {

  final val LOT_DEFAULT_CONTRACT_SIZE = 100000

  var currentMoney: Money = initialMoney

  val lastPrices: mutable.Map[MarketSymbol, BigDecimal] = new mutable.HashMap[MarketSymbol, BigDecimal]()

  var currentTrades = 0

  var maxTrades = 0

  def canTrade(symbol: MarketSymbol, value: BigDecimal, price: BidAskPrice, tradeType: TradeType): Boolean = {

    val moneyRequiredForTrade: Money = calculateMarginForTrade(symbol, value, price.getPriceForOpeningTrade(tradeType))
    return currentTrades < 20

    if (moneyRequiredForTrade.money < 1.0) {
      return false
    }

    currentMoney > moneyRequiredForTrade
  }


  def onNextTick(symbol: MarketSymbol, bidAskPriceCandlePrice: BidAskPrice, tickTime: Instant): Unit = {
    lastPrices.update(symbol, bidAskPriceCandlePrice.getAveraged)
  }

  def tradeOpened(trade: Trade): Unit = {
    val moneyRequiredForTrade: Money = calculateMarginForTrade(trade.symbol, trade.positionSize, trade.openPrice)

    val newMoney: Try[Money] = currentMoney - moneyRequiredForTrade

    if (newMoney.isSuccess) {
      currentMoney = newMoney.get
    } else {
      print("Could not properly subtract moneys")
    }
    currentTrades += 1
    maxTrades = Math.max(currentTrades, maxTrades)
  }

  //TODO add eventual profits
  def tradeClosed(trade: ClosedTrade): Unit = {
    val moneyRequiredForTrade: Money = calculateMarginForTrade(trade.symbol, trade.positionSize, trade.openPrice)

    val newMoney: Try[Money] = currentMoney + moneyRequiredForTrade

    if (newMoney.isSuccess) {
      currentMoney = newMoney.get
    } else {
      print("Could not properly subtract moneys")
    }
    currentTrades -= 1
  }

  private def calculateMarginForTrade(symbol: MarketSymbol, value: BigDecimal, price: BigDecimal): Money = {
    val latestPrice: BigDecimal = lastPrices.getOrElse(symbol, 0.0)

    val marginInBaseCurrency: BigDecimal = ((value * LOT_DEFAULT_CONTRACT_SIZE) / leverage) * latestPrice

    val moneyCurrencyToBaseCurrencyPrice: BigDecimal = lastPrices.getOrElse(new ForexMarketSymbol(symbol.getSellingAsset, initialMoney.currency.getCurrencyCode), 0.0)

    new Money(marginInBaseCurrency * moneyCurrencyToBaseCurrencyPrice, initialMoney.currency)

  }
}
