package com.mfuhrmann.trading.backtesting.trade

trait MarketSymbol {

  def getBuyingAsset: String

  def getSellingAsset: String

  def getName: String

}

class ForexMarketSymbol(val buyingAsset: String, val sellingAsset: String) extends MarketSymbol {

  def this(forexName: String) {
    this(forexName.splitAt(3)._1, forexName.splitAt(3)._2)
  }

  val name: String = buyingAsset + sellingAsset

  override def getBuyingAsset: String = buyingAsset

  override def getSellingAsset: String = sellingAsset

  override def getName: String = name

  override def toString: String = getName

  def canEqual(other: Any): Boolean = other.isInstanceOf[ForexMarketSymbol]


  override def equals(other: Any): Boolean = other match {
    case that: ForexMarketSymbol =>
      (that canEqual this) &&
        name == that.name
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(name)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}

class CommodityMarketSymbol(val buyingAsset: String, val sellingAsset: String, val name: String) extends MarketSymbol {
  override def getBuyingAsset: String = buyingAsset

  override def getSellingAsset: String = sellingAsset

  override def getName: String = name
}