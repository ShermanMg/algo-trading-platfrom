package com.mfuhrmann.trading.backtesting.trade.indicator

class EmaIndicator(val range: Int) extends Indicator {

  val weightedMultiplier: Double = 2 / (range + 1).doubleValue()
  var previousEma: BigDecimal = _

  override def calculate(values: Seq[BigDecimal]): BigDecimal = {

    val closingPrice = values.last
    val previousEma = getPreviousEma(values)

    val currentEma = (closingPrice - previousEma) * weightedMultiplier + previousEma

    this.previousEma = currentEma
    currentEma
  }


  def getPreviousEma(values: Seq[BigDecimal]): BigDecimal = {
    if (previousEma == null) {
      return values.last
    }

    previousEma


  }
}
