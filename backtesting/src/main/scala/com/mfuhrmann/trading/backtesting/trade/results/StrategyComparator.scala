package com.mfuhrmann.trading.backtesting.trade.results

import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicInteger

import com.mfuhrmann.trading.backtesting.trade.datasources.DataSource
import com.mfuhrmann.trading.backtesting.trade.strategy.Strategy
import com.mfuhrmann.trading.backtesting.trade.{StrategyBackTester, StrategyResult}

class StrategyComparator(strategies: List[Strategy], dataSource: DataSource) {


  def run = {

    val counter: AtomicInteger = new AtomicInteger()

    val start = System.nanoTime()

    strategies.grouped(24)
      .toList
//      .par
      .flatMap(stratsGrouped => findBestStratInGroup(stratsGrouped, counter))
      .sortBy(s => s._2.getProfitIncludingTradeCost)(Ordering[Long].reverse)
      .take(5)

    val secondsElapsed = TimeUnit.NANOSECONDS.toSeconds(System.nanoTime() - start)

    println("It took: " + secondsElapsed + " seconds to backtest " + strategies.size + " Average  is: " + (secondsElapsed / strategies.size.doubleValue() + " seconds/per strategy"))

    //    val run: StrategyResult = strategyBackTester.run(xtbDataSource.getTicks)
    //
    //    val strategyStatsCollector: StrategyStatsCollector = strategy.statsCollector
    //
    //    LOGGER.info(strategyStatsCollector.toString)
  }

  private def findBestStratInGroup(strategiesGrouped: List[Strategy], counter: AtomicInteger): List[(Strategy, StrategyResult)] = {
    strategiesGrouped
      .par
      .map(s => getStrategyWithResult(s, counter))
      .toList
      .sortBy(s => s._2.getProfitIncludingTradeCost)(Ordering[Long].reverse)
      .take(5)
      .map(set => {
        println
        println(set._2.toString)
        println(set._1.getStrategyParams)
        set
      })
  }

  private def getStrategyWithResult(strategy: Strategy, counter: AtomicInteger): (Strategy, StrategyResult) = {

    val strategyBackTester = new StrategyBackTester(strategy)

//    println(Thread.currentThread().getName)

    val strategyResult: StrategyResult = strategyBackTester.run(dataSource)

    println()
    println("Strategy " + counter.incrementAndGet() + " out of  " + strategies.size)

    (strategy, strategyResult)
  }
}
