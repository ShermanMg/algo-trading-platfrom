package com.mfuhrmann.trading.backtesting.trade.strategy.scalping

import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.indicator.{EmaIndicator, StochasticOscillator}
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio
import com.mfuhrmann.trading.backtesting.trade.risk.RiskCalculator
import com.mfuhrmann.trading.backtesting.trade.strategy.{AbstractStrategy, BidAskPrice}
import com.mfuhrmann.trading.backtesting.trade.{MarketSymbol, Trade}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer


class  ScalpingForexStrategy(portfolio: Portfolio, riskCalculator: RiskCalculator, params: ScalpingStrategyParams) extends AbstractStrategy(portfolio, riskCalculator, params) {

  val fastEmaIndicator: EmaIndicator = new EmaIndicator(params.fasterEmaValue)
  val slowEmaIndicator: EmaIndicator = new EmaIndicator(params.sloweEmaValue)
  val stochasticOscillator: StochasticOscillator = new StochasticOscillator(params.stochasticOscillatorValue)

  val fastIndicatorsMap = new mutable.HashMap[trade.MarketSymbol, EmaIndicator]()
  val slowIndicatorsMap = new mutable.HashMap[trade.MarketSymbol, EmaIndicator]()
  val prevStochOscilators = new mutable.HashMap[trade.MarketSymbol, BigDecimal]()


  val BUY_STOCH = 10
  val SELL_STOCH = 90


  override def shouldBuy(symbol: MarketSymbol, candlePrice: BidAskPrice): Boolean = {


    if (candlePrice.getSpreadInPips() > params.maxSpread) {
      return false
    }

    val decimals: ListBuffer[BigDecimal] = pastCandles.getOrElse(symbol, ListBuffer())
      .map(candle => candle.getClose)


    decimals.append(pastTicks.getOrElse(symbol, ListBuffer()).last.bidPrice)

    if (decimals.size < slowEmaIndicator.range) {
      return false
    }

    val fastEma = getFastEmaIndicator(symbol).calculate(decimals)
    val slowEma = getSlowEmaIndicator(symbol).calculate(decimals)
    val stoch = stochasticOscillator.calculate(decimals)


    if (fastEma > slowEma) {

      //      val isCrossingFromBelow = prevStochOscilators.getOrElse(symbol, BigDecimal(50)) < BigDecimal.apply(BUY_STOCH) && stoch >= BUY_STOCH
      val isCrossingFromBelow = stoch <= BUY_STOCH
      prevStochOscilators.update(symbol, stoch)

      return isCrossingFromBelow

    }
    prevStochOscilators.update(symbol, stoch)

    false

  }

  override def shouldSell(symbol: MarketSymbol, candlePrice: BidAskPrice): Boolean = {

    if (candlePrice.getSpreadInPips() > params.maxSpread) {
      return false
    }
    val decimals: ListBuffer[BigDecimal] = pastCandles.getOrElse(symbol, ListBuffer())
      .map(candle => candle.getClose)
    decimals.append(pastTicks.getOrElse(symbol, ListBuffer()).last.askPrice)


    if (decimals.size < slowEmaIndicator.range) {
      return false
    }
    val fastEma = getFastEmaIndicator(symbol).calculate(decimals)
    val slowEma = getSlowEmaIndicator(symbol).calculate(decimals)
    val stoch = stochasticOscillator.calculate(decimals)

    if (fastEma < slowEma) {

      val isCrossingFromBelow = stoch >= BUY_STOCH
      //      val isCrossingFromBelow = prevStochOscilators.getOrElse(symbol, BigDecimal(50)) > BigDecimal.apply(SELL_STOCH) && stoch <= SELL_STOCH
      prevStochOscilators.update(symbol, stoch)

      return isCrossingFromBelow
    }
    prevStochOscilators.update(symbol, stoch)

    false

  }

  override def shouldTakeProfit(trade: Trade): Boolean = {
    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    val pipsDiff = trade.calculatePips(lastPrice.getPriceForClosingTrade(trade))
    pipsDiff >= params.tp
  }

  override def shouldStopLoss(trade: Trade, tickTime: Instant): Boolean = {
    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    val pipsDiff = trade.calculatePips(lastPrice.getPriceForClosingTrade(trade))
    pipsDiff <= -params.stopLoss
  }

  private def getFastEmaIndicator(marketSymbol: MarketSymbol): EmaIndicator = {
    fastIndicatorsMap.getOrElseUpdate(marketSymbol, new EmaIndicator(params.fasterEmaValue))
  }

  private def getSlowEmaIndicator(marketSymbol: MarketSymbol): EmaIndicator = {
    slowIndicatorsMap.getOrElseUpdate(marketSymbol, new EmaIndicator(params.sloweEmaValue))
  }

}
