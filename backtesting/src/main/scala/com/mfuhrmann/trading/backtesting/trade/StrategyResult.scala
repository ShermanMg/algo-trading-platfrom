package com.mfuhrmann.trading.backtesting.trade

class StrategyResult(val profitInPips: Long, val tradesPerformed: List[ClosedTrade], val maxTrades: Long) {


  val numberOfTrades: Long = tradesPerformed.length
  val tradesBySymbol = tradesPerformed.groupBy(t => t.symbol)


  def getProfitInPipsPerTrade: Double = {
    if (tradesPerformed.nonEmpty) {
      return profitInPips / tradesPerformed.length
    }
    0.0
  }

  def getProfitIncludingTradeCost: Long = {
    profitInPips - (numberOfTrades * 2)

  }

  def getTradeBySymbol(fxSymbol: String): List[ClosedTrade] = {

    tradesBySymbol.getOrElse(new ForexMarketSymbol(fxSymbol), List())
  }

  override def toString: String = {


    "Strategy:  pips profit: " + profitInPips + System.lineSeparator() +
      " Number of Trades:" + numberOfTrades + System.lineSeparator() +
      " Avg pips per trade: " + getProfitInPipsPerTrade + System.lineSeparator() +
      " Possible profit including trade cost " + getProfitIncludingTradeCost + System.lineSeparator() +
      " Max trades " + maxTrades + System.lineSeparator() +
      tradesPerformed
        .groupBy(t => t.symbol)
        .map({ case (symbol: MarketSymbol, list: List[ClosedTrade]) => symbol + " " + list.length + " " + list.map(t => t.pipsInProfit).sum })
        .mkString(System.lineSeparator())
  }
}
