package com.mfuhrmann.trading.backtesting.trade.strategy

import java.time
import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.Trade
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio
import com.mfuhrmann.trading.backtesting.trade.risk.RiskCalculator

import scala.util.Random

class RandomStrategy(portfolio: Portfolio, riskCalculator: RiskCalculator, params: StrategyParams) extends AbstractStrategy(portfolio, riskCalculator, params) {


  override def shouldBuy(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean = Random.nextFloat() > 0.99995

  override def shouldSell(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean = Random.nextFloat() > 0.99995

  override def shouldTakeProfit(trade: Trade): Boolean = {

    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    trade.calculatePips(lastPrice.getPriceForClosingTrade(trade)) >= params.tp
  }

  override def shouldStopLoss(trade: Trade, tickTime: Instant): Boolean = {


    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    trade.calculatePips(lastPrice.getPriceForClosingTrade(trade)) <= -params.stopLoss

  }
}

