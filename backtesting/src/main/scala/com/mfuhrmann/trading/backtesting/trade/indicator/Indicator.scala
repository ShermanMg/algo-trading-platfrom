package com.mfuhrmann.trading.backtesting.trade.indicator

trait Indicator {

  def calculate(values:Seq[BigDecimal]): BigDecimal

}
