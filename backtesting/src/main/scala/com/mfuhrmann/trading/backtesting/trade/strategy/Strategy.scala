package com.mfuhrmann.trading.backtesting.trade.strategy

import java.time.{Instant, LocalDate, ZoneId}

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio
import com.mfuhrmann.trading.backtesting.trade.risk.RiskCalculator
import com.mfuhrmann.trading.backtesting.trade._

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

trait Strategy {

  def shouldBuy(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean

  def shouldSell(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean

  def shouldTakeProfit(trade: Trade): Boolean

  def shouldStopLoss(trade: Trade, tickTime: Instant): Boolean

  def onNextCandle(candle: DataSourceCandle, historicalData: Boolean = false)

  def onNextTick(symbol: MarketSymbol, bidAskPriceCandlePrice: BidAskPrice, tickTime: Instant)

  def finish(): StrategyResult

  def getStrategyResult: StrategyResult

  def getStrategyParams: StrategyParams

}

abstract class AbstractStrategy(val portfolio: Portfolio, riskCalculator: RiskCalculator, val params: StrategyParams) extends Strategy {

  val pastTicks: mutable.Map[trade.MarketSymbol, ListBuffer[BidAskPrice]] = new mutable.HashMap[trade.MarketSymbol, ListBuffer[BidAskPrice]]().withDefault(_ => ListBuffer())
  val pastCandles: mutable.Map[trade.MarketSymbol, ListBuffer[DataSourceCandle]] = new mutable.HashMap[trade.MarketSymbol, ListBuffer[DataSourceCandle]]().withDefault(_ => ListBuffer())
  val statsCollector: StrategyStatsCollector = new StrategyStatsCollector(portfolio)
  var localDate: LocalDate = _


  def onNextTick(symbol: MarketSymbol, bidAskPrice: BidAskPrice, tickTime: Instant): Unit = {

    updateTicks(symbol, bidAskPrice)

    portfolio.onNextTick(symbol, bidAskPrice, tickTime)


    portfolio.closeProfitableThatMatch(shouldTakeProfit, symbol, bidAskPrice)
    portfolio.closeLoosingThatMatch(shouldStopLoss, symbol, bidAskPrice)

    val positionSize: BigDecimal = 0.1

    if (canBuy(symbol, bidAskPrice, positionSize)) {
      portfolio.buy(symbol, positionSize, bidAskPrice, tickTime)

    } else if (canSell(symbol, bidAskPrice, positionSize)) {
      portfolio.sell(symbol, positionSize, bidAskPrice, tickTime)
    }

    statsCollector.onNext(symbol, bidAskPrice.getAveraged, tickTime)
  }


  def onNextCandleClose(symbol: MarketSymbol, candlePrice: DataSourceCandle, tickTime: Instant, historicalData: Boolean = false): Unit = {

    updateCandles(symbol, candlePrice)


    //    statsCollector.onNext(symbol, candlePrice, tickTime)
  }

  private def canSell(symbol: MarketSymbol, bidAskPrice: BidAskPrice, positionSize: BigDecimal): Boolean = {
    shouldSell(symbol, bidAskPrice) &&
      portfolio.canSell(symbol, positionSize, bidAskPrice) &&
      riskCalculator.getRisk(positionSize, symbol, TradeType.BUY).<=(BigDecimal.valueOf(params.riskThreshold)
      )
  }

  private def canBuy(symbol: MarketSymbol, bidAskPrice: BidAskPrice, positionSize: BigDecimal): Boolean = {

    shouldBuy(symbol, bidAskPrice) &&
      portfolio.canBuy(symbol, positionSize, bidAskPrice) &&
      riskCalculator.getRisk(positionSize, symbol, TradeType.SELL).<=(BigDecimal.valueOf(params.riskThreshold)
      )
  }

  private def updateTicks(symbol: MarketSymbol, candlePrice: BidAskPrice): Unit = {

    val decimals: ListBuffer[BidAskPrice] = pastTicks.getOrElseUpdate(symbol, ListBuffer())
    if (decimals.size >= params.maxTicksCount) {
      decimals.remove(0)
    }
    decimals.append(candlePrice)
  }

  private def updateCandles(symbol: MarketSymbol, candlePrice: DataSourceCandle): Unit = {
    val decimals: ListBuffer[DataSourceCandle] = pastCandles.getOrElseUpdate(symbol, ListBuffer())
    if (decimals.size >= params.maxTicksCount) {
      decimals.remove(0)
    }
    decimals.append(candlePrice)
  }

  override def finish(): StrategyResult = {
    val lastCandles: Map[MarketSymbol, (BidAskPrice, Instant)] = pastTicks
      .mapValues(prices => (prices.last, Instant.now()))
      .toMap

    portfolio.finish(lastCandles)
    statsCollector.onNext(null, null, null)

    getStrategyResult
  }


  def logProgress(candle: DataSourceCandle): Unit = {

    val date = candle.getOpenTime.atZone(ZoneId.systemDefault()).toLocalDate
    if (!date.equals(localDate)) {
      println(date)
      localDate = date
    }
  }

  override def onNextCandle(candle: DataSourceCandle, historicalData: Boolean): Unit = {

    logProgress(candle)

    val symbol: MarketSymbol = new ForexMarketSymbol(candle.getSymbol)

    //    onNextTick(symbol, candle.getOpen, candle.getOpenTime)

    if (candle.getOpen.doubleValue() > candle.getClose.doubleValue()) {
      //      onNextTick(symbol, candle.getHigh, candle.getOpenTime.plus(candle.getDuration.dividedBy(1)))
      //      onNextTick(symbol, candle.getLow, candle.getOpenTime.plus(candle.getDuration.dividedBy(1)))
    } else {
      //      onNextTick(symbol, candle.getLow, candle.getOpenTime.plus(candle.getDuration.dividedBy(1)))
      //      onNextTick(symbol, candle.getHigh, candle.getOpenTime.plus(candle.getDuration.dividedBy(1)))
    }

    //    onNextCandleClose(symbol, candle.getClose, candle.getOpenTime.plus(candle.getDuration), historicalData)
    updateCandles(new ForexMarketSymbol(candle.getSymbol), candle)

  }

  final override def getStrategyResult: StrategyResult = {
    portfolio.getStrategyResult
  }

  override def getStrategyParams: StrategyParams = params

}
