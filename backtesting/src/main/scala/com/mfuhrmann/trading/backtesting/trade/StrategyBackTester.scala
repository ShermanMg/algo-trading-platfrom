package com.mfuhrmann.trading.backtesting.trade

import com.mfuhrmann.trading.backtesting.trade.datasources.{DataSource, DataSourceWalker}
import com.mfuhrmann.trading.backtesting.trade.strategy.{BidAskPrice, Strategy}

class StrategyBackTester(val strategy: Strategy) {


  def run(dataSource: DataSource): StrategyResult = {

    dataSource.iterateOverDataSet(new BackTesterDataSourceWalker(strategy))

    strategy.finish()

    strategy.getStrategyResult
  }


}

class BackTesterDataSourceWalker(val strategy: Strategy) extends DataSourceWalker {

  override def onNextCandle(dataSourceCandle: DataSourceCandle): Unit = {
    strategy.onNextCandle(dataSourceCandle, historicalData = true)

  }

  override def onNextTick(dataSourceTick: DataSourceTick): Unit = {
    strategy.onNextTick(dataSourceTick.getMarketSymbol, new BidAskPrice(marketSymbol =, dataSourceTick.getBid, dataSourceTick.getAsk), dataSourceTick.getTime)
  }
}
