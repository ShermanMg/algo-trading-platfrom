package com.mfuhrmann.trading.backtesting.trade.indicator

class StochasticOscillator(val range: Int) extends Indicator {


  override def calculate(values: Seq[BigDecimal]): BigDecimal = {


    if (values.length < range) {
      //TODO better way ?
      return 50
    }
    val c = values.last
    val lowest = values.takeRight(range).min
    val highest = values.takeRight(range).max

    if (highest - lowest == 0) {
      return 50
    }


    ((c - lowest) / (highest - lowest)) * 100


  }


}
