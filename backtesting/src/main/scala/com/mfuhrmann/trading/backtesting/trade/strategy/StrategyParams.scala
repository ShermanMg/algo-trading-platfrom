package com.mfuhrmann.trading.backtesting.trade.strategy

class StrategyParams(val riskThreshold: Double, val stopLoss: Int, val tp: Int, val maxTicksCount: Int) {


  //  override def toString = s"StrategyParams(riskThreshold=$riskThreshold, ticksCount=$ticksCount, delta=$delta, smaIndicatorValue=$smaIndicatorValue, stopLoss=$stopLoss, tp=$tp)"
}
