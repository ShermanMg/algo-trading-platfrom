package com.mfuhrmann.trading.backtesting.trade

import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio

class StrategyStatsCollector(portfolio: Portfolio) {

  var maxDrawdown: Double = 0.0

  var lowestPips: Long = 0

  var highestPips: Long = 0

  var profitFactor: Double = 0.0


  def onNext(symbol: MarketSymbol, candlePrice: BigDecimal, tickTime: Instant): Unit = {

    lowestPips = Math.min(lowestPips, portfolio.getPipsProfit)
    highestPips = Math.max(highestPips, portfolio.getPipsProfit)

    if (lowestPips != 0) {
      profitFactor = Math.abs(highestPips).doubleValue() / lowestPips
    }

  }

  override def toString: String = {

    "Stats collector: " + System.lineSeparator() +
      "Max Drawdown:" + maxDrawdown + System.lineSeparator() +
      "Highest pips: " + highestPips + System.lineSeparator() +
      "Lowest pips: " + lowestPips + System.lineSeparator() +
      "Profiit factor: " + profitFactor
  }

}

class DrawDownCalculator {

}