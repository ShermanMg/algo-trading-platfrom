package com.mfuhrmann.trading.backtesting.trade.risk

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.portfolio.{Portfolio, SimpleForexPortfolio}

class NoRiskCalculator(portfolio: Portfolio) extends AbstractRiskCalculator(portfolio = portfolio) {
  override def getRisk(positionSize: BigDecimal, symbol: trade.MarketSymbol, tradeType: TradeType): BigDecimal = 0.0

}