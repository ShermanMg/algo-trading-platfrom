package com.mfuhrmann.trading.backtesting.trade

import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.CloseReason.CloseReason
import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.portfolio.PortfolioParams

class Trade(val symbol: MarketSymbol, val openPrice: BigDecimal, val openTime: Instant, val positionSize: BigDecimal, val tradeType: TradeType, val tradeId: Long) {

  def close(currentPrice: BigDecimal, tickTime: Instant, closeReason: CloseReason): ClosedTrade = {
    new ClosedTrade(symbol, openPrice, openTime, positionSize, tradeType, currentPrice, tickTime, tradeId, closeReason)
  }

  def calculatePips(closePrice: BigDecimal): Long = {

    tradeType match {
      case TradeType.BUY =>
        val priceDiff: BigDecimal = closePrice.-(openPrice) /*- (PortfolioParams.SPREAD * 2)*/
        priceDiff.*(getPipsDivideValue(symbol)).longValue()

      case TradeType.SELL =>
        val priceDiff: BigDecimal = openPrice.-(closePrice) /*- (PortfolioParams.SPREAD * 2)*/
        priceDiff.*(getPipsDivideValue(symbol)).longValue()
    }
  }

  def getPipsDivideValue(symbol: trade.MarketSymbol): Int = {
    if (symbol.getName.contains("JPY")) {
      return 100
    }
    10000
  }
}

class ClosedTrade(symbol: MarketSymbol, openPrice: BigDecimal, openTime: Instant, positionSize: BigDecimal, tradeType: TradeType, val closePrice: BigDecimal, val closeTime: Instant, tradeId: Long, val closeReason: CloseReason) extends
  Trade(symbol, openPrice, openTime, positionSize, tradeType, tradeId) {

  val pipsInProfit: Long = calculatePips(closePrice)


}

object TradeType extends Enumeration {

  type TradeType = Value


  val BUY, SELL = Value

  def parseFromCmd(cmd: Int): TradeType = {
    cmd match {
      case 0 =>
        TradeType.BUY
      case 1 =>
        TradeType.SELL
    }
  }

}

object CloseReason extends Enumeration {

  type CloseReason = Value


  val SL, TP, REVERSE_BUY, REVERSE_SELL, STRATEGY_FINISH, OTHER = Value

}