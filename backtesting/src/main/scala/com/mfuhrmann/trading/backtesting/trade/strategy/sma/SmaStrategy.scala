package com.mfuhrmann.trading.backtesting.trade.strategy.sma

import java.time
import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.{Trade, TradeType}
import com.mfuhrmann.trading.backtesting.trade.indicator.SmaIndicator
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio
import com.mfuhrmann.trading.backtesting.trade.risk.RiskCalculator
import com.mfuhrmann.trading.backtesting.trade.strategy.{AbstractStrategy, BidAskPrice}

import scala.collection.mutable.ListBuffer

class SmaStrategy(portfolio: Portfolio, riskCalculator: RiskCalculator, params: SmaStrategyParams) extends AbstractStrategy(portfolio, riskCalculator, params) {

  val smaIndicator: SmaIndicator = new SmaIndicator(params.smaIndicatorValue)

  override def shouldBuy(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean = {

    val decimals: ListBuffer[BigDecimal] = pastTicks.getOrElse(symbol, ListBuffer())
      .map(bidAsk => bidAsk.askPrice)

    //    val decimals: List[BigDecimal] = pastCandles.getOrElse(symbol, ListBuffer())
    //      .map(candle => BigDecimal.apply(candle.g  etClose))
    //      .toList

    if (decimals.length < smaIndicator.range) {
      return false
    }
    val smaValue = smaIndicator.calculate(decimals)
    candlePrice.getPriceForOpeningTrade(TradeType.BUY) < smaValue - params.delta * smaValue
  }

  override def shouldSell(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean = {

    val decimals: ListBuffer[BigDecimal] = pastTicks.getOrElse(symbol, ListBuffer())
      .map(bidAsk => bidAsk.bidPrice)

    //    val decimals: List[BigDecimal] = pastCandles.getOrElse(symbol, ListBuffer())
    //      .map(candle => BigDecimal.apply(candle.getClose))
    //      .toList
    if (decimals.length < smaIndicator.range) {
      return false
    }
    val smaValue = smaIndicator.calculate(decimals)
    candlePrice.getPriceForOpeningTrade(TradeType.SELL) > smaValue + params.delta * smaValue
  }

  override def shouldTakeProfit(trade: Trade): Boolean = {

    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    trade.calculatePips(lastPrice.getPriceForClosingTrade(trade)) >= params.tp
  }

  override def shouldStopLoss(trade: Trade, tickTime: Instant): Boolean = {

    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    val pipsDiff = trade.calculatePips(lastPrice.getPriceForClosingTrade(trade))
    pipsDiff <= -params.stopLoss ||
      (pipsDiff < 1 &&
        !time.Duration.between(trade.openTime, tickTime).minusSeconds(params.ticksCount).isNegative)

  }
}
