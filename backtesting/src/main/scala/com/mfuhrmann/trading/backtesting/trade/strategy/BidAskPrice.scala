package com.mfuhrmann.trading.backtesting.trade.strategy

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.{MarketSymbol, Trade, TradeType}

class BidAskPrice(val marketSymbol: MarketSymbol, val bidPrice: BigDecimal, val askPrice: BigDecimal) {


  def getAveraged: BigDecimal = {

    (bidPrice + askPrice) / 2
  }


  def getSpread(): BigDecimal = {
    askPrice - bidPrice
  }

  def getSpreadInPips(): BigDecimal = {
    (askPrice - bidPrice) * getPipsDivideValue(marketSymbol)
  }

  def getPipsDivideValue(symbol: trade.MarketSymbol): Int = {
    if (symbol.getName.contains("JPY")) {
      return 100
    }
    10000
  }

  def getPriceForOpeningTrade(tradeType: TradeType): BigDecimal = {
    tradeType match {
      case TradeType.BUY =>
        askPrice
      case TradeType.SELL =>
        bidPrice
    }
  }

  def getPriceForClosingTrade(trade: Trade): BigDecimal = {
    trade.tradeType match {
      case TradeType.BUY =>
        bidPrice
      case TradeType.SELL =>
        askPrice
    }
  }


}
