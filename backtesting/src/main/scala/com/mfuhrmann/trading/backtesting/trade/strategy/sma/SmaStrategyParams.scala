package com.mfuhrmann.trading.backtesting.trade.strategy.sma

import com.mfuhrmann.trading.backtesting.trade.strategy.StrategyParams

class SmaStrategyParams(riskThreshold: Double, val ticksCount: Int, val delta: Double, val smaIndicatorValue: Int, stopLoss: Int, tp: Int) extends StrategyParams(riskThreshold, stopLoss, tp, ticksCount) {


  override def toString = s"SmaStrategyParams(ticksCount=$ticksCount, delta=$delta, smaIndicatorValue=$smaIndicatorValue)"
}
