package com.mfuhrmann.trading.backtesting.trade

import java.time.{Duration, Instant}

trait DataSourceCandle {
  def getSymbol: String

  def getOpenTime: Instant

  def getOpen: BigDecimal

  def getHigh: BigDecimal

  def getLow: BigDecimal

  def getClose: BigDecimal

  def getVolume: BigDecimal

  def getDuration: Duration
}
