package com.mfuhrmann.trading.backtesting.trade.risk

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.{Trade, TradeType}
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio

class CurrencyExposureRiskCalculator(portfolio: Portfolio) extends AbstractRiskCalculator(portfolio = portfolio) {

  var exposureMap: CurrencyExposureMap = new CurrencyExposureMap(portfolio)

  var openTrades: List[Trade] = Nil

  override def getRisk(positionSize: BigDecimal, symbol: trade.MarketSymbol, tradeType: TradeType): BigDecimal = {
    if (!portfolio.getOpenTrades.equals(openTrades)) {
      exposureMap = new CurrencyExposureMap(portfolio)
      openTrades = portfolio.getOpenTrades
    }

    exposureMap.getRiskPerAsset(symbol, tradeType)

  }

}

class CurrencyExposureMap(val portfolio: Portfolio) {


  val buyingAssets: Map[String, BigDecimal] = portfolio.getOpenTrades
    .groupBy(t => t.symbol.getBuyingAsset)
    .mapValues(trades => trades.map(mapPositionToNumber).sum)


  val sellingAssets: Map[String, BigDecimal] = portfolio.getOpenTrades
    .groupBy(t => t.symbol.getSellingAsset)
    .mapValues(trades => trades
      .map(mapPositionToNumber)
      .map(v => v * (-1))
      .sum)

  var allKeys: Set[String] = buyingAssets.keySet.++(sellingAssets.keySet)

  val positionsByAsset: Map[String, BigDecimal] = allKeys
    .map(key => (key, getValueOrDefaultFromMap(buyingAssets, key)
      .+(getValueOrDefaultFromMap(sellingAssets, key))))
    .toMap


  val allPositions: BigDecimal = allKeys.toSeq
    .map(k => getValueOrDefaultFromMap(buyingAssets, k).abs + getValueOrDefaultFromMap(sellingAssets, k).abs)
    .sum

  def getRiskPerAsset(symbol: trade.MarketSymbol, tradeType: TradeType): BigDecimal = {

    if (allPositions == 0) {
      return BigDecimal.valueOf(0)
    }

    tradeType match {
      case TradeType.BUY =>
        getValueOrDefaultFromMap(positionsByAsset, symbol.getBuyingAsset)
          .-(getValueOrDefaultFromMap(positionsByAsset, symbol.getSellingAsset))
          ./(allPositions)
      case TradeType.SELL =>
        getValueOrDefaultFromMap(positionsByAsset, symbol.getSellingAsset)
          .-(getValueOrDefaultFromMap(positionsByAsset, symbol.getBuyingAsset))
          ./(allPositions)
    }

  }

  private def getValueOrDefaultFromMap(assets: Map[String, BigDecimal], k: String) = {
    assets.getOrElse(k, BigDecimal.valueOf(0))
  }

  private def mapPositionToNumber: Trade => BigDecimal = {

    t => {
      if (t.tradeType == TradeType.BUY) {
        t.positionSize
      } else {
        -t.positionSize
      }
    }

  }


}
