package com.mfuhrmann.trading.backtesting.trade.portfolio

import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.CloseReason.CloseReason
import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.strategy.BidAskPrice
import com.mfuhrmann.trading.backtesting.trade.utils.Money
import com.mfuhrmann.trading.backtesting.trade._
import org.graalvm.compiler.asm.amd64.AMD64Assembler.VexFloatCompareOp.Predicate

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

trait Portfolio {
  def closeLoosingThatMatch(shouldStopLoss: (Trade, Instant) => Boolean, symbol: trade.MarketSymbol, bidAskPriceCandlePrice: BidAskPrice)

  def closeProfitableThatMatch(predicate: Trade => Boolean, symbol: trade.MarketSymbol, bidAskPriceCandlePrice: BidAskPrice)

  def onNextTick(symbol: MarketSymbol, bidAskPriceCandlePrice: BidAskPrice, tickTime: Instant): Unit

  def canBuy(symbol: trade.MarketSymbol, value: BigDecimal, bidAskPrice: BidAskPrice): Boolean

  def canSell(symbol: trade.MarketSymbol, value: BigDecimal, bidAskPrice: BidAskPrice): Boolean

  def buy(symbol: trade.MarketSymbol, amount: BigDecimal, price: BidAskPrice, tickTime: Instant): Unit

  def sell(symbol: trade.MarketSymbol, amount: BigDecimal, price: BidAskPrice, tickTime: Instant): Unit

  def finish(candleClosePrices: Map[trade.MarketSymbol, (BidAskPrice, Instant)])

  def getInitialInvestment: BigDecimal

  def getPipsProfit: Long

  def getOpenTrades: List[Trade]

  def getClosedTrades: List[ClosedTrade]

  def getStrategyResult: StrategyResult

}

object PortfolioParams {
  val SPREAD: BigDecimal = BigDecimal.valueOf(0.0001)
}

abstract class AbstractPorfolio(val money: Money, val leverage: Int, tradesExecutor: TradesExecutor) extends Portfolio {

  protected var openTrades: mutable.Set[Trade] = new mutable.HashSet[Trade]()
  protected val closedTrades: ListBuffer[ClosedTrade] = ListBuffer()

  val moneyManager: MoneyManager = new MoneyManager(money, leverage)
  var pipsProfit: Long = 0
  var openTradesCount = 0
  private var lastTick: Instant = _

  final override def getPipsProfit: Long = pipsProfit

  override def getInitialInvestment: BigDecimal = 0.0

  override def canBuy(symbol: trade.MarketSymbol, value: BigDecimal, price: BidAskPrice): Boolean = {
    moneyManager.canTrade(symbol, value, price, TradeType.BUY)
  }

  override def canSell(symbol: trade.MarketSymbol, value: BigDecimal, price: BidAskPrice): Boolean = {
    moneyManager.canTrade(symbol, value, price, TradeType.SELL)
  }

  override def onNextTick(symbol: MarketSymbol, bidAskPriceCandlePrice: BidAskPrice, tickTime: Instant): Unit = {
    moneyManager.onNextTick(symbol, bidAskPriceCandlePrice, tickTime)
    lastTick = tickTime
  }

  override def closeProfitableThatMatch(predicate: Trade => Boolean, symbol: trade.MarketSymbol, bidAskPrice: BidAskPrice): Unit = {
    closeTrades((t, _) => predicate(t), symbol, bidAskPrice, CloseReason.TP)
  }

  override def closeLoosingThatMatch(predicate: (Trade, Instant) => Boolean, symbol: trade.MarketSymbol, bidAskPrice: BidAskPrice): Unit = {
    closeTrades(predicate, symbol, bidAskPrice, CloseReason.SL)
  }

  override def finish(candleClosePrices: Map[trade.MarketSymbol, (BidAskPrice, Instant)]): Unit = {
    candleClosePrices.foreach(candleClosePrice =>
      closeTrades((p, _) => true, candleClosePrice._1, candleClosePrice._2._1, CloseReason.STRATEGY_FINISH))
  }

  final override def buy(symbol: trade.MarketSymbol, amount: BigDecimal, price: BidAskPrice, tickTime: Instant): Unit = {

    closeTrades((t, _) => t.tradeType == TradeType.SELL, symbol, price, CloseReason.REVERSE_BUY)

    tradesExecutor.buy(symbol, amount)
      .map(id => new Trade(symbol, price.getPriceForOpeningTrade(TradeType.BUY), tickTime, amount, TradeType.BUY, id))
      .foreach(t => {
        openTrades.add(t)
        moneyManager.tradeOpened(t)
      })
    openTradesCount = Math.max(openTradesCount, openTrades.size)
  }


  final override def sell(symbol: trade.MarketSymbol, amount: BigDecimal, price: BidAskPrice, tickTime: Instant): Unit = {
    closeTrades((t, _) => t.tradeType == TradeType.BUY, symbol, price, CloseReason.REVERSE_SELL)


    tradesExecutor.sell(symbol, amount)
      .map(id => new Trade(symbol, price.getPriceForOpeningTrade(TradeType.SELL), tickTime, amount, TradeType.SELL, id))
      .foreach(t => {
        openTrades.add(t)
        moneyManager.tradeOpened(t)
      })
    openTradesCount = Math.max(openTradesCount, openTrades.size)

  }

  private def closeTrades(predicate: (Trade, Instant) => Boolean, symbol: MarketSymbol, price: BidAskPrice, closeReason: CloseReason): Unit = {

    val toRemove = openTrades
      .filter(t => t.symbol.equals(symbol) && predicate(t, lastTick))
      .map(t => {
        closeTrade(t, price.getPriceForClosingTrade(t), lastTick, closeReason)
        t
      }).toSet
    toRemove.foreach(openTrades.remove)
  }

  def closeTrade(trade: Trade, currentPrice: BigDecimal, tickTime: Instant, closeReason: CloseReason): Unit = {

    val closedTrade: ClosedTrade = trade.close(currentPrice, tickTime, closeReason)
    val profitPips: Long = tradesExecutor.close(closedTrade).getOrElse(0)
    pipsProfit += profitPips

    closedTrades.append(closedTrade)
    moneyManager.tradeClosed(closedTrade)

  }

  def synchronizePortfolio(trades: List[Trade]): Unit = {

    openTrades = openTrades ++ trades
    openTrades.foreach(t => moneyManager.tradeOpened(t))
  }


  override def getOpenTrades: List[Trade] = openTrades.toList

  override def getClosedTrades: List[ClosedTrade] = closedTrades.toList

  override def getStrategyResult: StrategyResult = {
    new StrategyResult(getPipsProfit, getClosedTrades, moneyManager.maxTrades)
  }


}

class TradeKey(val marketSymbol: MarketSymbol, val tradeType: TradeType) {

  def canEqual(other: Any): Boolean = other.isInstanceOf[TradeKey]

  override def equals(other: Any): Boolean = other match {
    case that: TradeKey =>
      (that canEqual this) &&
        marketSymbol == that.marketSymbol &&
        tradeType == that.tradeType
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(marketSymbol, tradeType)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
