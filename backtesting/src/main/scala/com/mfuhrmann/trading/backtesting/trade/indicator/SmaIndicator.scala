package com.mfuhrmann.trading.backtesting.trade.indicator

class SmaIndicator(val range: Int) extends Indicator {


  override def calculate(values: Seq[BigDecimal]): BigDecimal = {


    if (values.length < 1) {
      return 0.0
    }

    values.sum / values.length
  }
}
