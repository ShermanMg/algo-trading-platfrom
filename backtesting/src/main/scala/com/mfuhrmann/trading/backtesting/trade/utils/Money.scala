package com.mfuhrmann.trading.backtesting.trade.utils

import java.util.Currency

import scala.util.{Failure, Success, Try}

class Money(val money: BigDecimal, val currency: Currency) {


  def >(that: Money): Boolean = {

    if (that.currency.getCurrencyCode.equals(currency.getCurrencyCode)) {
      return money > that.money
    }
    false
  }

  def <(that: Money): Boolean = {

    if (that.currency.getCurrencyCode.equals(currency.getCurrencyCode)) {
      return money < that.money
    }
    false
  }


  def -(that: Money): Try[Money] = {

    if (that.currency.getCurrencyCode.equals(currency.getCurrencyCode)) {
      return Success(new Money(money - that.money, currency))
    }
    Failure(new RuntimeException("Incompatible currencies " + that.currency + " " + currency))
  }


  def +(that: Money): Try[Money] = {

    if (that.currency.getCurrencyCode.equals(currency.getCurrencyCode)) {
      return Success(new Money(money + that.money, currency))
    }
    Failure(new RuntimeException("Incompatible currencies " + that.currency + " " + currency))
  }


}
