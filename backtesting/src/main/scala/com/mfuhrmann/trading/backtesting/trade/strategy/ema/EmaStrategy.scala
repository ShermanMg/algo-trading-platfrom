package com.mfuhrmann.trading.backtesting.trade.strategy.ema

import java.time
import java.time.Instant

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.{MarketSymbol, Trade, TradeType}
import com.mfuhrmann.trading.backtesting.trade.indicator.{EmaIndicator, SmaIndicator}
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio
import com.mfuhrmann.trading.backtesting.trade.risk.RiskCalculator
import com.mfuhrmann.trading.backtesting.trade.strategy.{AbstractStrategy, BidAskPrice}
import com.mfuhrmann.trading.backtesting.trade.strategy.sma.SmaStrategyParams

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class EmaStrategy(portfolio: Portfolio, riskCalculator: RiskCalculator, params: SmaStrategyParams) extends AbstractStrategy(portfolio, riskCalculator, params) {

  val indicatorsMap = new mutable.HashMap[trade.MarketSymbol, EmaIndicator]()
  val emaIndicator: EmaIndicator = new EmaIndicator(params.smaIndicatorValue)

  override def shouldBuy(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean = {

    val decimals: ListBuffer[BigDecimal] = pastTicks.getOrElse(symbol, ListBuffer())
      .map(bidAsk => bidAsk.askPrice)

    //    val decimals: List[BigDecimal] = pastCandles.getOrElse(symbol, ListBuffer())
    //      .map(candle => BigDecimal.apply(candle.g  etClose))
    //      .toList

    if (decimals.length < getIndicator(symbol).range) {
      return false
    }
    val smaValue = getIndicator(symbol).calculate(decimals)
    candlePrice.getPriceForOpeningTrade(TradeType.BUY) < smaValue - params.delta * smaValue
  }

  override def shouldSell(symbol: trade.MarketSymbol, candlePrice: BidAskPrice): Boolean = {

    val decimals: ListBuffer[BigDecimal] = pastTicks.getOrElse(symbol, ListBuffer())
      .map(bidAsk => bidAsk.bidPrice)

    //    val decimals: List[BigDecimal] = pastCandles.getOrElse(symbol, ListBuffer())
    //      .map(candle => BigDecimal.apply(candle.getClose))
    //      .toList
    if (decimals.length < getIndicator(symbol).range) {
      return false
    }
    val smaValue = getIndicator(symbol).calculate(decimals)
    candlePrice.getPriceForOpeningTrade(TradeType.SELL) > smaValue + params.delta * smaValue
  }

  private def getIndicator(marketSymbol: MarketSymbol): EmaIndicator = {
    indicatorsMap.getOrElseUpdate(marketSymbol, new EmaIndicator(params.smaIndicatorValue))
  }

  override def shouldTakeProfit(trade: Trade): Boolean = {

    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    trade.calculatePips(lastPrice.getPriceForClosingTrade(trade)) >= params.tp
  }

  override def shouldStopLoss(trade: Trade, tickTime: Instant): Boolean = {

    val lastPrice = pastTicks.getOrElse(trade.symbol, List()).last

    val pipsDiff = trade.calculatePips(lastPrice.getPriceForClosingTrade(trade))
    pipsDiff <= -params.stopLoss ||
      (pipsDiff < 1 &&
        !time.Duration.between(trade.openTime, tickTime).minusSeconds(params.ticksCount).isNegative)

  }
}
