package com.mfuhrmann.trading.backtesting.trade.portfolio

import java.lang
import java.time.Instant
import java.util.Optional

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.ClosedTrade

trait TradesExecutor {

  def buy(symbol: trade.MarketSymbol, amount: BigDecimal): Option[Long]

  def sell(symbol: trade.MarketSymbol, amount: BigDecimal): Option[Long]

  def close(closedTrade: ClosedTrade): Option[Long]
}
