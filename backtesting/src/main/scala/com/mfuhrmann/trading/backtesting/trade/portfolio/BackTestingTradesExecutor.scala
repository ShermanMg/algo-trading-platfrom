package com.mfuhrmann.trading.backtesting.trade.portfolio

import java.lang
import java.time.Instant
import java.util.Optional

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.{ClosedTrade, MarketSymbol}

class BackTestingTradesExecutor extends TradesExecutor {
  override def buy(symbol: MarketSymbol, amount: BigDecimal): Option[Long] = {
    Option(1)
  }

  override def sell(symbol: MarketSymbol, amount: BigDecimal): Option[Long] = {
    Option(1)
  }

  override def close(closedTrade: ClosedTrade): Option[Long] = {
    Option(closedTrade.pipsInProfit)
  }
}
