package com.mfuhrmann.trading.backtesting.trade.risk

import com.mfuhrmann.trading.backtesting.trade
import com.mfuhrmann.trading.backtesting.trade.TradeType.TradeType
import com.mfuhrmann.trading.backtesting.trade.portfolio.Portfolio

abstract class AbstractRiskCalculator(portfolio: Portfolio) extends RiskCalculator {
}

trait RiskCalculator {


  def getRisk(positionSize: BigDecimal, symbol: trade.MarketSymbol, tradeType: TradeType): BigDecimal

}
