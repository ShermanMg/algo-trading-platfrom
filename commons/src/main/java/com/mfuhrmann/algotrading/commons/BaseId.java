package com.mfuhrmann.algotrading.commons;

import org.springframework.data.annotation.Id;

public abstract class BaseId {

    @Id
    private String id;

    public String getId() {
        return id;
    }
}
