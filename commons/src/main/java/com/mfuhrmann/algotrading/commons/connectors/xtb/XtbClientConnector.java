package com.mfuhrmann.algotrading.commons.connectors.xtb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pro.xstore.api.message.codes.PERIOD_CODE;
import pro.xstore.api.message.command.APICommandFactory;
import pro.xstore.api.message.error.APICommunicationException;
import pro.xstore.api.message.response.AllSymbolsResponse;
import pro.xstore.api.message.response.ChartResponse;
import pro.xstore.api.message.response.LoginResponse;
import pro.xstore.api.sync.Credentials;
import pro.xstore.api.sync.ServerData;
import pro.xstore.api.sync.SyncAPIConnector;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class XtbClientConnector {

    private static final Logger LOGGER = LoggerFactory.getLogger(XtbClientConnector.class);

    @Value("${xtb.login}")
    private String apiLogin;

    @Value("${xtb.password}")
    private String apiPassword;


    private SyncAPIConnector connector;
    private List<SyncAPIConnector> connectors = new ArrayList<>();
//    private final CandleStreamHandler candleStreamHandler;
//    private final XtbInstrumentService xtbInstrumentService;

//    @Autowired
//    XtbClientConnector(CandleStreamHandler candleStreamHandler, XtbInstrumentService xtbInstrumentService) {
//        this.candleStreamHandler = candleStreamHandler;
//        this.xtbInstrumentService = xtbInstrumentService;
//    }


    public Optional<ChartResponse> getChartResponseForDateRange(String symbol, Instant start, Instant end, PERIOD_CODE period_code) {

        try {
            return Optional.of(APICommandFactory.executeChartRangeCommand(
                    connector, symbol, period_code,
                    start.toEpochMilli(),
                    end.toEpochMilli(),
                    0L));

        } catch (Exception e) {

            LOGGER.error("Exception {} , {} , {} , {}", symbol, start, end, period_code);
            return Optional.empty();
        }

    }

    public AllSymbolsResponse getAllSymbols() {

        try {
            return APICommandFactory.executeAllSymbolsCommand(connector);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    @PostConstruct
    public void init() {
        this.connector = createConnector(ServerData.ServerEnum.DEMO);
    }


    @PreDestroy
    public void cleanup() {
        try {
            connector.unsubscribeCandles(List.of("BTCUSD", "LTCUSD"));
            System.out.println("unsubscribing");
        } catch (APICommunicationException e) {
            throw new RuntimeException(e);
        }

    }

//    public void initializeStreamingListener() {
//
//        try {
//
//            StreamingListener sl = new StreamingListener() {
//                @Override
//                public void receiveTradeRecord(STradeRecord tradeRecord) {
//                }
//
//
//                @Override
//                public void receiveCandleRecord(SCandleRecord candleRecord) {
//                    candleStreamHandler.onCandle(candleRecord);
//                }
//            };
//            connector.connectStream(sl);
//
//            List<String> xtbInstruments = xtbInstrumentService.getSelectedInstruments().stream()
//                    .map(XtbInstrument::getSymbol)
//                    .collect(Collectors.toList());
//
//            connector.subscribeCandles(xtbInstruments);
//
//
//            connector.subscribeTrades();
//        } catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//
//    }


    public SyncAPIConnector createConnector(ServerData.ServerEnum serverEnum) {
        try {
            Credentials credentials = new Credentials(apiLogin, apiPassword);

            connector = new SyncAPIConnector(serverEnum);
            LoginResponse loginResponse = APICommandFactory.executeLoginCommand(connector, credentials);
            System.out.println(loginResponse);
            connectors.add(connector);

        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }

        return connector;
    }


    @Scheduled(fixedRate = 1000 * 60)
    public void keepAliveStreams() {

        connectors.forEach(conn ->
        {
            try {
                APICommandFactory.executePingCommand(connector);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

}


