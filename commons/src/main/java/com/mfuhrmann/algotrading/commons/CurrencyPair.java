package com.mfuhrmann.algotrading.commons;

import org.springframework.data.mongodb.core.index.Indexed;

import java.util.Objects;

public class CurrencyPair {

    @Indexed
    private final String currency1;
    @Indexed
    private final String currency2;

    public CurrencyPair(String currency1, String currency2) {
        this.currency1 = currency1;
        this.currency2 = currency2;
    }

    public String getCurrency1() {
        return currency1;
    }

    public String getCurrency2() {
        return currency2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyPair that = (CurrencyPair) o;
        return Objects.equals(currency1, that.currency1) &&
                Objects.equals(currency2, that.currency2);
    }

    @Override
    public int hashCode() {

        return Objects.hash(currency1, currency2);
    }

    @Override
    public String toString() {
        return "CurrencyPair{" +
                "currency1='" + currency1 + '\'' +
                ", currency2='" + currency2 + '\'' +
                '}';
    }
}
