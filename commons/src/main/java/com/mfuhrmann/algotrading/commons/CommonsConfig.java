package com.mfuhrmann.algotrading.commons;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.mfuhrmann.algotrading.commons")
public class CommonsConfig {
}
