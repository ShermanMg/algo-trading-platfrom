/*
 * Copyright (c) 2017 Dukascopy (Suisse) SA. All Rights Reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * -Redistribution of source code must retain the above copyright notice, this
 *  list of conditions and the following disclaimer.
 *
 * -Redistribution in binary form must reproduce the above copyright notice,
 *  this list of conditions and the following disclaimer in the documentation
 *  and/or other materials provided with the distribution.
 *
 * Neither the name of Dukascopy (Suisse) SA or the names of contributors may
 * be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * This software is provided "AS IS," without a warranty of any kind. ALL
 * EXPRESS OR IMPLIED CONDITIONS, REPRESENTATIONS AND WARRANTIES, INCLUDING
 * ANY IMPLIED WARRANTY OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
 * OR NON-INFRINGEMENT, ARE HEREBY EXCLUDED. DUKASCOPY (SUISSE) SA ("DUKASCOPY")
 * AND ITS LICENSORS SHALL NOT BE LIABLE FOR ANY DAMAGES SUFFERED BY LICENSEE
 * AS A RESULT OF USING, MODIFYING OR DISTRIBUTING THIS SOFTWARE OR ITS
 * DERIVATIVES. IN NO EVENT WILL DUKASCOPY OR ITS LICENSORS BE LIABLE FOR ANY LOST
 * REVENUE, PROFIT OR DATA, OR FOR DIRECT, INDIRECT, SPECIAL, CONSEQUENTIAL,
 * INCIDENTAL OR PUNITIVE DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
 * OF LIABILITY, ARISING OUT OF THE USE OF OR INABILITY TO USE THIS SOFTWARE,
 * EVEN IF DUKASCOPY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 */
package com.mfuhrmann.algotrading.dukascopy.gui;

import com.dukascopy.api.Instrument;
import com.dukascopy.api.system.ISystemListener;
import com.mfuhrmann.algotrading.dukascopy.strategies.AdapterStrategy;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrument;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrumentService;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * This small program demonstrates how to initialize Dukascopy tester and start a strategy in GUI mode
 */
@SuppressWarnings("serial")
@Service
public class TesterMainGUIModeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(TesterMainGUIModeService.class);

    private static String jnlpUrl = "http://platform.dukascopy.com/demo/jforex.jnlp";
    private static String userName = "DEMO2MvNyN";
    private static String password = "MvNyN";
    private static String reportFileName = "report.html";
    private static Instrument instrument = Instrument.EURUSD;

    private static MyTesterWindow myTesterWindow;
    private static TesterClientRunner testerClientRunner;
    private final XtbInstrumentService xtbInstrumentService;
    private final Set<Instrument> instruments;


    public TesterMainGUIModeService(XtbInstrumentService xtbInstrumentService) {
        this.xtbInstrumentService = xtbInstrumentService;
        instruments = getInstruments();
    }

    @PostConstruct
    public void init() throws Exception {
        testerClientRunner = new TesterClientRunner();
        myTesterWindow = new MyTesterWindow(instruments, getTesterThread());
        myTesterWindow.showChartFrame();
    }

    public Thread getTesterThread() {


        Runnable r = () -> {
            try {
                testerClientRunner.start(
                        jnlpUrl,
                        userName,
                        password,
                        instruments,
                        myTesterWindow,
                        myTesterWindow,
                        getSystemListener(),
                        new AdapterStrategy());

            } catch (Exception e2) {
                LOGGER.error(e2.getMessage(), e2);
                e2.printStackTrace();
                myTesterWindow.resetButtons();
            }
        };
        return new Thread(r);
    }

    @NotNull
    private Set<Instrument> getInstruments() {
        Set<String> currencies = Set.of("EUR", "USD", "JPY", "GBP", "CHF", "CAD", "AUD");

        return xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> currencies.contains(xtbInstrument.getSymbol().substring(0, 3)) && currencies.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .map(XtbInstrument::getSymbol)
                .map(instrument -> instrument.substring(0, 3) + "/" + instrument.substring(3, 6))
                .map(Instrument::fromString)
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }


    private ISystemListener getSystemListener() {
        //set the listener that will receive system events
        return new ISystemListener() {
            @Override
            public void onStart(long processId) {
                LOGGER.info("Strategy started: " + processId);
                myTesterWindow.updateButtons();
            }

            @Override
            public void onStop(long processId) {
                LOGGER.info("Strategy stopped: " + processId);
                myTesterWindow.resetButtons();
                createReport(processId, reportFileName);
            }

            @Override
            public void onConnect() {
                LOGGER.info("Connected");
            }

            @Override
            public void onDisconnect() {
                //tester doesn't disconnect
            }
        };
    }

    private void createReport(long processId, String reportFileName) {
        File reportFile = new File(reportFileName);
        try {
            testerClientRunner.client.createReport(processId, reportFile);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
        }
        if (testerClientRunner.client.getStartedStrategies().size() == 0) {
            //Do nothing
        }
    }

}
