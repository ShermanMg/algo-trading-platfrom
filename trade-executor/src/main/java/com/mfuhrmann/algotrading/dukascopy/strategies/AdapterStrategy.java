package com.mfuhrmann.algotrading.dukascopy.strategies;

import com.dukascopy.api.*;
import com.mfuhrmann.trading.backtesting.trade.ClosedTrade;
import com.mfuhrmann.trading.backtesting.trade.DataSourceCandle;
import com.mfuhrmann.trading.backtesting.trade.ForexMarketSymbol;
import com.mfuhrmann.trading.backtesting.trade.MarketSymbol;
import com.mfuhrmann.trading.backtesting.trade.portfolio.SimpleForexPortfolio;
import com.mfuhrmann.trading.backtesting.trade.portfolio.TradesExecutor;
import com.mfuhrmann.trading.backtesting.trade.risk.CurrencyExposureRiskCalculator;
import com.mfuhrmann.trading.backtesting.trade.strategy.BidAskPrice;
import com.mfuhrmann.trading.backtesting.trade.strategy.Strategy;
import com.mfuhrmann.trading.backtesting.trade.strategy.scalping.ScalpingForexStrategy;
import com.mfuhrmann.trading.backtesting.trade.strategy.scalping.ScalpingStrategyParams;
import com.mfuhrmann.trading.backtesting.trade.utils.Money;
import org.jetbrains.annotations.NotNull;
import scala.Option;
import scala.math.BigDecimal;

import java.time.Duration;
import java.time.Instant;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

public class AdapterStrategy implements IStrategy {

    private Strategy strategy;
    private IEngine engine = null;
    private IIndicators indicators = null;
    private int tagCounter = 0;
    private double[] ma1 = new double[Instrument.values().length];
    private IConsole console;

    private Map<MarketSymbol, String> symbolsToString = new HashMap<>();
    private Map<String, MarketSymbol> stringsToSymbol = new HashMap<>();
    private long counter;

    @Override
    public void onStart(IContext context) throws JFException {
        engine = context.getEngine();
        indicators = context.getIndicators();
        this.console = context.getConsole();
        console.getOut().println("Started");

        TradesExecutor tradesExecutor = new TradesExecutor() {
            public Option<Object> buy(MarketSymbol symbol, BigDecimal amount) {
//                engine.submitOrder(getLabel(instrument), instrument, IEngine.OrderCommand.SELL, 0.01, 0, 0, tick.getAsk()
//                        + instrument.getPipValue() * 10, tick.getAsk() - instrument.getPipValue() * 15);
                String symbolString = symbolsToString.computeIfAbsent(symbol, marketSymbol -> marketSymbol.getBuyingAsset() + "/" + marketSymbol.getSellingAsset());
                try {
                    engine.submitOrder(symbol.getName() + ++counter, Instrument.fromString(symbolString), IEngine.OrderCommand.BUY, 0.01);
                } catch (JFException e) {
                    throw new RuntimeException(e);
                }

                return Option.<Object>apply(counter);
            }

            public Option<Object> sell(MarketSymbol symbol, BigDecimal amount) {

                String symbolString = symbolsToString.computeIfAbsent(symbol, marketSymbol -> marketSymbol.getBuyingAsset() + "/" + marketSymbol.getSellingAsset());
                try {
                    engine.submitOrder(symbol.getName() + ++counter, Instrument.fromString(symbolString), IEngine.OrderCommand.SELL, 0.01);
                } catch (JFException e) {
                    throw new RuntimeException(e);
                }

                return Option.<Object>apply(counter);

            }

            @Override
            public Option<Object> close(ClosedTrade closedTrade) {
                try {
                    IOrder order = engine.getOrder(closedTrade.symbol().getName() + closedTrade.tradeId());
                    order.setComment(closedTrade.closeReason().toString());
                    order.close();
                    return Option.<Object>apply(Math.round(order.getProfitLossInPips()));
                } catch (JFException e) {
                    throw new RuntimeException(e);
                }


            }


        };

        Money pln = new Money(BigDecimal.valueOf(50_000), Currency.getInstance("USD"));

        SimpleForexPortfolio portfolio = new SimpleForexPortfolio(pln, 200, tradesExecutor);
//        StrategyParams(riskThreshold=0.4, ticksCount=1600, delta=0.001, smaIndicatorValue=1600, stopLoss=6, tp=22)
//        strategy = new SmaStrategy(portfolio, new CurrencyExposureRiskCalculator(portfolio), new SmaStrategyParams(0.4, 1600, 0.001, 1600, 6, 22));
        strategy = new ScalpingForexStrategy(
                portfolio,
                new CurrencyExposureRiskCalculator(portfolio),
                new ScalpingStrategyParams(0.2, 1000, 50, 100, 6, 12, 14, scala.math.BigDecimal.valueOf(1.0)));
//        strategy = new RandomStrategy(portfolio, new CurrencyExposureRiskCalculator(portfolio), new ScalpingStrategyParams(0.4, 1000, 50, 100, 4, 12, 14));

//        strategy = new EmaStrategy(portfolio, new CurrencyExposureRiskCalculator(portfolio), new SmaStrategyParams(0.4, 1600, 0.0005, 1600, 6, 22));


    }

    @Override
    public void onTick(Instrument instrument, ITick tick) throws JFException {
        MarketSymbol marketSymbol = stringsToSymbol.computeIfAbsent(instrument.getName(), s -> new ForexMarketSymbol(s.substring(0, 3), s.substring(4, 7)));

        strategy.onNextTick(marketSymbol, new BidAskPrice(marketSymbol, BigDecimal.valueOf(tick.getBid()), BigDecimal.valueOf(tick.getAsk())), Instant.ofEpochMilli(tick.getTime()));

    }

    @Override
    public void onBar(Instrument instrument, Period period, IBar askBar, IBar bidBar) throws JFException {

        if (period.getUnit() == Unit.Minute && period.getNumOfUnits() == 1) {
            DataSourceCandle dataSourceCandle = getDataSourceCandle(instrument, askBar);
            strategy.onNextCandle(dataSourceCandle, false);
        }


    }

    @NotNull
    private DataSourceCandle getDataSourceCandle(Instrument instrument, IBar askBar) {
        return new DataSourceCandle() {
            @Override
            public String getSymbol() {
                return instrument.getName().substring(0, 3) + instrument.getName().substring(4, 7);
            }

            @Override
            public Instant getOpenTime() {
                return Instant.ofEpochMilli(askBar.getTime());
            }

            @Override
            public BigDecimal getOpen() {
                return BigDecimal.valueOf(askBar.getOpen());

            }

            @Override
            public BigDecimal getHigh() {
                return BigDecimal.valueOf(askBar.getHigh());
            }

            @Override
            public BigDecimal getLow() {
                return BigDecimal.valueOf(askBar.getLow());
            }

            @Override
            public BigDecimal getClose() {
                return BigDecimal.valueOf(askBar.getClose());
            }

            @Override
            public BigDecimal getVolume() {
                return BigDecimal.valueOf(askBar.getVolume());
            }

            @Override
            public Duration getDuration() {
                return Duration.ofMinutes(1);
            }
        };
    }

    @Override
    public void onMessage(IMessage message) throws JFException {

    }


    private long tickCounter = 0;

    double maxMargin = 0;

    @Override
    public void onAccount(IAccount account) throws JFException {
        if (tickCounter++ % 300 == 0) {
            maxMargin = Math.max(account.getUsedMargin(), maxMargin);
            System.out.println(account.getEquity() + " used margin  " + account.getUsedMargin() + " max margin " + maxMargin);

        }
//        System.out.println(account.getBalance());
    }

    @Override
    public void onStop() throws JFException {
        strategy.finish();
    }
}
