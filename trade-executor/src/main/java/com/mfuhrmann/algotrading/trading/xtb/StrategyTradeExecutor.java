package com.mfuhrmann.algotrading.trading.xtb;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.model.xtb.XtbCandle;
import com.mfuhrmann.algotrading.model.xtb.XtbCandleDao;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrument;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrumentService;
import com.mfuhrmann.algotrading.trading.execution.XtbTradeExecutor;
import com.mfuhrmann.trading.backtesting.trade.ForexMarketSymbol;
import com.mfuhrmann.trading.backtesting.trade.Trade;
import com.mfuhrmann.trading.backtesting.trade.TradeType;
import com.mfuhrmann.trading.backtesting.trade.portfolio.SimpleForexPortfolio;
import com.mfuhrmann.trading.backtesting.trade.portfolio.TradesExecutor;
import com.mfuhrmann.trading.backtesting.trade.risk.CurrencyExposureRiskCalculator;
import com.mfuhrmann.trading.backtesting.trade.strategy.BidAskPrice;
import com.mfuhrmann.trading.backtesting.trade.strategy.sma.SmaStrategy;
import com.mfuhrmann.trading.backtesting.trade.strategy.sma.SmaStrategyParams;
import com.mfuhrmann.trading.backtesting.trade.utils.Money;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pro.xstore.api.message.command.APICommandFactory;
import pro.xstore.api.message.error.APICommandConstructionException;
import pro.xstore.api.message.error.APICommunicationException;
import pro.xstore.api.message.error.APIReplyParseException;
import pro.xstore.api.message.records.SCandleRecord;
import pro.xstore.api.message.records.STickRecord;
import pro.xstore.api.message.response.APIErrorResponse;
import pro.xstore.api.message.response.TradesResponse;
import pro.xstore.api.streaming.StreamingListener;
import pro.xstore.api.sync.ServerData;
import pro.xstore.api.sync.SyncAPIConnector;
import scala.collection.JavaConverters;
import scala.math.BigDecimal;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

//@Profile("execute-test")
@Service
public class StrategyTradeExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(StrategyTradeExecutor.class);

    private final XtbClientConnector connector;
    private final XtbInstrumentService xtbInstrumentService;
    private final XtbCandleDao xtbCandleDao;
    private final TradesExecutor xtbTradeExecutor;

    public StrategyTradeExecutor(XtbClientConnector connector, XtbInstrumentService xtbInstrumentService, XtbCandleDao xtbCandleDao, XtbTradeExecutor xtbTradeExecutor) {
        this.connector = connector;
        this.xtbInstrumentService = xtbInstrumentService;
        this.xtbCandleDao = xtbCandleDao;
        this.xtbTradeExecutor = xtbTradeExecutor;
    }

    @PostConstruct
    public void startStrategy() throws IOException, APICommunicationException, APICommandConstructionException, APIErrorResponse, APIReplyParseException {


        SyncAPIConnector apiConnector = connector.createConnector(ServerData.ServerEnum.DEMO);

//        List<String> symbols = instrumentService.getSelectedInstruments().stream()
//                .map(XtbInstrument::getMarketSymbol)
//                .collect(Collectors.toList());
        Set<String> currencies = Set.of("EUR", "USD", "JPY", "GBP", "CHF", "CAD", "AUD", "PLN");
        List<String> symbols = xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> currencies.contains(xtbInstrument.getSymbol().substring(0, 3)) && currencies.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .map(XtbInstrument::getSymbol)
                .collect(Collectors.toList());

        List<Long> pipsChanges = new ArrayList<>();

        SimpleForexPortfolio portfolio = createPortfolio();
        synchronizePortfolio(apiConnector, portfolio);

        List<XtbCandle> candles = xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> currencies.contains(xtbInstrument.getSymbol().substring(0, 3)) && currencies.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .flatMap(fxInstrument -> xtbCandleDao.findAllBySymbolAndDurationAndOpenTimeAfterOrderByOpenTimeAsc(fxInstrument.getSymbol(), Duration.ofMinutes(1), LocalDateTime.now().minusDays(3).toInstant(ZoneOffset.UTC)))
                .collect(Collectors.toList());

        SmaStrategy strategy = new SmaStrategy(portfolio, new CurrencyExposureRiskCalculator(portfolio), new SmaStrategyParams(0.4, 1600, 0.001, 1600, 6, 22));

//        candles.forEach(xtbCandle -> strategy.onNextCandle(xtbCandle, true));
        initStreaming(apiConnector, symbols, pipsChanges, portfolio, strategy);


    }

    private void synchronizePortfolio(SyncAPIConnector apiConnector, SimpleForexPortfolio portfolio) throws APICommandConstructionException, APIReplyParseException, APICommunicationException, APIErrorResponse {
        TradesResponse tradesResponse = APICommandFactory.executeTradesCommand(apiConnector, true);
        List<Trade> collect = tradesResponse.getTradeRecords().stream()
                .map(tradeRecord ->
                        new Trade(new ForexMarketSymbol(
                                tradeRecord.getSymbol()),
                                BigDecimal.valueOf(tradeRecord.getOpen_price()),
                                Instant.ofEpochMilli(tradeRecord.getTimestamp()),
                                BigDecimal.valueOf(tradeRecord.getPosition()),
                                TradeType.parseFromCmd(tradeRecord.getCmd()),
                                tradeRecord.getOrder()))
                .collect(Collectors.toList());
        portfolio.synchronizePortfolio(JavaConverters.asScalaIterator(collect.iterator()).toList());
    }

    @NotNull
    private SimpleForexPortfolio createPortfolio() {

        return new SimpleForexPortfolio(new Money(BigDecimal.valueOf(10_000), Currency.getInstance("PLN")), 30, xtbTradeExecutor);
    }

    private void initStreaming(SyncAPIConnector apiConnector, List<String> symbols, List<Long> pipsChanges, SimpleForexPortfolio portfolio, SmaStrategy strategy) throws IOException, APICommunicationException {
        StreamingListener sl = new StreamingListener() {

            private Instant lastTimestamp = Instant.now();

            @Override
            public void receiveTickRecord(STickRecord tickRecord) {
                ForexMarketSymbol symbol = new ForexMarketSymbol(tickRecord.getSymbol());
                strategy.onNextTick(
                        symbol,
                        new BidAskPrice(symbol, BigDecimal.valueOf(tickRecord.getBid()), BigDecimal.valueOf(tickRecord.getAsk())),
                        Instant.ofEpochMilli(tickRecord.getTimestamp()));
            }

            @Override
            public void receiveCandleRecord(SCandleRecord candleRecord) {

//                strategy.onNextCandleClose(new ForexMarketSymbol(candleRecord.getMarketSymbol()), BigDecimal.valueOf(candleRecord.getClose()), Instant.ofEpochMilli(candleRecord.getCtm()),);
//                pipsChanges.add(portfolio.pipsProfit());
//                if (!Duration.between(lastTimestamp, Instant.now()).minusMinutes(1).isNegative()) {
//                    lastTimestamp = Instant.now();
//                    LOGGER.info("Candle for time " + lastTimestamp);
//
//                }

            }
        };


        apiConnector.connectStream(sl);

        symbols.forEach(symbol -> {
            try {
                apiConnector.subscribePrice(symbol, 1, 0);
            } catch (APICommunicationException e) {
                e.printStackTrace();
            }
        });

        apiConnector.subscribeCandles(symbols);
    }

//new TradesExecutor() {
//
//
//        @Override
//        public Option<Object> buy(MarketSymbol symbol, BigDecimal amount, BigDecimal price, Instant tickTime) {
//
//            xtbTradeExecutor.sell(symbol, amount.bigDecimal());
//            return xtbTradeExecutor.buyOpen(symbol, amount.bigDecimal());
//        }
//
//
//        @Override
//        public Option<Object> sell(MarketSymbol symbol, BigDecimal amount, BigDecimal price, Instant tickTime) {
//
//            LOGGER.info("SELL on " + symbol + " :  " + amount + " : " + price + " : " + tickTime);
//            xtbTradeExecutor.buyClose(symbol, amount.bigDecimal());
//
//            return Option.apply(xtbTradeExecutor.sellOpen(symbol, amount.bigDecimal()));
//        }
//
//        //TODO
//        @Override
//        public Option<Object> close(ClosedTrade closedTrade) {
//
//            if (closedTrade.tradeType() == TradeType.BUY()) {
//                xtbTradeExecutor.buyClose(closedTrade.symbol(), closedTrade.positionSize().bigDecimal(), closedTrade.tradeId());
//            } else {
//                xtbTradeExecutor.sellClose(closedTrade.symbol(), closedTrade.positionSize().bigDecimal(), closedTrade.tradeId());
//            }
//            return Option.<Object>apply(closedTrade.pipsInProfit());
//
//        }

}
