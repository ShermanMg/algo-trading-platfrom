package com.mfuhrmann.algotrading.trading.account;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.trading.execution.XtbTradeExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pro.xstore.api.message.command.APICommandFactory;
import pro.xstore.api.message.records.SBalanceRecord;
import pro.xstore.api.message.response.MarginLevelResponse;
import pro.xstore.api.streaming.StreamingListener;
import pro.xstore.api.sync.ServerData;
import pro.xstore.api.sync.SyncAPIConnector;

import javax.annotation.PostConstruct;

@Service
public class BalanceChecker {

    private static final Logger LOGGER = LoggerFactory.getLogger(BalanceChecker.class);


    private final SyncAPIConnector connector;

    private SBalanceRecord balanceRecord;
    private MarginLevelResponse marginLevelResponse;
    private final XtbTradeExecutor tradeExecutor;

    @Autowired
    public BalanceChecker(XtbClientConnector xtbClientConnector, XtbTradeExecutor tradeExecutor) {
        this.connector = xtbClientConnector.createConnector(ServerData.ServerEnum.DEMO);
        this.tradeExecutor = tradeExecutor;
    }


    @Scheduled(fixedRate = 1000 * 60)
    public void printBalance() {


        if (balanceRecord != null) {
            LOGGER.info(balanceRecord.toString());
        } else {
            LOGGER.info(marginLevelResponse.toString());
        }

    }

    @PostConstruct
    private void init() throws Exception {
        marginLevelResponse = APICommandFactory.executeMarginLevelCommand(connector);

        StreamingListener sl = new StreamingListener() {
            @Override
            public void receiveBalanceRecord(SBalanceRecord balanceRecord) {
                BalanceChecker.this.balanceRecord = balanceRecord;
            }
        };
        connector.connectStream(sl);
        connector.subscribeBalance();


//        boolean b = tradeExecutor.buyOpen(new ForexMarketSymbol("EUR", "USD"), BigDecimal.valueOf(0.1));


//        System.out.println(b);
    }

    public SBalanceRecord getBalanceRecord() {
        return balanceRecord;
    }
}
