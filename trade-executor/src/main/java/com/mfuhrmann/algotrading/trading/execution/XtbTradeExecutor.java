package com.mfuhrmann.algotrading.trading.execution;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.trading.backtesting.trade.ClosedTrade;
import com.mfuhrmann.trading.backtesting.trade.MarketSymbol;
import com.mfuhrmann.trading.backtesting.trade.TradeType;
import com.mfuhrmann.trading.backtesting.trade.portfolio.TradesExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pro.xstore.api.message.codes.TRADE_OPERATION_CODE;
import pro.xstore.api.message.codes.TRADE_TRANSACTION_TYPE;
import pro.xstore.api.message.command.APICommandFactory;
import pro.xstore.api.message.command.TradeTransactionCommand;
import pro.xstore.api.message.error.APICommandConstructionException;
import pro.xstore.api.message.error.APICommunicationException;
import pro.xstore.api.message.error.APIReplyParseException;
import pro.xstore.api.message.records.TradeRecord;
import pro.xstore.api.message.response.APIErrorResponse;
import pro.xstore.api.message.response.TradeTransactionResponse;
import pro.xstore.api.message.response.TradeTransactionStatusResponse;
import pro.xstore.api.message.response.TradesResponse;
import pro.xstore.api.sync.ServerData;
import pro.xstore.api.sync.SyncAPIConnector;
import scala.Option;

import java.math.BigDecimal;
import java.util.concurrent.atomic.AtomicLong;


@Service
public class XtbTradeExecutor implements TradesExecutor {

    private static final Logger LOGGER = LoggerFactory.getLogger(XtbTradeExecutor.class);


    private final AtomicLong atomicLong = new AtomicLong();

    private final SyncAPIConnector connector;

    @Autowired
    public XtbTradeExecutor(XtbClientConnector xtbClientConnector) {
        this.connector = xtbClientConnector.createConnector(ServerData.ServerEnum.DEMO);
    }

    @Override
    public Option<Object> buy(MarketSymbol symbol, scala.math.BigDecimal amount) {

        return Option.<Object>apply(openTransaction(symbol, amount.bigDecimal(), TRADE_OPERATION_CODE.BUY));

    }

    @Override
    public Option<Object> sell(MarketSymbol symbol, scala.math.BigDecimal amount) {
        return Option.<Object>apply(openTransaction(symbol, amount.bigDecimal(), TRADE_OPERATION_CODE.SELL));

    }

    @Override
    public Option<Object> close(ClosedTrade closedTrade) {

        if (closedTrade.tradeType() == TradeType.BUY()) {
            closeTransaction(closedTrade.symbol(), closedTrade.positionSize().bigDecimal(), TRADE_OPERATION_CODE.BUY, TRADE_TRANSACTION_TYPE.CLOSE, closedTrade.tradeId());
            return Option.<Object>apply(closedTrade.pipsInProfit());

        } else {
            closeTransaction(closedTrade.symbol(), closedTrade.positionSize().bigDecimal(), TRADE_OPERATION_CODE.SELL, TRADE_TRANSACTION_TYPE.CLOSE, closedTrade.tradeId());
            return Option.<Object>apply(closedTrade.pipsInProfit());
        }
    }

    private Long closeTransaction(MarketSymbol symbol, BigDecimal amount, TRADE_OPERATION_CODE operation, TRADE_TRANSACTION_TYPE transactionType, Long order) {

        LOGGER.info(operation + " on " + symbol + " :  " + amount + " : Market order");


        try {
            TradesResponse tradesResponse = getTradesResponseSafely();
            TradeRecord openTrade = tradesResponse.getTradeRecords().stream()
                    .filter(tradeRecord -> tradeRecord.getOrder2() == order)
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException("No order found " + order));

            TradeTransactionResponse tradeTransactionResponse = createTrade(operation, transactionType, symbol, amount, openTrade.getOrder());
            TradeTransactionStatusResponse tradeTransactionStatusResponse = APICommandFactory.executeTradeTransactionStatusCommand(connector, tradeTransactionResponse.getOrder());

            if (tradeTransactionStatusResponse.getAsk() == 0.0 && tradeTransactionStatusResponse.getBid() == 0.0) {
                LOGGER.info("Trade not executed. Cause : " + tradeTransactionStatusResponse.getMessage());
                return null;
            }
            LOGGER.info(tradeTransactionStatusResponse.toString());

            return tradeTransactionResponse.getOrder();
        } catch (Exception e) {
            LOGGER.error("Exception during operation  ", e);
            return null;
        }
    }

    private TradesResponse getTradesResponseSafely() throws APICommandConstructionException, APIReplyParseException, APICommunicationException, APIErrorResponse {

        try {

            return APICommandFactory.executeTradesCommand(connector, true);
        } catch (Exception e) {
            return getTradesResponseSafely();
        }
    }


    private Long openTransaction(MarketSymbol symbol, BigDecimal amount, TRADE_OPERATION_CODE operation) {
        LOGGER.info(operation + " on " + symbol + " :  " + amount + " : Market order");


        try {
            TradeTransactionResponse tradeTransactionResponse = createTrade(operation, TRADE_TRANSACTION_TYPE.OPEN, symbol, amount, null);
            TradeTransactionStatusResponse tradeTransactionStatusResponse = APICommandFactory.executeTradeTransactionStatusCommand(connector, tradeTransactionResponse.getOrder());

            if (tradeTransactionStatusResponse.getAsk() == 0.0 && tradeTransactionStatusResponse.getBid() == 0.0) {
                LOGGER.info("Trade not executed. Cause : " + tradeTransactionStatusResponse.getMessage());
                return null;
            }
            LOGGER.info(tradeTransactionStatusResponse.toString());

            return tradeTransactionResponse.getOrder();
        } catch (Exception e) {
            LOGGER.error("Exception during operation  ", e);
            return null;
        }
    }

    private TradeTransactionResponse createTrade(TRADE_OPERATION_CODE operation, TRADE_TRANSACTION_TYPE
            transaction, MarketSymbol symbol, BigDecimal volume, Long order) throws Exception {

        TradeTransactionCommand tradeTransactionCommand = APICommandFactory.createTradeTransactionCommand(operation,
                transaction,
                1.0,
                null,
                null,
                symbol.getName(),
                volume.doubleValue(),
                order,
                null,
//                "operation- " + atomicLong.incrementAndGet(),
                null);

        return APICommandFactory.executeTradeTransactionCommand(connector, tradeTransactionCommand);
    }


}
