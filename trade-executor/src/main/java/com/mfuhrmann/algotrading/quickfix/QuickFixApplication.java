package com.mfuhrmann.algotrading.quickfix;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import quickfix.*;
import quickfix.field.*;
import quickfix.fix44.QuoteRequest;

public class QuickFixApplication extends MessageCracker implements Application {

    private static final Logger LOGGER = LoggerFactory.getLogger(QuickFixApplication.class);

    @Override
    public void onCreate(SessionID sessionID) {
        LOGGER.info("create " + sessionID);

    }

    @Override
    public void onLogon(SessionID sessionID) {

        LOGGER.info("logon " + sessionID);
        try {
            QuoteRequest message = new QuoteRequest(new QuoteReqID("7"));
            message.set(new RFQReqID("7"));
            message.set(new ClOrdID("7"));


            Session.sendToTarget(message, sessionID);
        } catch (SessionNotFound sessionNotFound) {
            sessionNotFound.printStackTrace();
        }
    }

    @Override
    public void onLogout(SessionID sessionID) {

    }

    @Override
    public void toAdmin(Message message, SessionID sessionID) {
        try {

            if (MsgType.LOGON.compareTo(message.getHeader().getString(MsgType.FIELD)) == 0) {
                message.setField(Username.FIELD, new StringField(Username.FIELD, "3512791"));
                message.setField(Password.FIELD, new StringField(Password.FIELD, "Test8989"));
            }
            LOGGER.info("toAdmin " + message);


        } catch (FieldNotFound fieldNotFound) {
            fieldNotFound.printStackTrace();
        }


    }

    @Override
    public void fromAdmin(Message message, SessionID sessionID) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon {

        LOGGER.info("fromAdmin " + message);

        try {
            crack(message, sessionID);


        } catch (UnsupportedMessageType unsupportedMessageType) {
            unsupportedMessageType.printStackTrace();
        }

    }

    @Override
    protected void onMessage(Message message, SessionID sessionID) throws FieldNotFound, UnsupportedMessageType, IncorrectTagValue {
        LOGGER.info("on message " + message);
    }

    @Override
    public void toApp(Message message, SessionID sessionID) throws DoNotSend {
        LOGGER.info("toApp " + message);

    }

    @Override
    public void fromApp(Message message, SessionID sessionID) throws FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType {
        LOGGER.info("fromApp " + message);
    }
}
