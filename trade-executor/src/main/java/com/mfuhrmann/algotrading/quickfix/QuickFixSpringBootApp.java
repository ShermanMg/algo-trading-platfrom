package com.mfuhrmann.algotrading.quickfix;

import com.mfuhrmann.algotrading.commons.CommonsConfig;
import com.mfuhrmann.algotrading.config.DbConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;


@Import({CommonsConfig.class, DbConfig.class, QuickFixConfig.class})
@EnableScheduling
@SpringBootApplication(exclude = {MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@ComponentScan(basePackages = {"com.mfuhrmann.algotrading.quickfix"})
@EnableMongoRepositories(basePackages = "com.mfuhrmann.algotrading.quickfix")
public class QuickFixSpringBootApp {

    public static void main(String[] args) {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(QuickFixSpringBootApp.class);
        builder.headless(false);
        builder.run(args);


    }
}