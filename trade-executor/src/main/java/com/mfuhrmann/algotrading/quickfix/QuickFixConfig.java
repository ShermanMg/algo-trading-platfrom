package com.mfuhrmann.algotrading.quickfix;

import com.google.common.io.Resources;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import quickfix.*;

import java.io.IOException;

@Configuration
public class QuickFixConfig {


    @Bean
    public Application quickFixApplication() throws ConfigError, InterruptedException, IOException {
        Application application = new QuickFixApplication();

        SessionSettings settings = new SessionSettings(Resources.getResource("quickfix.settings").openStream());
        MessageStoreFactory storeFactory = new FileStoreFactory(settings);
        LogFactory logFactory = new FileLogFactory(settings);

        MessageFactory messageFactory = new DefaultMessageFactory();
        Initiator initiator = new SocketInitiator(application, storeFactory, settings, logFactory, messageFactory);
        initiator.start();
//        Acceptor acceptor = new SocketAcceptor
//                (application, storeFactory, settings, logFactory, messageFactory);
//        acceptor.start();

        // while(condition == true) { do something; }

        while (true) {
            Thread.sleep(10_000);
            if (false) {
                break;
            }
        }
//        acceptor.stop();

        initiator.stop();
        return application;
    }

}
