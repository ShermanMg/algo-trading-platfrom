package com.mfuhrmann.algotrading.strategymanager;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import org.ta4j.core.BaseTimeSeries;
import org.ta4j.core.indicators.helpers.ClosePriceIndicator;
import org.ta4j.core.trading.rules.OverIndicatorRule;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DataCollectorApplicationTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void contextLoads() {
    }

    @Test
    public void testSaving() {

        mongoTemplate.save(new OverIndicatorRule(new ClosePriceIndicator(new BaseTimeSeries()), new ClosePriceIndicator(new BaseTimeSeries())), "test");

    }


}
