package com.mfuhrmann.algotrading.strategymanager.connectors.xtb;

public class ScandleRecordBuilder {
    private long ctm;
    private String ctmString = "";
    private double open;
    private double high;
    private double low;
    private double close;
    private double vol;
    private int quoteId;

    public ScandleRecordBuilder setCtm(long ctm) {
        this.ctm = ctm;
        return this;
    }

    public ScandleRecordBuilder setCtmString(String ctmString) {
        this.ctmString = ctmString;
        return this;
    }

    public ScandleRecordBuilder setOpen(double open) {
        this.open = open;
        return this;
    }

    public ScandleRecordBuilder setHigh(double high) {
        this.high = high;
        return this;
    }

    public ScandleRecordBuilder setLow(double low) {
        this.low = low;
        return this;
    }

    public ScandleRecordBuilder setClose(double close) {
        this.close = close;
        return this;
    }

    public ScandleRecordBuilder setVol(double vol) {
        this.vol = vol;
        return this;
    }

    public ScandleRecordBuilder setQuoteId(int quoteId) {
        this.quoteId = quoteId;
        return this;
    }

    public CandleStreamHandlerTest.ScandleRecordData createScandleRecordBuilder() {
        return new CandleStreamHandlerTest.ScandleRecordData(ctm, ctmString, open, high, low, close, vol, quoteId);
    }
}