package com.mfuhrmann.algotrading.strategymanager.connectors.xtb;


import com.mfuhrmann.algotrading.model.xtb.InstrumentType;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;

public class XtbCandleDataBuilder {
    private String symbol;
    private InstrumentType instrumentType;
    private Duration duration;
    private Instant openTime;
    private BigDecimal open;
    private BigDecimal high;
    private BigDecimal low;
    private BigDecimal close;
    private BigDecimal volume;

    public XtbCandleDataBuilder setSymbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public XtbCandleDataBuilder setInstrumentType(InstrumentType instrumentType) {
        this.instrumentType = instrumentType;
        return this;
    }

    public XtbCandleDataBuilder setDuration(Duration duration) {
        this.duration = duration;
        return this;
    }

    public XtbCandleDataBuilder setOpenTime(Instant openTime) {
        this.openTime = openTime;
        return this;
    }

    public XtbCandleDataBuilder setOpen(BigDecimal open) {
        this.open = open;
        return this;
    }

    public XtbCandleDataBuilder setHigh(BigDecimal high) {
        this.high = high;
        return this;
    }

    public XtbCandleDataBuilder setLow(BigDecimal low) {
        this.low = low;
        return this;
    }

    public XtbCandleDataBuilder setClose(BigDecimal close) {
        this.close = close;
        return this;
    }

    public XtbCandleDataBuilder setVolume(BigDecimal volume) {
        this.volume = volume;
        return this;
    }

    public CandleStreamHandlerTest.XtbCandleData build() {
        return new CandleStreamHandlerTest.XtbCandleData(symbol, instrumentType, duration, openTime, open, high, low, close, volume);
    }
}