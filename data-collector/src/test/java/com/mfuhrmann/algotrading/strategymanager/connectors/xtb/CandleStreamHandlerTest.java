package com.mfuhrmann.algotrading.strategymanager.connectors.xtb;

import com.mfuhrmann.algotrading.connectors.xtb.CandleStreamHandler;
import com.mfuhrmann.algotrading.model.xtb.InstrumentType;
import com.mfuhrmann.algotrading.model.xtb.XtbCandle;
import com.mfuhrmann.algotrading.model.xtb.XtbCandleDao;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import pro.xstore.api.message.records.SCandleRecord;

import java.math.BigDecimal;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

@RunWith(MockitoJUnitRunner.class)
public class CandleStreamHandlerTest {

    @InjectMocks
    private CandleStreamHandler candleStreamHandler;

    @Mock
    private XtbCandleDao xtbCandleDao;


    @Test
    public void shouldCreate5MinCandle() {

        //Given
        Instant incomingCandleTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 4)).atZone(ZoneId.systemDefault()).toInstant();
        Map<String, Object> map = new ScandleRecordBuilder().setCtm(incomingCandleTime.toEpochMilli())
                .createScandleRecordBuilder()
                .getMap();
        SCandleRecord sCandleRecord = new SCandleRecord();
        sCandleRecord.setFieldsFromJSONObject(new JSONObject(map));

        XtbCandle first = new XtbCandleDataBuilder().setOpenTime(incomingCandleTime).build().createCandle();

        Mockito.when(xtbCandleDao.findAllBySymbolAndOpenTimeGreaterThanEqualAndOpenTimeLessThanAndDuration(sCandleRecord.getSymbol(),
                incomingCandleTime.minus(4, ChronoUnit.MINUTES),
                incomingCandleTime.plus(1, ChronoUnit.MINUTES),
                Duration.ofMinutes(5)))
                .thenReturn(List.of(first));

        //When
        candleStreamHandler.onCandle(sCandleRecord);

        //Then

    }

    static class XtbCandleData {
        private String symbol;
        private InstrumentType instrumentType;
        private Duration duration;
        private Instant openTime;
        private BigDecimal open;
        private BigDecimal high;
        private BigDecimal low;
        private BigDecimal close;
        private BigDecimal volume;

        public XtbCandleData(String symbol, InstrumentType instrumentType, Duration duration, Instant openTime,
                             BigDecimal open, BigDecimal high, BigDecimal low, BigDecimal close, BigDecimal volume) {
            this.symbol = symbol;
            this.instrumentType = instrumentType;
            this.duration = duration;
            this.openTime = openTime;
            this.open = open;
            this.high = high;
            this.low = low;
            this.close = close;
            this.volume = volume;
        }

        public XtbCandle createCandle() {
            return new XtbCandle(symbol, instrumentType, duration, openTime, open, high, low, close, volume);
        }

    }


    static class ScandleRecordData {
        private long ctm;
        private String ctmString;
        private double open;
        private double high;
        private double low;
        private double close;
        private double vol;
        private long quoteId;

        public ScandleRecordData(long ctm, String ctmString, double open, double high, double low, double close, double vol, int quoteId) {
            this.ctm = ctm;
            this.ctmString = ctmString;
            this.open = open;
            this.high = high;
            this.low = low;
            this.close = close;
            this.vol = vol;
            this.quoteId = quoteId;
        }

        public Map<String, Object> getMap() {
            return Map.of(
                    "ctm", ctm,
                    "ctmString", ctmString,
                    "open", open,
                    "high", high,
                    "low", low,
                    "close", close,
                    "vol", vol,
                    "quoteId", quoteId);
        }

    }
}