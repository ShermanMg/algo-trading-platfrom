package com.mfuhrmann.algotrading;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class DataCollectorApplication {

    public static void main(String[] args) {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(DataCollectorApplication.class);
        builder.headless(false);
        builder.run(args);


    }
}