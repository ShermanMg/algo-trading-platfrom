package com.mfuhrmann.algotrading;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Main {


    public static void main(String[] args) throws IOException {


        URL url = new URL("https://www.db.com/belgium/docs/22032019_1pm_Fixing_Belgie.pdf");


        String text = getText(url.openStream());
        System.out.println(text);
        String[] split = text.split("/r");

        System.out.println(split.length);


    }

    static String getText(InputStream pdfFile) throws IOException {
        PDDocument doc = PDDocument.load(pdfFile);



        return new PDFTextStripper().getText(doc);
    }
}
