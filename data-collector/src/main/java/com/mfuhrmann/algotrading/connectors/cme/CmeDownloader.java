package com.mfuhrmann.algotrading.connectors.cme;

import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CmeDownloader {

    private static DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyyMMdd");

    private final CmeFixingRepository repository;

    public CmeDownloader(CmeFixingRepository repository) {
        this.repository = repository;
    }

    @PostConstruct
    public void init() throws IOException {

        FTPClient ftpClient = loginToFtpClient();

        FTPFile[] ftpFiles = ftpClient.listFiles();


        Set<String> existingFixes = repository.findAll().stream()
                .map(CmeFixing::getFileName)
                .collect(Collectors.toSet());

        Arrays.stream(ftpFiles)
                .filter(ftpFile -> !existingFixes.contains(ftpFile.getName()))
                .map(ftpFile -> convertToCmeFixing(ftpClient, ftpFile))
                .peek(cmeFixing -> System.out.println("Downloaded file " + cmeFixing))
                .forEach(repository::save);

        ftpClient.disconnect();

        System.out.println("Finished");


    }

    private FTPClient loginToFtpClient() throws IOException {
        FTPClient ftpClient = new FTPClient();

//        ftpClient.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));

        ftpClient.connect("ftp.cmegroup.com");
        ftpClient.enterLocalPassiveMode();

        ftpClient.login("anonymous", "");
        ftpClient.changeWorkingDirectory("/otcfx/");
        return ftpClient;
    }

    private CmeFixing convertToCmeFixing(FTPClient ftpClient, FTPFile ftpFile) {

        try {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(ftpClient.retrieveFileStream(ftpFile.getName()));
            CSVReader csvReader = new CSVReader(new InputStreamReader(bufferedInputStream));
            String[] header = csvReader.readNext();

            List<CmeFixing.FixingRow> fixingRows = csvReader.readAll().stream()
                    .map(entries -> new CmeFixing.FixingRow(CmeFixing.TENOR.parse(entries[1]), Double.parseDouble(entries[2])))
                    .collect(Collectors.toList());


            ftpClient.completePendingCommand();
            bufferedInputStream.close();
            return new CmeFixing(ftpFile.getName(), ftpFile.getName().split("_")[3], LocalDate.parse(ftpFile.getName().split("_")[4].split("\\.")[0], dateTimeFormatter), fixingRows);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

    }
}



