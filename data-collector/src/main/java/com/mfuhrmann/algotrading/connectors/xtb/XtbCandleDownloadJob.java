package com.mfuhrmann.algotrading.connectors.xtb;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.model.xtb.XtbCandleDao;
import com.mfuhrmann.algotrading.model.xtb.XtbCandleSnapshotDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class XtbCandleDownloadJob {

    private final XtbClientConnector xtbClientConnector;
    private final XtbCandleDao xtbCandleDao;
    private final XtbCandleSnapshotDao xtbCandleSnapshotDao;


    @Autowired
    public XtbCandleDownloadJob(XtbClientConnector xtbClientConnector, XtbCandleDao xtbCandleDao, XtbCandleSnapshotDao xtbCandleSnapshotDao) {
        this.xtbClientConnector = xtbClientConnector;
        this.xtbCandleDao = xtbCandleDao;
        this.xtbCandleSnapshotDao = xtbCandleSnapshotDao;
//        xtbClientConnector.initializeStreamingListener();

    }


}
