package com.mfuhrmann.algotrading.connectors.xtb;

import pro.xstore.api.message.command.APICommandFactory;
import pro.xstore.api.message.error.APICommandConstructionException;
import pro.xstore.api.message.error.APICommunicationException;
import pro.xstore.api.message.error.APIReplyParseException;
import pro.xstore.api.message.records.STickRecord;
import pro.xstore.api.message.records.STradeRecord;
import pro.xstore.api.message.records.SymbolRecord;
import pro.xstore.api.message.records.TickRecord;
import pro.xstore.api.message.response.APIErrorResponse;
import pro.xstore.api.message.response.AllSymbolsResponse;
import pro.xstore.api.message.response.LoginResponse;
import pro.xstore.api.message.response.TickPricesResponse;
import pro.xstore.api.streaming.StreamingListener;
import pro.xstore.api.sync.*;
import pro.xstore.api.sync.ServerData.ServerEnum;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Map;
import java.util.stream.Collectors;

public class ExampleDemo {

    public void runExample(ServerEnum server, Credentials credentials) throws Exception {
        try {
            SyncAPIConnector connector = new SyncAPIConnector(server);
            LoginResponse loginResponse = APICommandFactory.executeLoginCommand(connector, credentials);

            AllSymbolsResponse allSymbolsResponse = APICommandFactory.executeAllSymbolsCommand(connector);

//
//            ChartResponse eurusd = APICommandFactory.executeChartLastCommand(
//                    connector, "EURUSD", PERIOD_CODE.PERIOD_M1,
//                    LocalDateTime.now().minusMonths(2).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli());
//
//            eurusd.getRateInfos().forEach(record -> System.out.println(record));

//			FX
//					STC
//			CRT
//					ETF
//			CMD
//					IND

//            other(connector, loginResponse, allSymbolsResponse);

            allSymbolsResponse.getSymbolRecords().stream()
                    .filter(symbolRecord -> symbolRecord.getCategoryName().equals("CRT"))
                    .collect(Collectors.groupingBy(SymbolRecord::getCategoryName))
                    .forEach((s, symbolRecords) -> symbolRecords.forEach(symbolRecord -> System.out.println(s + " " + symbolRecord)));

        } catch (Exception ex) {
            System.err.println(ex);
        }

    }

    private void other(SyncAPIConnector connector, LoginResponse loginResponse, AllSymbolsResponse allSymbolsResponse) throws APICommandConstructionException, APICommunicationException, APIReplyParseException, APIErrorResponse, IOException, InterruptedException {
        allSymbolsResponse.getSymbolRecords().stream()
                .filter(symbolRecord -> symbolRecord.getCategoryName().equals("FX"))
                .collect(Collectors.groupingBy(SymbolRecord::getCategoryName))
                .forEach((s, symbolRecords) -> symbolRecords.forEach(System.out::println));


        System.out.println(loginResponse);
        if (loginResponse != null && loginResponse.getStatus()) {
            StreamingListener sl = new StreamingListener() {
                @Override
                public void receiveTradeRecord(STradeRecord tradeRecord) {
                    System.out.println("Stream trade record: " + tradeRecord);
                }

                @Override
                public void receiveTickRecord(STickRecord tickRecord) {
                    System.out.println("Stream tick record: " + tickRecord);
                }
            };

            LinkedList<String> list = new LinkedList<String>();
            String symbol = "BTCUSD";
            list.add(symbol);

            TickPricesResponse resp = APICommandFactory.executeTickPricesCommand(connector, 0L, list, 0L);
            for (TickRecord tr : resp.getTicks()) {
                System.out.println("TickPrices result: " + tr.getSymbol() + " - ask: " + tr.getAsk());
            }

            connector.connectStream(sl);
            System.out.println("Stream connected.");

            connector.subscribePrice(symbol);
            connector.subscribeTrades();

            Thread.sleep(10000);

            connector.unsubscribePrice(symbol);
            connector.unsubscribeTrades();

            connector.disconnectStream();
            System.out.println("Stream disconnected.");

            Thread.sleep(5000);

            connector.connectStream(sl);
            System.out.println("Stream connected again.");
            connector.disconnectStream();
            System.out.println("Stream disconnected again.");
            System.exit(0);
        }
    }

    protected Map<String, Server> getAvailableServers() {
        return ServerData.getProductionServers();
    }
}