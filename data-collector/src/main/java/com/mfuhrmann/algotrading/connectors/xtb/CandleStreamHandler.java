package com.mfuhrmann.algotrading.connectors.xtb;

import com.google.common.base.Stopwatch;
import com.mfuhrmann.algotrading.model.xtb.*;
import net.intelie.omnicron.Cron;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pro.xstore.api.message.codes.PERIOD_CODE;
import pro.xstore.api.message.records.SCandleRecord;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;

@Component
public class CandleStreamHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CandleStreamHandler.class);

    private final XtbCandleDao xtbCandleDao;
    private final XtbInstrumentService xtbInstrumentService;

    private final List<CandleHandler> candleHandlers = List.of(
            new CandleHandler("*/5 * * * *", PERIOD_CODE.PERIOD_M5),
            new CandleHandler("*/30 * * * *", PERIOD_CODE.PERIOD_M30),
            new CandleHandler("0 * * * *", PERIOD_CODE.PERIOD_H1),
            new CandleHandler("0 */4 * * *", PERIOD_CODE.PERIOD_H4),
            new CandleHandler("0 0 * * *", PERIOD_CODE.PERIOD_D1),
            new CandleHandler("0 0 * * 0", PERIOD_CODE.PERIOD_W1)
    );


    @Autowired
    public CandleStreamHandler(XtbCandleDao xtbCandleDao, XtbInstrumentService xtbInstrumentService) {
        this.xtbCandleDao = xtbCandleDao;
        this.xtbInstrumentService = xtbInstrumentService;
    }


    public void onCandle(SCandleRecord candleRecord) {
        Stopwatch stopwatch = Stopwatch.createStarted();
        saveOneMinuteCandle(candleRecord);

        candleHandlers.forEach(candleHandler -> candleHandler.handleCandle(candleRecord));
//        LOGGER.info("One candle {} handling took  {} ms", candleRecord.getMarketSymbol(), stopwatch.elapsed(TimeUnit.MILLISECONDS));

    }


    class CandleHandler {

        private final Cron cron;
        private final PERIOD_CODE periodCode;

        CandleHandler(String cron, PERIOD_CODE periodCode) {
            this.cron = new Cron(cron);
            this.periodCode = periodCode;
        }

        private void handleCandle(SCandleRecord candleRecord) {
            Instant candleEndTime = Instant.ofEpochMilli(candleRecord.getCtm()).plus(1, ChronoUnit.MINUTES);
            Instant candleStartTime = candleEndTime.minus(periodCode.getCode(), ChronoUnit.MINUTES);


            if (cron.matches(candleEndTime.atZone(ZoneId.systemDefault()).toLocalDateTime())) {
                Duration duration = Duration.ofMinutes(periodCode.getCode());
                List<XtbCandle> candles = xtbCandleDao.findAllBySymbolAndOpenTimeGreaterThanEqualAndOpenTimeLessThanAndDuration(candleRecord.getSymbol(), candleStartTime, candleEndTime, Duration.ofMinutes(1));

                if (candles.isEmpty()) {
                    LOGGER.error("Candle data is empty for symbol: {} and dates {} - {} ", candleRecord.getSymbol(), candleStartTime, candleEndTime);
                    return;
                }
                persistCandle(candleRecord, duration, candles);

            }
        }


        private void persistCandle(SCandleRecord candleRecord, Duration duration, List<XtbCandle> candles) {
            XtbCandle xtbCandle = candles.stream().findFirst().get();
            BigDecimal high = candles.stream().max(Comparator.comparing(XtbCandle::getHigh)).map(XtbCandle::getHigh).get();
            BigDecimal low = candles.stream().min(Comparator.comparing(XtbCandle::getLow)).map(XtbCandle::getLow).get();
            BigDecimal open = candles.stream().min(Comparator.comparing(XtbCandle::getOpenTime)).map(XtbCandle::getOpen).get();
            BigDecimal close = candles.stream().max(Comparator.comparing(XtbCandle::getOpenTime)).map(XtbCandle::getClose).get();
            BigDecimal volume = BigDecimal.valueOf(candles.stream().mapToDouble(candle -> candle.getVolume().doubleValue()).average().orElse(0.0));


            xtbCandleDao.findBySymbolAndDurationAndOpenTime(candleRecord.getSymbol(), duration, Instant.ofEpochMilli(candleRecord.getCtm()))
                    .ifPresentOrElse(XtbCandle::getSymbol, () ->
                            xtbCandleDao.save(new XtbCandle(
                                    candleRecord.getSymbol(),
                                    xtbCandle.getInstrumentType(),
                                    duration, xtbCandle.getOpenTime(),
                                    open,
                                    high,
                                    low,
                                    close,
                                    volume
                            )));

        }


    }

    private void saveOneMinuteCandle(SCandleRecord candleRecord) {
        InstrumentType instrumentType = xtbInstrumentService.getInstrumentForSymbol(candleRecord.getSymbol())
                .map(XtbInstrument::getInstrumentType)
                .orElseThrow(() -> new IllegalArgumentException("Invalid symbol " + candleRecord.getSymbol()));

        xtbCandleDao.save(new XtbCandle(
                candleRecord.getSymbol(),
                instrumentType,
                Duration.ofMinutes(1),
                Instant.ofEpochMilli(candleRecord.getCtm()),
                BigDecimal.valueOf(candleRecord.getOpen()),
                BigDecimal.valueOf(candleRecord.getHigh()),
                BigDecimal.valueOf(candleRecord.getLow()),
                BigDecimal.valueOf(candleRecord.getClose()),
                BigDecimal.valueOf(candleRecord.getVol())
        ));
    }

}



