package com.mfuhrmann.algotrading.connectors.deutschebank;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.springframework.scheduling.annotation.Scheduled;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.stream.Stream;

//@Service
public class DBFxRateDownloader {

    private final static String URL = "https://www.db.com/belgium/docs/";
    public static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("ddMMyyyy");

    private final FxRateRangeRepository fxRateRangeRepository;
    private final Map<String, String> fxPairToCurrency = Map.of(
            "EURAUD", "AUD",
            "EURCAD", "CAD",
            "EURJPY", "JPY",
            "EURPLN", "PLN",
            "EURUSD", "USA",
            "EURGBP", "GBP",
            "EURCHF", "CHF"
    );

    public DBFxRateDownloader(FxRateRangeRepository fxRateRangeRepository) {
        this.fxRateRangeRepository = fxRateRangeRepository;
    }

    public FxRateRange getFxRateRange(String pair, LocalDate date, Fixing fixing) throws IOException {

        String url = URL + DATE_TIME_FORMATTER.format(date) + "_" + fixing.getFixingString() + "_Fixing_Belgie.pdf";



        String text = getText(new URL(url).openStream());
        String[] split = text.split("\\n");

        String pairLine = Arrays.stream(split)
                .filter(line -> line.contains(mapPairToString(pair)))
                .findFirst()
                .orElseThrow(RuntimeException::new);


        String[] s = pairLine.split(" ");


        return new FxRateRange(Double.parseDouble(s[s.length - 3]), Double.parseDouble(s[s.length - 1]), Double.parseDouble(s[s.length - 2]), pair, date, fixing);
    }

    private String mapPairToString(String pair) {
        return fxPairToCurrency.get(pair);
    }


    @Scheduled(fixedRate = 1000 * 3600)
    public void downloadFxRates() {
        fxPairToCurrency.forEach((key, value) -> {
            try {
                downloadSingleFxRate(key);
                System.out.println("Finished for the: " + key);

            } catch (FileNotFoundException e) {
                System.out.println("Could not find file" + e.getMessage());

            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });

        System.out.println("Finished downloading all of the fx rate ranges ");

    }

    private void downloadSingleFxRate(String fxPair) throws IOException {

        final LocalDate start = fxRateRangeRepository.findAll().stream()
                .filter(fxRateRange -> fxRateRange.getCurrency().equals(fxPair))
                .map(FxRateRange::getLocalDate)
                .max(Comparator.naturalOrder())
                .orElseGet(() -> LocalDate.ofYearDay(2018, 1));

        final LocalDate end = LocalDate.now();

        final int days = (int) start.until(end, ChronoUnit.DAYS);

        Stream.iterate(start, d -> d.plusDays(1))
                .limit(days)
                .filter(date -> date.getDayOfWeek() != DayOfWeek.SUNDAY && date.getDayOfWeek() != DayOfWeek.SATURDAY)
                .forEach(date -> saveFxRateRangeForDay(fxPair, date));
    }

    private void saveFxRateRangeForDay(String fxPair, LocalDate date) {
        try {


            FxRateRange morningRate = getFxRateRange(fxPair, date, Fixing.MORNING);
            fxRateRangeRepository.save(morningRate);

            FxRateRange afterNoonRate = getFxRateRange(fxPair, date, Fixing.AFTERNOON);
            fxRateRangeRepository.save(afterNoonRate);

            System.out.println("Fixings for date " + date + " created. Pair: " + fxPair);

        } catch (Exception e) {
            System.out.println("Could not find file for " + e.getMessage());

        }
    }


    static String getText(InputStream pdfFile) throws IOException {
        PDDocument doc = PDDocument.load(pdfFile);


        String text = new PDFTextStripper().getText(doc);
        doc.close();

        return text;
    }


}
