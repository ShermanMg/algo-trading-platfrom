package com.mfuhrmann.algotrading.connectors.cme;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CmeFixingRepository extends MongoRepository<CmeFixing, String> {
}
