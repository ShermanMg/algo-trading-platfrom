package com.mfuhrmann.algotrading.connectors.bitbay;

import com.mfuhrmann.algotrading.commons.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
class BitbayClientConnector {
    private final RestTemplate restTemplate;
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    @Autowired
    BitbayClientConnector(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    List<BitbayTrades> getTradesFromServer(CurrencyPair currencyPair, long since) {
        try {
            return executorService.schedule(() -> restTemplate.exchange(createUrlToBitbay(currencyPair, since),
                    HttpMethod.GET, null, new ParameterizedTypeReference<List<BitbayTrades>>() {
                    }).getBody(),
                    1, TimeUnit.SECONDS)
                    .get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
    }


    ResponseEntity<BitbayTicker> getTickerForCurrency(CurrencyPair currencyPair) {
        return restTemplate.getForEntity("https://bitbay.net/API/Public/" + currencyPair.getCurrency1() + currencyPair.getCurrency2() + "/ticker.json", BitbayTicker.class);
    }

    private String createUrlToBitbay(CurrencyPair currencyPair, long since) {
        return "https://bitbay.net/API/Public/"
                + currencyPair.getCurrency1() + currencyPair.getCurrency2() +
                "/trades.json?since=" + (since - 1);
    }

}
