package com.mfuhrmann.algotrading.connectors.bitbay;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

public class BitbayTrades {

    private final long tid;
    private final LocalDateTime date;
    private final double price;
    private final String type;
    private final double amount;

    public BitbayTrades(long tid, int date, double price, String type, double amount) {
        this.tid = tid;
        this.date = LocalDateTime.ofInstant(Instant.ofEpochSecond(date), ZoneId.systemDefault());
        this.price = price;
        this.type = type;
        this.amount = amount;
    }

    public long getTid() {
        return tid;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public double getPrice() {
        return price;
    }

    public String getType() {
        return type;
    }

    public double getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "BitbayTrades{" +
                "tid=" + tid +
                ", date=" + date +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                '}';
    }
}
