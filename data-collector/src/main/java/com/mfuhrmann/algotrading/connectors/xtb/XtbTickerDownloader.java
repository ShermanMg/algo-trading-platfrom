package com.mfuhrmann.algotrading.connectors.xtb;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.model.xtb.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pro.xstore.api.message.error.APICommunicationException;
import pro.xstore.api.message.records.SCandleRecord;
import pro.xstore.api.message.records.STickRecord;
import pro.xstore.api.streaming.StreamingListener;
import pro.xstore.api.sync.ServerData;
import pro.xstore.api.sync.SyncAPIConnector;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Service
public class XtbTickerDownloader {
    private static final Logger LOGGER = LoggerFactory.getLogger(XtbTickerDownloader.class);


    private final XtbInstrumentService xtbInstrumentService;
    private final XtbCandleDao xtbCandleDao;
    private final XtbTickDao xtbTickDao;
    private final SyncAPIConnector apiConnector;
    private final CandleStreamHandler candleStreamHandler;

    public XtbTickerDownloader(XtbInstrumentService xtbInstrumentService, XtbCandleDao xtbCandleDao, XtbTickDao xtbTickDao, XtbClientConnector xtbClientConnector, CandleStreamHandler candleStreamHandler) {

        this.xtbInstrumentService = xtbInstrumentService;
        this.xtbCandleDao = xtbCandleDao;
        this.xtbTickDao = xtbTickDao;
        this.apiConnector = xtbClientConnector.createConnector(ServerData.ServerEnum.REAL);
        this.candleStreamHandler = candleStreamHandler;
    }

    @PostConstruct
    public void init() throws IOException, APICommunicationException {


        Set<String> currencies = Set.of("EUR", "USD", "JPY", "GBP", "CHF", "CAD", "AUD");
        List<String> symbols = xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> currencies.contains(xtbInstrument.getSymbol().substring(0, 3)) && currencies.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .map(XtbInstrument::getSymbol)
                .collect(Collectors.toList());

        List<Long> pipsChanges = new ArrayList<>();


        List<XtbCandle> candles = xtbInstrumentService.getFxInstruments().stream()
                .filter(xtbInstrument -> currencies.contains(xtbInstrument.getSymbol().substring(0, 3)) && currencies.contains(xtbInstrument.getSymbol().substring(3, 6)))
                .flatMap(fxInstrument -> xtbCandleDao.findAllBySymbolAndDurationAndOpenTimeAfterOrderByOpenTimeAsc(fxInstrument.getSymbol(), Duration.ofMinutes(1), LocalDateTime.now().minusDays(3).toInstant(ZoneOffset.UTC)))
                .collect(Collectors.toList());


        StreamingListener sl = new StreamingListener() {

            private final AtomicLong counter = new AtomicLong();
            private final Map<String, Long> map = new ConcurrentHashMap<>();

            @Override
            public void receiveTickRecord(STickRecord tickRecord) {
                xtbTickDao.save(new XtbTick(tickRecord));
//                map.compute(tickRecord.getMarketSymbol(), (symbol, value) -> {
//                    if (value == null) {
//                        return 1L;
//                    }
//                    return value + 1;
//                });
//                if (counter.incrementAndGet() % 10000 == 0) {
//
//                    LOGGER.info(map.toString());
//
//                }

            }

            @Override
            public void receiveCandleRecord(SCandleRecord candleRecord) {

                candleStreamHandler.onCandle(candleRecord);

//                strategy.onNextCandleClose(new ForexMarketSymbol(candleRecord.getMarketSymbol()), BigDecimal.valueOf(candleRecord.getClose()), Instant.ofEpochMilli(candleRecord.getCtm()),);
//                pipsChanges.add(portfolio.pipsProfit());
//                if (!Duration.between(lastTimestamp, Instant.now()).minusMinutes(1).isNegative()) {
//                    lastTimestamp = Instant.now();
//                    LOGGER.info("Candle for time " + lastTimestamp);
//
//                }

            }
        };


        apiConnector.connectStream(sl);

        symbols.forEach(symbol -> {
            try {
                apiConnector.subscribePrice(symbol, 1);
            } catch (APICommunicationException e) {
                e.printStackTrace();
            }
        });

        apiConnector.subscribeCandles(symbols);

    }
}
