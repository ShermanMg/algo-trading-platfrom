package com.mfuhrmann.algotrading.connectors.deutschebank;

public enum Fixing {
    MORNING {
        @Override
        String getFixingString() {
            return "9am";
        }
    }, AFTERNOON {
        @Override
        String getFixingString() {
            return "1pm";
        }

    };

    abstract String getFixingString();
}