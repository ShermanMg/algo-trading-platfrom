package com.mfuhrmann.algotrading.connectors.xtb;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.model.xtb.*;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pro.xstore.api.message.codes.PERIOD_CODE;
import pro.xstore.api.message.records.RateInfoRecord;
import pro.xstore.api.message.response.ChartResponse;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.text.DecimalFormat;
import java.time.*;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;


@Component
public class XtbCandleSynchronizerJob {
    private static final Logger LOGGER = LoggerFactory.getLogger(XtbCandleSynchronizerJob.class);

    private static final List<PeriodData> PERIOD_DATA_LIST = List.of(
            new PeriodData(PERIOD_CODE.PERIOD_M1, LocalDate.of(2018, 6, 16).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_M5, LocalDate.of(2018, 6, 16).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_M15, LocalDate.of(2018, 1, 27).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_M30, LocalDate.of(2018, 1, 27).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_H1, LocalDate.of(2018, 1, 27).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_H4, LocalDate.of(2017, 7, 15).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_D1, LocalDate.of(2008, 7, 20).atStartOfDay()),
            new PeriodData(PERIOD_CODE.PERIOD_W1, LocalDate.of(2008, 7, 20).atStartOfDay()));

    public static final int MULTIPLICAND = 360;
    private static final DecimalFormat df = new DecimalFormat("#.####");

    private final XtbClientConnector xtbClientConnector;
    private final XtbCandleDao xtbCandleDao;
    private final XtbCandleSnapshotDao xtbCandleSnapshotDao;
    private final XtbInstrumentService xtbInstrumentService;
    private final List<CandleJobData> candleJobData;


    @Autowired
    public XtbCandleSynchronizerJob(XtbClientConnector xtbClientConnector, XtbCandleDao xtbCandleDao, XtbCandleSnapshotDao xtbCandleSnapshotDao, XtbInstrumentService xtbInstrumentService) {
        this.xtbClientConnector = xtbClientConnector;
        this.xtbCandleDao = xtbCandleDao;
        this.xtbCandleSnapshotDao = xtbCandleSnapshotDao;
        this.xtbInstrumentService = xtbInstrumentService;
        this.candleJobData = xtbInstrumentService.getSelectedInstruments().stream()
                .map(xtbInstrument -> new CandleJobData(xtbInstrument.getSymbol(), xtbInstrument.getInstrumentType()))
                .collect(Collectors.toList());
    }


    @PostConstruct
    public void downloadData() throws IOException {
//
//        PERIOD_DATA_LIST.forEach(periodData ->
//                xtbCandleDao.deleteAllByOpenTimeBeforeAndDuration(periodData.firstCandleTime, periodData.getDuration()));


        new Thread(this::synchronizePastCandles).start();


    }

    private void synchronizePastCandles() {
        LOGGER.info("Started downloading history candles");

        AtomicInteger atomicInteger = new AtomicInteger(1);
        candleJobData.stream()
                .peek(data -> LOGGER.info("Starting new instrument " + data.symbol + " of type: " + data.instrumentType + " which is: " + atomicInteger.getAndIncrement() + " out of: " + candleJobData.size()))
                .forEach(data -> PERIOD_DATA_LIST
                        .forEach(periodData -> downloadSingleInstrument(data, periodData)));

        LOGGER.info("Finished downloading history candles");
    }


    private void downloadSingleInstrument(CandleJobData data, PeriodData periodData) {

        LOGGER.debug("Starting  downloading instrument {} for period {} ", data.symbol, periodData.getDuration());


        Duration duration = periodData.getDuration();
        Duration multipliedDuration = duration.multipliedBy(MULTIPLICAND);

        Instant startDate = findLastSnapshot(data.symbol, duration)
                .map(i -> i.plus(duration))
                .orElse(periodData.firstCandleTime);

        Instant endDate = startDate.plus(multipliedDuration).plus(duration);

        while (startDate.isBefore(Instant.now())) {

            Optional<ChartResponse> responseOp = xtbClientConnector.getChartResponseForDateRange(data.symbol, startDate, endDate, periodData.periodCode);

            Instant finalStartDate = startDate;
            Instant finalEndDate = endDate;

            responseOp.ifPresent(chartResponseForDateRange -> {

                int digits = chartResponseForDateRange.getDigits();

                Map<CandleStatus, Long> statsMap = chartResponseForDateRange.getRateInfos().stream()
                        .map(record -> saveIfNotPresent(data, duration, record, digits))
                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

                LOGGER.debug("Downloaded {} candles {} from range : {} to {} for duration {} Candles by group: {}", chartResponseForDateRange.getRateInfos().size(), data.symbol, finalStartDate, finalEndDate, duration, statsMap);

                delayConnectionToAPI();
                saveCandleSnapshot(data, duration, chartResponseForDateRange);
            });
            startDate = startDate.plus(multipliedDuration).minus(duration);
            endDate = startDate.plus(multipliedDuration).plus(duration);
        }

        LOGGER.info("Finished downloading instrument {} for period {} ", data.symbol, periodData.getDuration());

    }

    private Optional<Instant> findLastSnapshot(String symbol, Duration duration) {
        return xtbCandleSnapshotDao.findFirstBySymbolAndDurationOrderByOpenTimeDesc(symbol, duration)
                .map(XtbCandleSnapshot::getOpenTime);
    }


    private void saveCandleSnapshot(CandleJobData data, Duration duration, ChartResponse chartResponseForDateRange) {
        chartResponseForDateRange.getRateInfos().stream()
                .max(Comparator.comparing(RateInfoRecord::getCtm))
                .ifPresent(record -> {
                    XtbCandleSnapshot snapshot = new XtbCandleSnapshot(data.symbol, duration, Instant.ofEpochMilli(record.getCtm()));
                    xtbCandleSnapshotDao.save(snapshot);
                    LOGGER.debug("Created snapshot for {}", snapshot);
                });
    }

    private CandleStatus saveIfNotPresent(CandleJobData data, Duration duration, RateInfoRecord record, int digits) {


        AtomicReference<CandleStatus> candleStatus = new AtomicReference<>(CandleStatus.CREATED);
        xtbCandleDao.findBySymbolAndDurationAndOpenTime(data.symbol, duration, Instant.ofEpochMilli(record.getCtm()))
                .ifPresentOrElse(xtbCandle -> {
                    XtbCandle candle = createCandle(data, duration, record, digits);
                    if (!xtbCandle.equals(candle)) {
                        candleStatus.set(CandleStatus.UPDATED);
                        xtbCandleDao.save(xtbCandle.updateWith(candle));
                    }
                    candleStatus.set(CandleStatus.SKIPPED);
                }, () -> xtbCandleDao.save(createCandle(data, duration, record, digits)));

        return candleStatus.get();
    }

    @NotNull
    private XtbCandle createCandle(CandleJobData data, Duration duration, RateInfoRecord record, int digits) {
        return new XtbCandle(data.symbol, data.instrumentType, duration, record, digits);
    }

    private void delayConnectionToAPI() {
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static class CandleJobData {
        private final String symbol;
        private final InstrumentType instrumentType;

        private CandleJobData(String symbol, InstrumentType instrumentType) {
            this.symbol = symbol;
            this.instrumentType = instrumentType;
        }
    }

    private static class PeriodData {
        private final PERIOD_CODE periodCode;
        private final Instant firstCandleTime;

        private PeriodData(PERIOD_CODE periodCode, LocalDateTime periodDate) {
            this.periodCode = periodCode;
            this.firstCandleTime = periodDate.atZone(ZoneId.systemDefault()).toInstant();
        }

        Duration getDuration() {
            return Duration.ofMinutes(periodCode.getCode());
        }

    }

    enum CandleStatus {
        CREATED, UPDATED, SKIPPED,
    }

}
