package com.mfuhrmann.algotrading.connectors.bitbay;

import com.fasterxml.jackson.annotation.JsonCreator;

public class BitbayTicker {


    private double max;
    private double min;
    private double last;
    private double bid;
    private double ask;
    private double vwap;
    private double average;
    private double volume;

    @JsonCreator(mode = JsonCreator.Mode.PROPERTIES)
    public BitbayTicker(double max, double min, double last, double bid, double ask, double vwap, double average,
                                double volume) {
        this.max = max;
        this.min = min;
        this.last = last;
        this.bid = bid;
        this.ask = ask;
        this.vwap = vwap;
        this.average = average;
        this.volume = volume;
    }


    public double getMax() {
        return max;
    }

    public double getMin() {
        return min;
    }

    public double getLast() {
        return last;
    }

    public double getBid() {
        return bid;
    }

    public double getAsk() {
        return ask;
    }

    public double getVwap() {
        return vwap;
    }

    public double getAverage() {
        return average;
    }

    public double getVolume() {
        return volume;
    }

    @Override
    public String toString() {
        return "BitbayTickerResponse{" +
                ", max=" + max +
                ", min=" + min +
                ", last=" + last +
                ", bid=" + bid +
                ", ask=" + ask +
                ", vwap=" + vwap +
                ", average=" + average +
                ", volume=" + volume +
                '}';
    }
}
