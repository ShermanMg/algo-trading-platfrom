package com.mfuhrmann.algotrading.connectors.xtb;

import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.model.xtb.InstrumentType;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrument;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrumentDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class XtbInstrumentJob {

    private final XtbClientConnector xtbClientConnector;
    private final XtbInstrumentDao xtbInstrumentDao;

    private static final Logger LOGGER = LoggerFactory.getLogger(XtbInstrumentJob.class);

    @Autowired
    public XtbInstrumentJob(XtbClientConnector xtbClientConnector, XtbInstrumentDao xtbInstrumentDao) {
        this.xtbClientConnector = xtbClientConnector;
        this.xtbInstrumentDao = xtbInstrumentDao;
    }


//    @PostConstruct
    public void downloadInstruments() {

        xtbClientConnector.getAllSymbols().getSymbolRecords()
                .forEach(symbolRecord -> xtbInstrumentDao.findBySymbol(symbolRecord.getSymbol())
                        .ifPresentOrElse(xtbInstrument -> xtbInstrumentDao.save(xtbInstrument.update(symbolRecord)),
                                () -> xtbInstrumentDao.save(new XtbInstrument(symbolRecord.getSymbol(), InstrumentType.parse(symbolRecord.getCategoryName()), symbolRecord))));

        LOGGER.info("Finished downloading instruments");


    }

//    TODO job every 24 hour to update all


}
