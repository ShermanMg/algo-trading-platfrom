package com.mfuhrmann.algotrading.config;

import com.mongodb.MongoClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;


@Configuration
public class DbConfig extends AbstractMongoConfiguration {
    @Override
    protected String getDatabaseName() {
        return "data-collector";
    }

    @Override
    public MongoClient mongoClient() {
        return new MongoClient("localhost", 27017);
    }
}
