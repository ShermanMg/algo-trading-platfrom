package com.mfuhrmann.algotrading.model.xtb.realtime;


import com.mfuhrmann.algotrading.commons.connectors.xtb.XtbClientConnector;
import com.mfuhrmann.algotrading.model.xtb.XtbCandle;
import com.mfuhrmann.algotrading.model.xtb.XtbCandleDao;
import com.mfuhrmann.algotrading.model.xtb.XtbInstrumentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pro.xstore.api.message.error.APICommunicationException;
import pro.xstore.api.message.records.SCandleRecord;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;

@Component
public class RealTimeCandleCollector {

    private static final Logger LOGGER = LoggerFactory.getLogger(RealTimeCandleCollector.class);


    private final XtbClientConnector connector;
    private final XtbInstrumentService instrumentService;
    private final XtbCandleDao xtbCandleDao;

    @Autowired
    public RealTimeCandleCollector(XtbClientConnector connector, XtbInstrumentService instrumentService, XtbCandleDao xtbCandleDao) {
        this.connector = connector;
        this.instrumentService = instrumentService;
        this.xtbCandleDao = xtbCandleDao;
    }


    @PostConstruct
    public void init() throws IOException, APICommunicationException {


//        SyncAPIConnector apiConnector = connector.createConnector();
//
//        List<String> symbols = instrumentService.getSelectedInstruments().stream()
//                .map(XtbInstrument::getMarketSymbol)
//                .collect(Collectors.toList());
//
//
//        StreamingListener sl = new StreamingListener() {
//
//            @Override
//            public void receiveBalanceRecord(SBalanceRecord balanceRecord) {
//                LOGGER.info(balanceRecord.toString());
//            }
//
//            @Override
//            public void receiveCandleRecord(SCandleRecord candleRecord) {
//                xtbCandleDao.save(createCandle(candleRecord));
//            }
//        };
//
//        apiConnector.connectStream(sl);
//
//
//        apiConnector.subscribeCandles(symbols);
//        apiConnector.subscribeBalance();


    }


    private XtbCandle createCandle(SCandleRecord record) {

        return new XtbCandle(
                record.getSymbol(),
                instrumentService.getInstrumentForSymbol(record.getSymbol())
                        .orElseThrow(() -> new RuntimeException("Unknown instrument type " + record.getSymbol()))
                        .getInstrumentType(),
                Duration.ofMinutes(1),
                Instant.ofEpochMilli(record.getCtm()),
                BigDecimal.valueOf(record.getOpen()),
                BigDecimal.valueOf(record.getHigh()),
                BigDecimal.valueOf(record.getLow()),
                BigDecimal.valueOf(record.getClose()),
                BigDecimal.valueOf(record.getVol())
        );
    }

}
