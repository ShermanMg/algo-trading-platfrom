package com.mfuhrmann.algotrading.model.xtb;

import com.mfuhrmann.algotrading.commons.BaseId;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import pro.xstore.api.message.records.SymbolRecord;

@Document(collection = "xtb_instrument")
public class XtbInstrument extends BaseId {

    @Indexed(unique = true)
    private String symbol;
    @Indexed
    private InstrumentType instrumentType;
    private SymbolRecord symbolRecordData;


    public XtbInstrument(String symbol, InstrumentType instrumentType, SymbolRecord symbolRecordData) {
        this.symbol = symbol;
        this.instrumentType = instrumentType;
        this.symbolRecordData = symbolRecordData;
    }

    public XtbInstrument update(SymbolRecord symbolRecord) {
        this.symbolRecordData = symbolRecord;
        this.symbol = symbolRecord.getSymbol();
        return this;
    }

    public String getSymbol() {
        return symbol;
    }

    public InstrumentType getInstrumentType() {
        return instrumentType;
    }

    public SymbolRecord getSymbolRecordData() {
        return symbolRecordData;
    }
}
