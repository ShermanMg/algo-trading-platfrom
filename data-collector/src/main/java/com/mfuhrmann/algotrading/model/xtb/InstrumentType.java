package com.mfuhrmann.algotrading.model.xtb;

import java.util.Arrays;

public enum InstrumentType {

    FOREX("FX"),
    INDEX("IND"),
    COMMODITY("CMD"),
    CRYPTO("CRT"),
    ETF("ETF"),
    STOCKS("STK"),
    STOCKS_CASH("STC");
    private final String code;

    InstrumentType(String code) {
        this.code = code;
    }

    public static InstrumentType parse(String symbol) {
        return Arrays.stream(InstrumentType.values())
                .filter(instrumentType -> instrumentType.code.equals(symbol)).findAny()
                .orElseThrow(() -> new IllegalArgumentException("No such instrument type " + symbol));
    }
}
