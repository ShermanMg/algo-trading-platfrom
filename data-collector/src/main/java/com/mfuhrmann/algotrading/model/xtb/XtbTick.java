package com.mfuhrmann.algotrading.model.xtb;

import com.mfuhrmann.algotrading.commons.BaseId;
import com.mfuhrmann.trading.backtesting.trade.DataSourceTick;
import com.mfuhrmann.trading.backtesting.trade.ForexMarketSymbol;
import com.mfuhrmann.trading.backtesting.trade.MarketSymbol;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import pro.xstore.api.message.records.STickRecord;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Instant;

//@Getter
@Document(collection = "xtb_tick")
//@CompoundIndexes(@CompoundIndex(name = "compound_id", def = "{'symbol':1,'quoteId':1}", unique = true))
public class XtbTick extends BaseId implements DataSourceTick {


    @Indexed
    private String symbol;
    @NotNull
    private BigDecimal bid;
    @NotNull
    private BigDecimal ask;
    @NotNull
    private Instant time;
    @NotNull
    private InstrumentType instrumentType;
    @NotNull
    private int quoteId;
    @NotNull
    private Long askVolume;
    @NotNull
    private Long bidVolume;


    private XtbTick() {
    }

    public XtbTick(STickRecord tickRecord) {
        this.symbol = tickRecord.getSymbol();
        this.bid = BigDecimal.valueOf(tickRecord.getBid());
        this.ask = BigDecimal.valueOf(tickRecord.getAsk());
        this.time = Instant.ofEpochMilli(tickRecord.getTimestamp());
        this.quoteId = tickRecord.getQuoteId();
        this.instrumentType = InstrumentType.FOREX;
        this.askVolume = tickRecord.getAskVolume();
        this.bidVolume = tickRecord.getAskVolume();
    }


//    public XtbTick(String symbol, InstrumentType instrumentType, Duration duration, RateInfoRecord record, int digits) {
//        this.symbol = symbol;
//        this.instrumentType = instrumentType;
//        this.openTime = Instant.ofEpochMilli(record.getCtm());
//        this.duration = duration;
//
//        BigDecimal divisor = BigDecimal.valueOf(Math.pow(10, digits));
//
//        this.open = convertToDecimal(record.getOpen(), divisor);
//        this.high = convertToDecimal(record.getOpen() + record.getHigh(), divisor);
//        this.low = convertToDecimal(record.getOpen() + record.getLow(), divisor);
//        this.close = convertToDecimal(record.getOpen() + record.getClose(), divisor);
//        this.volume = convertToDecimal(record.getVol(), divisor);
//    }
//
//    public XtbTick(String symbol, @NotNull InstrumentType instrumentType, @NotNull Duration duration,
//                   @NotNull Instant openTime, @NotNull BigDecimal open, @NotNull BigDecimal high,
//                   @NotNull BigDecimal low, @NotNull BigDecimal close, @NotNull BigDecimal volume) {
//        this.symbol = symbol;
//        this.instrumentType = instrumentType;
//        this.duration = duration;
//        this.openTime = openTime;
//        this.open = open;
//        this.high = high;
//        this.low = low;
//        this.close = close;
//        this.volume = volume;
//    }


    private BigDecimal convertToDecimal(double value, BigDecimal divisor) {
        return BigDecimal.valueOf(value).divide(divisor, MathContext.DECIMAL32);
    }


    @Override
    public MarketSymbol getMarketSymbol() {
        return new ForexMarketSymbol(symbol);
    }

    @Override
    public BigDecimal getBid() {
        return bid;
    }

    @Override
    public BigDecimal getAsk() {
        return ask;
    }

    @Override
    public Instant getTime() {
        return time;
    }

    @Override
    public long getAskVolume() {
        return askVolume;
    }

    @Override
    public long getBidVolume() {
        return bidVolume;
    }


}
