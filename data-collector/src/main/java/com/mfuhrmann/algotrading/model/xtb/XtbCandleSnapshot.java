package com.mfuhrmann.algotrading.model.xtb;

import com.mfuhrmann.algotrading.commons.BaseId;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

@Document(collection = "xtb_candle_snapshot")
public class XtbCandleSnapshot extends BaseId {

    private String symbol;
    private Duration duration;
    private Instant openTime;


    private XtbCandleSnapshot() {
    }

    public XtbCandleSnapshot(String symbol, Duration duration, Instant openTime) {
        this.symbol = symbol;
        this.duration = duration;
        this.openTime = openTime;
    }

    public String getSymbol() {
        return symbol;
    }

    public Duration getDuration() {
        return duration;
    }

    public Instant getOpenTime() {
        return openTime;
    }

    @Override
    public String toString() {
        return "XtbCandleSnapshot{" +
                "symbol='" + symbol + '\'' +
                ", duration=" + duration +
                ", openTime=" + LocalDateTime.ofInstant(openTime, ZoneId.systemDefault()) +
                '}';
    }
}
