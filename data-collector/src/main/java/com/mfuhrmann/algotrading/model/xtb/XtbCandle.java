package com.mfuhrmann.algotrading.model.xtb;

import com.mfuhrmann.algotrading.commons.BaseId;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import pro.xstore.api.message.records.RateInfoRecord;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

@Document(collection = "xtb_candle")
@CompoundIndexes(@CompoundIndex(name = "compound_id", def = "{'symbol':1,'openTime':1 ,'duration':1}", unique = true))
public class XtbCandle extends BaseId  {

    @Indexed
    private String symbol;
    @NotNull
    private InstrumentType instrumentType;
    @NotNull
    private Duration duration;
    @NotNull
    private Instant openTime;
    @NotNull
    private BigDecimal open;
    @NotNull
    private BigDecimal high;
    @NotNull
    private BigDecimal low;
    @NotNull
    private BigDecimal close;
    @NotNull
    private BigDecimal volume;

    private XtbCandle() {
    }


    public XtbCandle(String symbol, InstrumentType instrumentType, Duration duration, RateInfoRecord record, int digits) {
        this.symbol = symbol;
        this.instrumentType = instrumentType;
        this.openTime = Instant.ofEpochMilli(record.getCtm());
        this.duration = duration;

        BigDecimal divisor = BigDecimal.valueOf(Math.pow(10, digits));

        this.open = convertToDecimal(record.getOpen(), divisor);
        this.high = convertToDecimal(record.getOpen() + record.getHigh(), divisor);
        this.low = convertToDecimal(record.getOpen() + record.getLow(), divisor);
        this.close = convertToDecimal(record.getOpen() + record.getClose(), divisor);
        this.volume = convertToDecimal(record.getVol(), divisor);
    }

    public XtbCandle(String symbol, @NotNull InstrumentType instrumentType, @NotNull Duration duration,
                     @NotNull Instant openTime, @NotNull BigDecimal open, @NotNull BigDecimal high,
                     @NotNull BigDecimal low, @NotNull BigDecimal close, @NotNull BigDecimal volume) {
        this.symbol = symbol;
        this.instrumentType = instrumentType;
        this.duration = duration;
        this.openTime = openTime;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.volume = volume;
    }

    public String getSymbol() {
        return symbol;
    }

    public InstrumentType getInstrumentType() {
        return instrumentType;
    }

    public Instant getOpenTime() {
        return openTime;
    }

    public BigDecimal getOpen() {
        return open;
    }

    public BigDecimal getHigh() {
        return high;
    }

    public BigDecimal getLow() {
        return low;
    }

    public BigDecimal getClose() {
        return close;
    }

    public BigDecimal getVolume() {
        return volume;
    }

    public Duration getDuration() {
        return duration;
    }

    private BigDecimal convertToDecimal(double value, BigDecimal divisor) {
        return BigDecimal.valueOf(value).divide(divisor, MathContext.DECIMAL32);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        XtbCandle xtbCandle = (XtbCandle) o;
        return instrumentType == xtbCandle.instrumentType &&
                Objects.equals(symbol, xtbCandle.symbol) &&
                Objects.equals(openTime, xtbCandle.openTime) &&
                Objects.equals(duration, xtbCandle.duration) &&
                Objects.equals(open, xtbCandle.open) &&
                Objects.equals(high, xtbCandle.high) &&
                Objects.equals(low, xtbCandle.low) &&
                Objects.equals(close, xtbCandle.close) &&
                Objects.equals(volume, xtbCandle.volume);
    }

    @Override
    public int hashCode() {

        return Objects.hash(instrumentType, symbol, openTime, duration, open, high, low, close, volume);
    }

    public XtbCandle updateWith(XtbCandle candle) {
        this.duration = candle.duration;
        this.openTime = candle.openTime;
        this.open = candle.open;
        this.high = candle.high;
        this.low = candle.low;
        this.close = candle.close;
        this.volume = candle.volume;
        return this;
    }
}
