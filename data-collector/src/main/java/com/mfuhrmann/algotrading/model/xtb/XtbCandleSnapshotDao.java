package com.mfuhrmann.algotrading.model.xtb;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.util.Optional;

@Repository
public interface XtbCandleSnapshotDao extends MongoRepository<XtbCandleSnapshot, String> {

    Optional<XtbCandleSnapshot> findFirstBySymbolAndDurationOrderByOpenTimeDesc(String symbol, Duration duration);


}
